<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class auth extends CI_Controller {
	public function __construct()
	  {
	    parent::__construct();
	    $this->load->model('Model_Login');
	    $this->load->model('Model_Pengelola');
	  }




	public function login()
	{
		$this->load->view('pengelola/login');
	}

	public function proses_login()
	{
		$id_pengelola = $this->input->post('id_pengelola');
		$password = $this->input->post('password');

		$data_pengelola = array(
			'ID_pengelola' => $id_pengelola,
			'Password' => md5($password)
		);

		$cekLogin = $this->Model_Login->cekLogin('pengelola', $data_pengelola)->num_rows();

		if ($cekLogin > 0) {
			
			$dataSession = array(

				'id_pengelola' => $id_pengelola,
				'Status' => "Login"
			);

			print_r($dataSession);

		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert">Username dan Password salah</div>');
			redirect(base_url('pengelola/auth/login'));
		}
	}

	public function logout(){
	$this->session->sess_destroy();
	redirect(base_url('pengelola/auth/login'));
	}
}
