<!DOCTYPE html>
<html>
<?php $this->load->view('kasir/head') ?>
<head>
  <style type="text/css">
    .tombol {

      margin: 0 0 100px 400px;
    }
  </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php $this->load->view('kasir/header') ?>
  <!-- Left side column. contains the logo and sidebar -->

<?php $this->load->view('kasir/leftbar') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
      <div class="row">

        <div class="col-md-12">

          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Pembukaan Kasir</h3>
            </div>
            <div class="box-header">
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="<?php echo base_url(); ?>kasir/BukaKasir/proses_inputBukaKasir" method="POST">
              <div class="box-footer">
                <div class="col-md-6">
                  <div class="box-body">

                  <div class="form-group">
                    <label for="exampleInputEmail1">Saldo Awal</label>
                      <input type="text" class="form-control rupiah" id="potongan" placeholder="Rp100.000-" name="Saldo_awal">
                      </div>

                </div>
              </div>
              <div class="col-md-6 tombol">
              <button type="submit" class="btn btn-primary">INPUT DATA</button>
            </div>

            </div>
         </form>
          <!-- /.box -->


        </div>

        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script src="<?php echo base_url('assets/template/back/dist') ?>/js/adminlte.min.js"></script>
<script type="text/javascript">
   $('#potongan').keypress(function(event){
            console.log(event.which);
        if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
            event.preventDefault();
        }});

         $(document).ready(function(){

    // Format mata uang.
    $( '.rupiah' ).mask('0.000.000.000', {reverse: true});

})
</script>

</body>
</html>
