<!DOCTYPE html>
<html>
<?php $this->load->view('kasir/head') ?>
<head>
  <style type="text/css">
    .tombol {
        float: right;
    }

    .box-body {
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
}
  </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php $this->load->view('kasir/header') ?>


<?php $this->load->view('kasir/leftbar') ?>

  <div class="content-wrapper">
    <section class="content">
      <div class="row">
       
        <div class="col-md-12">

          <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">INPUT PROMO</h3>
            </div>
             <form role="form" action="<?php echo base_url(); ?>kasir/pembayaran/prosesInputDiskon" method="POST">
              <div class="box-footer">
                  <div class="box-body">
                    <div class="col-md-6">
                  <div class="form-group">
                      <label for="exampleInputPassword1">Keterangan</label>
                    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Keterangan" name="keterangan">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Total Diskon (%)</label>
                    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Total Diskon" name="total">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Jumlah</label>
                    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Jumlah" name="jumlah">
                  </div>

                  <div class="form-group">
                    <label for="exampleInputPassword1">Jenis</label>
                    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Jenis" name="jenis">
                  </div>
                   <div class="tombol">
                     <button type="submit" class="btn btn-primary">INPUT DISKON</button>
                   </div>

                </div>
                          
              </div>
            </div>

            </div>
         </form>
          <!-- /.box -->


        </div>

        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
<?php $this->load->view('kasir/footer') ?>
<script type="text/javascript">
   //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })
</script>

</body>
</html>
