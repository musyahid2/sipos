<!DOCTYPE html>
<html>
<?php $this->load->view('kasir/head') ?>
<head>
 
   <style>
    .example-modal .modal {
      position: relative;
      top: auto;
      bottom: auto;
      right: auto;
      left: auto;
      display: block;
      z-index: 1;
    }

    .example-modal .modal {
      background: transparent !important;
    }
  </style> 

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php $this->load->view('kasir/header') ?>
  <!-- Left side column. contains the logo and sidebar -->

<?php $this->load->view('kasir/leftbar') ?>

<div class="content-wrapper">


    <section class="content">
      <div class="row">

        <div class="col-xs-12">
          <div class="box">
             <div class="box-header">
              <h3 class="box-title">Data Diskon</h3>
            </div>
            <div class="box-header">
              <a class="btn btn-info" href="<?php echo site_url('kasir/pembayaran/inputDiskon') ?>" role="button"><span class="glyphicon glyphicon-plus"> TAMBAH DISKON</span></a>
            </div>
          <div class="box-header">
          <div class="col-md-4">
               <?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');} ?>
      </div>
        </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th><center>Kode Diskon</center></th>
                    <th><center>Keterangan</center></th>
                    <th><center>Total Diskon</center></th>
                    <th><center>Jenis </center></th>
                    <th><center>Diinput Oleh</center></th>
                    <th><center>Kelola</center></th>
                  </tr>
                </thead>
                <tbody>

                  <?php 
                  $i = 1;
                  $datadiskon = $this->session->diskon;

                foreach ($datadiskon as $data) { ?>
                        <tr>
                          <td><center><?php echo $data->Kode_diskon ?></center></td>
                          <td><center><?php echo $data->Keterangan ?></center></td>
                          <td><center><?php echo $data->Total_diskon; echo " %" ?></center></td>
                          <td><center><?php echo $data->Jenis ?></center></td>
                          <td><center><?php echo $data->Nama_kasir ?></center></td>
                        <td>
                          <center>
                            <a href="<?php echo site_url() ?>/kasir/pembayaran/editDiskon?KodeDiskon=<?php echo $data->Kode_diskon ?>" class="btn btn-sm btn-success"> <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>

                            <Button data-toggle="modal" data-target="#confirm-delete<?php echo $i; ?>" class='btn btn-sm btn-danger'><span class="glyphicon glyphicon-trash"></a></Button>

                             <div class="modal fade" id="confirm-delete<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                      <div class="modal-dialog">
                                          <div class="modal-content">
                                              <div class="modal-header">
                                                  Konfirmasi Hapus
                                              </div>
                                              <div class="modal-body">
                                                  Apakah yakin ingin menghapus data?
                                              </div>
                                              <div class="modal-footer">
                                                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                  <a href="<?php echo base_url()?>/kasir/pembayaran/hapusDiskon?KodeDiskon=<?php echo $data->Kode_diskon ?>" class="btn btn-danger btn-ok" >Delete</a>
                                              </div>
                                          </div>
                                      </div>
                              </div>

                          </center>
                        </td>
                      </tr>
                   <?php } ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>
    </section>
</div>

<?php $this->load->view('kasir/footer') ?>
</body>
</html>
