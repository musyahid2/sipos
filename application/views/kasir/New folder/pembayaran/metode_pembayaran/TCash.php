<!DOCTYPE html>
<html>
<?php $this->load->view('kasir/head') ?>
<head>
 
   <style>
    .example-modal .modal {
      position: relative;
      top: auto;
      bottom: auto;
      right: auto;
      left: auto;
      display: block;
      z-index: 1;
    }

    .example-modal .modal {
      background: transparent !important;
    }
  </style> 

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php $this->load->view('kasir/header') ?>
  <!-- Left side column. contains the logo and sidebar -->

<?php $this->load->view('kasir/leftbar') ?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Tables
        <small>advanced tables</small>
      </h1>

      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>

    </section>

    <section class="content">
      <div class="row">

        <div class="col-xs-12">
          <div class="box">
             <div class="box-header">
              <h3 class="box-title">Data Kasir</h3>
            </div>
              <div class="box-header">
                <a class="btn btn-info" href="<?php echo site_url('kasir/pembayaran/input_barcodeTcash') ?>" role="button"><span class="glyphicon glyphicon-plus"> TAMBAH BARCODE</span></a>
              </div>
          <div class="box-header">
          <div class="col-md-4">
               <?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');} ?>
      </div>
        </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th style="text-align: center" width="10">No.</th>
                  <th style="text-align: center">Nomor Telephone</th>
                  <th style="text-align: center">Kode (Barcode)</th>
                  <th style="text-align: center">AKSI</th>
                </tr>
                </thead>
                <tbody>

                  <?php 

                  $dataBarcode = $this->session->all_data;
                  $i = 1;
                  foreach ($dataBarcode as $data) { ?>
                      <tr>
                        <td style="text-align: center"><?php echo $i++ ?></td>
                        <td style="text-align: center"><?php echo $data->Nomor_Telp; ?></td>
                        <td style="text-align: center" ><img src="<?php echo base_url('assets/images/tcash/'.$data->Kode_Tcash); ?>" width="70"></td>
                        <td>
                          <center>
                            <a href="<?php echo site_url() ?>pengelola/karyawan/edit_kasir?Id_tcash=<?php echo $data->Id_tcash; ?>" class="btn btn-sm btn-success"><span class="glyphicon glyphicon-pencil"></span></a>

                            <Button data-toggle="modal" data-target="#confirm-delete<?php echo $i; ?>" class='btn btn-sm btn-danger'><span class="glyphicon glyphicon-trash"></a></Button>

                          </center>
                        </td>
                      </tr>
                   <?php } ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>
    </section>
</div>

<?php $this->load->view('kasir/footer') ?>
</body>
</html>
