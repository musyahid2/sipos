<!DOCTYPE html>
<html>
<?php $this->load->view('kasir/head'); ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
	<?php $this->load->view('kasir/header'); ?>
	<?php $this->load->view('kasir/leftbar'); ?>

  <div class="content-wrapper">
  	<section class="content-header">
  		
  			<div>
  			</div>
  			<div class="row">
		        <div class="col-md-3">
		          <div class="box box-solid">
		            <div class="box-header">
		              <h3 class="box-title text-danger">Bayar Tunai</h3>
		            </div>
		            <!-- /.box-header -->
		            <div class="box-body text-center">
		            	<a href="<?php echo site_url('kasir/pembayaran/pembayarantunai') ?>">
		             	 <img src="<?php echo base_url(); ?>assets/images/pembayaran/money.png" width="75px" height="70px">
		             	</a>
		            </div>
		            <!-- /.box-body -->
		          </div>
		          <!-- /.box -->
		        </div>
	        <!-- /.col -->

	        <?php foreach ($metode_pembayaranWallet as $data) { ?>
	        	<div class="col-md-3">
		          <div class="box box-solid">
		            <div class="box-header">
		              <h3 class="box-title text-blue"><?php echo $data->Nama ?></h3>
		            </div>
		            <!-- /.box-header -->
		            <div class="box-body text-center">
		             <a href="<?php echo site_url()?>/kasir/pembayaran/pembayaranWallet?Id_metodePembayaran=<?php echo $data->Id_metodePembayaran; ?>">
		             	<img src="<?php echo base_url('assets/images/metode_pembayaran/'.$data->Logo); ?>" width="100px" height="70px">
		             </a>
		            </div>
		            <!-- /.box-body -->
		          </div>
		          <!-- /.box -->
		        </div>
	        <?php } ?>

	        <?php foreach ($metode_pembayaranDebit as $data) { ?>
	        	<div class="col-md-3">
		          <div class="box box-solid">
		            <div class="box-header">
		              <h3 class="box-title text-blue"><?php echo $data->Nama ?></h3>
		            </div>
		            <!-- /.box-header -->
		            <div class="box-body text-center">
		             <a href="<?php echo site_url()?>/kasir/pembayaran/pembayaranDebit?Id_metodePembayaran=<?php echo $data->Id_metodePembayaran; ?>">
		             	<img src="<?php echo base_url('assets/images/metode_pembayaran/'.$data->Logo); ?>" width="100px" height="70px">
		             </a>
		            </div>
		            <!-- /.box-body -->
		          </div>
		          <!-- /.box -->
		        </div>
	        <?php } ?>
	      </div> 		
  	</section>
  	
  </div>
<?php $this->load->view('pengelola/footer'); ?>
</div>
</body>
</html>