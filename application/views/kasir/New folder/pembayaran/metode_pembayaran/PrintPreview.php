<!DOCTYPE html>
<html>
<?php $this->load->view('kasir/head'); ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php $this->load->view('kasir/header'); ?>
  <?php $this->load->view('kasir/leftbar'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

      <section class="content">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <center>Ritelpreneur Lab</center>
          </h2>
        </div>
        <!-- /.col -->
      </div>

      <!-- info row -->
      <div class="row invoice-info">

        <div class="col-sm-4 invoice-col">
          <address>
            Kode Transaksi <br>
            Tanggal Transaksi <br>
            Nama Kasir <br>
            Outlet 
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <address>
            : <?php echo $struk->Kode_transaksi ?> <br>
            : <?php echo $struk->Tanggal_transaksi." ".$struk->Waktu_transaksi ?> <br>
            : <?php echo $struk->Nama_kasir?> <br>
            : Enterpreneur Lab <br>
          </address>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Nama Barang</th>
              <th>Qty</th>
              <th>Harga</th>
              <th>Total</th>
            </tr>
            </thead>

            <?php 

                foreach ($struk as $data) {
            ?>

            <tbody>
            <tr>
              <td><?php echo $data->Nama_barang?></td>
              <td><?php echo $data->Jumlah?></td>
              <td><?php echo $data->Harga_satuan?></td>
              <td><?php echo $data->Harga_Total?></td>
            </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        
        <!-- /.col -->
        <div class="col-xs-6">

          <div class="table-responsive">
            <table class="table">
              <tr>
                <th style="width:50%">Subtotal</th>
                <td>: Rp <?php echo $data->Estimasi_pembayaran?></td>
              </tr>
              <tr>
                <th>Diskon (<?php echo $data->Total_diskon; echo '%';?>)</th>
                <td>: Rp <?php echo ROUND(($data->diskon),2)?></td>
              </tr>
              <tr>
                <th>Total</th>
                <td>: Rp <?php echo ROUND(($data->total),2)?></td>
              </tr>
              <tr>
                <th>Tunai</th>
                <td>: Rp <?php echo $data->tunai?></td>
              </tr>
              <tr>
                <th>Kembali</th>
                <td>: Rp <?php echo ROUND(($data->kembalian),2)?></td>
              </tr>
            </table>


          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div>
                  <center>
                    <a href="<?php echo site_url() ?>/kasir/pembayaran/Print?Kode=<?php echo $data->Kode_transaksi ?>" class="btn btn-sm btn-success">Print Struk <span class="glyphicon glyphicon-print" aria-hidden="true"></span></a>
                  </center>
      </div>
    </section>

    </section>


    <!-- Main content -->
   
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <?php $this->load->view('kasir/footer'); ?>
</div>
</body>
</html>