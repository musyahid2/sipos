<?php
$this->load->helper('barang');
ob_start();
    $pdf = new pdf_report('P', 'mm', 'A4', true, 'UTF-8', false);
    $pdf->SetTitle('Print Invoice');
    $pdf->SetHeaderMargin(10);
    $pdf->SetTopMargin(20);
    $pdf->SetLeftMargin(5);
    $pdf->SetRightMargin(5);
    // $pdf->setFooterMargin(20);
    $pdf->SetAutoPageBreak(true);
    $pdf->SetAuthor('Author');
    $pdf->SetDisplayMode('real', 'default');
    $pdf->AddPage();
    $pdf->SetFont('times', 'B', 35, '', 'false');
    $pdf->Cell(190,7,'RITELPRENEUR LAB',0,1,'C');
    $pdf->Cell(190,7,'FAKULTAS KOMUNIKASI BISNIS',0,1,'C');
    $pdf->Ln('1');
    $pdf->SetFont('times', '', 30, '', 'false');
    $data['struk'] = $this->Model_Pembayaran->detailStrukWallet($_GET['Kode_transaksi']);
    $pdf->SetFont('times', 'B', 30, '', 'false');
    $pdf->Cell(100,6,'Kode Transaksi');
    $pdf->SetFont('times', '', 30, '', 'false');
    $pdf->Cell(20,6,': '.$struk[0]->Kode_transaksi);
    $pdf->Ln('1');
    $pdf->SetFont('times', 'B', 30, '', 'false');
    $pdf->Cell(100,6,'Tanggal Transaksi');
    $pdf->SetFont('times', '', 30, '', 'false');
    $pdf->Cell(20,6,': '.$struk[0]->Tanggal_transaksi.' '.$struk[0]->Waktu_transaksi);
    $pdf->Ln('1');
    $pdf->SetFont('times', 'B', 30, '', 'false');
    $pdf->Cell(100,6,'Nama Kasir');
    $pdf->SetFont('times', '', 30, '', 'false');
    $pdf->Cell(20,6,': '.$struk[0]->Nama_kasir);
    $pdf->Ln('1');
    $pdf->SetFont('times', 'B', 30, '', 'false');
    $pdf->Cell(100,6,'Nama Outlet');
    $pdf->SetFont('times', '', 30, '', 'false');
    $pdf->Cell(20,6,': '.'Enterpreneur Lab');
    $pdf->Line(200,130,10,130);
    $pdf->Ln('1');
    $pdf->Ln('1');
    $pdf->SetFont('times', 'B', 30, '', 'false');
    $pdf->Cell(65,6,'Produk',0,0);
    $pdf->Cell(20,6,'Qty',0,0);
    $pdf->Cell(60,6,'Harga',0,0);
    $pdf->Cell(35,6,'Total',0,1);
    $pdf->writeHTML("<hr>", true, false, false, false, '');
    $pdf->SetFont('times', '', 28, '', 'false');
    foreach ($struk as $data) {
        $databrg = printproduct($data->Nama_barang);
        if ($databrg['n']>1) {
           
            for ($i=0; $i < $databrg['n'] ; $i++) { 
                if ($i==0) {
                    $pdf->Cell(65,6,$databrg['label'][($i+1)],0,0);
                    $pdf->Cell(20,6,$data->Jumlah,0,0);
                    $pdf->Cell(60,6,'Rp '.$data->Harga_barang,0,0);
                    $pdf->Cell(35,6,'Rp '.$data->Harga_Total,0,1);
                }
                else
                {
                    $pdf->Cell(65,6,$databrg['label'][($i+1)],0,0);
                    $pdf->Cell(20,6,'',0,0);
                    $pdf->Cell(60,6,'',0,0);
                    $pdf->Cell(35,6,'',0,1);
                }
                
            }       
       }
       else{
            $pdf->Cell(65,6,$data->Nama_barang,0,0);
            $pdf->Cell(20,6,$data->Jumlah,0,0);
            $pdf->Cell(60,6,'Rp '.$data->Harga_barang,0,0);
            $pdf->Cell(35,6,'Rp '.$data->Harga_Total,0,1);
       }
    }
    $pdf->Ln('1');
    $pdf->writeHTML("<hr>", true, false, false, false, '');
    $pdf->SetFont('times', 'B', 30, '', 'false');
    $pdf->Cell(60,6,'Subtotal');
    $pdf->SetFont('times', '', 30, '', 'false');
    $pdf->Cell(20,6,': Rp '.$data->Estimasi_pembayaran);
    $pdf->Ln('1');
    $pdf->SetFont('times', 'B', 30, '', 'false');
    $pdf->Cell(60,6,'Diskon '.$data->Total_diskon);
    $pdf->SetFont('times', '', 30, '', 'false');
    $pdf->Cell(20,6,': Rp '.($data->diskon));
    $pdf->Ln('1');
    $pdf->SetFont('times', 'B', 30, '', 'false');
    $pdf->Cell(60,6,'Total');
    $pdf->SetFont('times', '', 30, '', 'false');
    $pdf->Cell(20,6,': Rp '.($data->Total_bayar));
    $pdf->Ln('1');
    $pdf->SetFont('times', 'B', 20, '', 'false');
    $pdf->Cell(0, 9, 'Pembayaran '.$data->Nama_pembayaran, 0, false, 'R', 0, '', 0, false, 'T', 'M' );
    ob_end_clean();
    $pdf->Ln('1');
    $pdf->writeHTML("<hr>", true, false, false, false, '');
    $pdf->Cell(190,7,'Terima Kasih. Selamat Belanja Kembali',0,1,'C');
    
    $pdf->Output('Struk_Wallet.pdf', 'I');


?>