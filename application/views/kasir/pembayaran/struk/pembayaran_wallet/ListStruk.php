<!DOCTYPE html>
<html>
<?php $this->load->view('kasir/head') ?>
<head>
 
   <style>
    .example-modal .modal {
      position: relative;
      top: auto;
      bottom: auto;
      right: auto;
      left: auto;
      display: block;
      z-index: 1;
    }

    .example-modal .modal {
      background: transparent !important;
    }
  </style> 

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php $this->load->view('kasir/header') ?>
  <!-- Left side column. contains the logo and sidebar -->

<?php $this->load->view('kasir/leftbar') ?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <section class="content">
      <div class="row">

        <div class="col-xs-12">
          <div class="box">
             <div class="box-header">
              <h3 class="box-title">Data Transaksi Wallet</h3>
            </div>
          <div class="box-header">
        </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th><center>Kode Transaksi</center></th>
                    <th><center>Tanggal Transaksi</center> </th>
                    <th><center>Nama Kasir</center></th>
                    <th><center>Total Pembayaran</center></th>
                    <th><center>Detail</center></th>
                  </tr>
                </thead>
                      <tbody>
                  <?php $struk = $this->session->TampilStruk;

                    foreach ($struk as $data) {
                      
                   ?>
                  <tr>
                    <td style="text-align: center"><?php echo $data->Kode_transaksi ?></td>
                    <td style="text-align: center"><?php echo $data->Tanggal_transaksi." ".$data->Waktu_transaksi ?></td>
                    <td style="text-align: center"><?php echo $data->Nama_kasir ?></td>
                    <td style="text-align: center">Rp <?php echo number_format(ROUND(($data->Total_bayar),2),0,',','.') ?></td>
                    <td>
                      <center>
                        <form method="POST" action="<?php echo site_url() ?>/kasir/pembayaran/DetailStrukWallet?Kode_transaksi=<?php echo $data->Kode_transaksi ?>">
                          <button type="submit" name="submit" class="btn btn-sm btn-success">
                            Lihat Struk
                          </button>
                        </form>
                      </center>
                    </td>
                    
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>
    </section>
</div>

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>

  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('assets/template/back/bower_components') ?>/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url('assets/template/back/bower_components') ?>/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('assets/template/back/bower_components') ?>/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="<?php echo base_url('assets/template/back/bower_components') ?>/raphael/raphael.min.js"></script>
<script src="<?php echo base_url('assets/template/back/bower_components') ?>/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url('assets/template/back/bower_components') ?>/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo base_url('assets/template/back/plugins') ?>/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url('assets/template/back/plugins') ?>/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url('assets/template/back/bower_components') ?>/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url('assets/template/back/bower_components') ?>/moment/min/moment.min.js"></script>
<script src="<?php echo base_url('assets/template/back/bower_components') ?>/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url('assets/template/back/bower_components') ?>/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url('assets/template/back/plugins') ?>/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url('assets/template/back/bower_components') ?>/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assets/template/back/bower_components') ?>/fastclick/lib/fastclick.js"></script>
<!-- DataTables -->

<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/template/back/dist') ?>/js/adminlte.min.js"></script>


<script src="<?php echo base_url('assets/template/back/bower_components') ?>/datatables.net/js/jquery-3.1.0.js"></script>
<script src="<?php echo base_url('assets/template/back/bower_components') ?>/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url('assets/template/back/bower_components') ?>/datatables.net/js/dataTables.bootstrap.min.js"></script>
<script>
$(document).ready(function() {
    $('#example1').DataTable( {
        "order": [[0, "desc" ]]
    } );
} );
</script>



</body>
</html>
