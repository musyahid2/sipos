<!DOCTYPE html>
<html>
<?php $this->load->view('kasir/head'); ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php $this->load->view('kasir/header'); ?>
  <?php $this->load->view('kasir/leftbar'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

      <section class="content">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <center>Ritelpreneur Lab</center>
          </h2>
        </div>
        <!-- /.col -->
      </div>

      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <address>
            Kode Transaksi <br>
            Tanggal Transaksi <br>
            Nama Kasir <br>
            Outlet 
          </address>
        </div>
        <!-- /.col -->
        <?php 
          $struk = $this->session->detail_struk;
          
        ?>

        <div class="col-sm-4 invoice-col">
          <address>
            : <?php echo $struk[0]->Kode_transaksi ?> <br>
            : <?php echo $struk[0]->Tanggal_transaksi." ".$struk[0]->Waktu_transaksi ?> <br>
            : <?php echo $struk[0]->Nama_kasir?> <br>
            : Enterpreneur Lab <br>
          </address>
        </div>

        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Nama Barang</th>
              <th>Qty</th>
              <th>Harga</th>
              <th>Total</th>
            </tr>
            </thead>
            <?php 
              $struk = $this->session->detail_struk;
              foreach ($struk as $data) {
            ?>
            <tbody>
            <tr>
              <td><?php echo $data->Nama_barang?></td>
              <td><?php echo $data->Jumlah?></td>
              <td>Rp <?php echo $data->Harga_barang?></td>
              <td>Rp <?php echo $data->Harga_Total?></td>
            </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        
        <!-- /.col -->
        <div class="col-xs-6">

          <div class="table-responsive">
            <?php 
              $struk = $this->session->detail_struk;
            ?>
            <table class="table">
              <tr>
                <th style="width:50%">Subtotal</th>
                <td>: Rp <?php echo $struk[0]->Estimasi_pembayaran?></td>
              </tr>
              <tr>
                <th>Diskon</th>
                <td>: Rp <?php echo $struk[0]->diskon?></td>
              </tr>
              <tr>
                <th>Total</th>
                <td>: Rp <?php echo ($struk[0]->Total_bayar)?></td>
              </tr>
            </table>

          </div>
        </div>
        <!-- /.col -->
      </div>
      <div>
        <?php 
           $struk = $this->session->detail_struk;
           ?>
        <center>
            <a href="<?php echo site_url() ?>/kasir/pembayaran/PrintStrukWallet?Kode=<?php echo $struk[0]->Kode_transaksi ?>" target="_blank" class="btn btn-sm btn-success">Print Struk <span class="glyphicon glyphicon-print" aria-hidden="true"></span></a>
        </center>
      </div>
    </section>

    </section>

  </div>
  
  <?php $this->load->view('kasir/footer'); ?>
</div>
</body>
</html>