<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Form SPH</title>
    <link rel="stylesheet" href="">
    <style>
        .page-header, .page-header-space {
            height: 40px;
        }
        .space{
            width: 100%;
            height: 20px;
            display: inline-block;
        }
        .row{
            width: 100%;
            font-size: 9.1px;
            font-family: Calibri, serif;
            display: inline-block;
            line-height: 10px;
        }

        .page-footer, .page-footer-space {
            height: 0px;

        }
        .page-footer {
            position: fixed;
            bottom: 0;
            width: 100%;
        }
        .page-header {
            position: fixed;
            top: 10px;
            width: 100%;
            font-size: 10px;
            font-style: Calibri, serif;
            font-weight: bold;
        }
        .page {
            page-break-after: always;

        }
        @page {
            margin-top: 3mm;
            margin-left: 25mm;
            margin-right: 20mm;
            /*margin: 20mm*/
        }
        @media print {
            thead {display: table-header-group;} 
            tfoot {display: table-footer-group;}

            button {display: none;}

            body {margin: 0;}
        }

        .tg  {border-collapse:collapse;border-spacing:0;}
        .tg td{font-family:Calibri, serif;font-size:9.1px;padding:5px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:black;}
        .tg th{font-family:Calibri, serif;font-size:9.1px;font-weight:normal;padding:5px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:black;}
        .tg .tg-hysb{font-weight:bold;font-size:12px;text-align:left}
        .tg .tg-8oiy{font-size:12px;text-align:left}
        .tg .tg-z9od{font-size:12px;text-align:left;vertical-align:top}
        .tg .tg-kr94{font-size:12px;text-align:center}
        .tg .tg-pi53{font-weight:bold;font-size:12px;text-align:center}
        .tg .tg-ir4y{font-weight:bold;font-size:12px;text-align:center;vertical-align:top}
        .tg .tg-rg0h{font-size:12px;text-align:center;vertical-align:top}
        @media screen and (max-width: 767px) {.tg {width: auto !important;}.tg col {width: auto !important;}.tg-wrap {overflow-x: auto;-webkit-overflow-scrolling: touch;}}
                
    </style>
</head>
<body style="border:0px;  width: 100%">
    <div class="page-header" style="">       
        <center>RITELPRENEUR LAB</center>
        <center>FAKULTAS KOMUNIKASI BISNIS</center>
        <hr style="border:0.5px solid black">
    </div>
    <div class="page-footer"></div>
    <table>
        <thead>
            <tr>
                <td>
                    <div class="page-header-space">
                    </div>
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <!--*** CONTENT GOES HERE ***-->
                    <div class="page" style="line-height: 1.5; font-size: 12pt; font-family: Calibri,serif;margin-top: 12px;">
                        <div class="row">Transaksi : <?= $Kode_transaksi ?></div>
                        <div class="row">Tanggal : <?= $Tanggal_transaksi." ".$Waktu_transaksi ?></div>
                        <div class="row">Nama Kasir : <?= $Nama_kasir ?></div>
                        <div class="row">Nama Outlet : Enterpreneur Lab</div>
                        <div class="row" style="margin-left: -10px;"><hr style="border:0.5px dotted black; width: 108%"></div>
                        <div class="row">
                            <div class="tg-wrap">
                                <table class="tg" style="width: 100%">
                                    <tr>
                                        <?php 
                                            $no = 1;
                                            $total = 0;
foreach ($data_produk as $value){
                                        ?>
                                        <tr>
                                            <td><?= $value->Nama_barang ?></td>
                                            <td><?= $value->Jumlah ?></td>
                                            <td style="text-align: right;"><?= number_format($value->Harga_barang, 0,",",".") ?></td>
                                            <td style="text-align: right;"><?= number_format($value->Harga_total, 0,",",".") ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row" style="margin-left: -10px;"><hr style="border:0.5px dotted black; width: 108%"></div>
                        <div class="row">Subtotal : Rp <?= number_format($value->Estimasi_pembayaran, 0,",",".") ?></div>
                        <div class="row">Diskon : Rp <?= number_format($value->diskon, 0,",",".") ?></div>
                        <div class="row">Total : Rp <?= number_format($value->Total_bayar, 0,",",".") ?></div>
                        <div class="row">Bayar : Rp <?= number_format($value->tunai, 0,",",".") ?></div>
                        <div class="row">Kembali : Rp <?= number_format($value->kembali, 0,",",".") ?></div>
                        <div><hr style="border:0.5px solid black"></div>
                    </div>
                </td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td>
                    <!--place holder for the fixed-position footer-->
                    <div class="page-footer-space" style="font-style: Calibri,serif;font-size: 9.1px;text-align: center;">Terima Kasih Atas Kunjungan Anda</div>
                </td>
            </tr>
        </tfoot>
    </table>
</body>
<script type="text/javascript">
      window.onload = function() { window.print(); }
 </script>
</html>