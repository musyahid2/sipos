<!DOCTYPE html>
<html>
<?php $this->load->view('kasir/head') ?>
<head>
  <style type="text/css">
    .bayar {
      width: 100%;
    }
  </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php $this->load->view('kasir/header') ?>


<?php $this->load->view('kasir/leftbar') ?>

  <div class="content-wrapper">
    <section class="content">
      <div class="row">
       
        <div class="col-md-12">

          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">PEMBAYARAN WALLET</h3>
            </div>
            <form role="form" action="<?php echo base_url(); ?>kasir/pembayaran/inputWallet" method="POST">
              <div class="box-footer">
                  <div class="box-body">
                    <div class="col-md-6">
                  <div class="form-group">
                    <label for="exampleInputEmail1">ID Pembayaran</label>
                      <input type="text" class="form-control" id="exampleInputEmail1" placeholder="ID Pembayaran" value="<?php echo $id_pembayaran;?>" name="id_pembayaran" readonly>
                      </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Kode Transaksi</label>
                      <input type="text" class="form-control" id="exampleInputEmail1" placeholder="ID Pembayaran" value="<?php echo $kode_transaksi;?>" name="kode_transaksi" readonly>
                      </div>
                      <div class="form-group">
                      <label for="exampleInputEmail1">Total Tagihan</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Total Tagihan" value="Rp <?php echo $this->session->userdata('total') ?>" name="" readonly>
                    </div>
                     <div class="form-group">
                        <input type="hidden" class="form-control" id="exampleInputEmail1" placeholder="Tunai Pembayaran" value="<?php echo $this->session->userdata('total2') ?>" name="total_tagihan" readonly>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Jumlah Barang</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Tunai Pembayaran" value="<?php echo $this->session->userdata('jumlah') ?>" name="jumlah_barang" readonly>
                    </div>
                  <div class="form-group"> 
                  <label for="exampleInputPassword1">Diskon</label>
                  <select name="diskon" class="form-control">
                    <?php

                          $dataPromo = $this->session->diskon;
                          //print_r($dataSiswa);

                          foreach ($dataPromo as $data) { //ngabsen data
                            echo "<option value='". $data->Kode_Diskon."'>".$data->Kode_Diskon." - ". $data->Nama_Diskon."</option>";
                          }
                      ?>
                  </select>
                </div> 
                <div class="form-group">
                  <label for="exampleInputEmail1">Nomor Transaksi</label>
               
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Nomor Transaksi" name="nomor_transaksi">
                </div>
               
                    <input type="hidden" class="form-control" id="exampleInputEmail1" value="<?php echo $pembayaranWallet[0]->Nama ?>" name="nama_pembayaran">
                   
                  
                     <button type="submit" class="btn btn-info bayar">INPUT PEMBAYARAN</button>
          

                </div>
                  <div class="col-md-6">
                  <?php
                            foreach ($pembayaranWallet as $data) { ?>
                            <img src="<?php echo base_url('assets/images/metode_pembayaran/'.$data->Foto); ?>" width="400">
                          
                          <?php } ?>
           
    
                </div>     
              </div>
            </div>

            </div>
         </form>
          <!-- /.box -->


        </div>

        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
<?php $this->load->view('kasir/footer') ?>
<script type="text/javascript">
   //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })
</script>

</body>
</html>
