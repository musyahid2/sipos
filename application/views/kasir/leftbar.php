  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url('assets/images/kasir/'.$this->session->userdata("foto")); ?>" >
        </div>
        <div class="pull-left info">
          <p><?php echo $this->session->userdata("nama") ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> <?php echo $this->session->userdata("id_kas") ?></a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active treeview">
          <a href="<?php echo site_url('kasir/home') ?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard Transaksi</span>  
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo site_url('kasir/home') ?>"><i class="fa fa-circle-o"></i> Dashboard</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="<?php echo site_url('kasir/keranjang/daftar_item') ?>">
            <i class="fa fa-laptop"></i>
            <span>Keranjang</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo site_url('kasir/keranjang') ?>"><i class="fa fa-circle-o"></i> Daftar Item Barang</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Pembayaran</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo site_url('kasir/pembayaran/StrukTunai') ?>"><i class="fa fa-circle-o"></i> List Struk Tunai</a></li>
            <li><a href="<?php echo site_url('kasir/pembayaran/StrukWallet') ?>"><i class="fa fa-circle-o"></i> List Struk Wallet</a></li>
            <li><a href="<?php echo site_url('kasir/pembayaran/StrukDebit') ?>"><i class="fa fa-circle-o"></i> List Struk Debit</a></li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>