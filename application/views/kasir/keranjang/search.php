<!DOCTYPE html>
<html>
<head>
  <style type="text/css">
    .thumbnail {
      height: 140px;
    }
    .caption {
      margin-top: -20px;
      text-align: center;
    }
    .caption p {
      margin-top: -10px;
    }
    input[type=text] {
      padding: 8px 2px;
      box-sizing: border-box;
      font-size: 15px;
    }


    .listitem input {
      border: none;
    }
    .total input {
      border: none;
    }

    .col-md-3 {
      font-size: 16px;
    }

    hr {
      border-top: 20px;
    }

  
    .tt {
      margin-top: 15px;
    }
  </style>
  <script type="text/javascript">
     var baseurl = "<?php echo base_url("index.php/"); ?>";
  </script>
</head>
<?php $this->load->view('kasir/head'); ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php $this->load->view('kasir/header'); ?>
  <?php $this->load->view('kasir/leftbar'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
   
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-6">
               <div class="box">
            <div class="box-header">
              <h3 class="box-title">Daftar Item Barang</h3>
            </div>
            <div class="box-header">
              <form role="form" action="">
              <div class="input-group input-group-sm">
                <input type="text" class="form-control" placeholder="Produk / SKU / Barcode" id="kode" name="kode">
                    <span class="input-group-btn">
                      <button type="submit" class="btn btn-info btn-flat">Cari Produk</button>
                    </span>
              </div>
            
            </form>
            </div>
                <div class="box-body">
                  <div class="row">

                    <?php 

                      foreach ($dataproduk as $data) { ?>
                <div class="col-md-3 col-xs-2" >
                    <div class="thumbnail tt">
                    <img src="<?php echo base_url('assets/images/produk/'.$data->Gambar_barang); ?>" class="card-img-top" alt="...">
                  </div><script type="text/javascript"></script>
                <div class="caption">
                  <h5><?php echo $data->Nama_barang; ?></h5>
                  <p>Rp <?php echo $data->Harga_barang; ?></p>
                  <input class="form-control input-number" align="center" type="number" id="kty_<?php echo $data->Barcode?>" name="" value="1" min="1">
                </div>
                 <button href="#"  class="test btn btn-info" id="<?php echo $data->Barcode?>" role="button" title="<?php echo $data->Nama_barang;?>||<?php echo $data->Harga_barang?>||<?php echo $data->Barcode;?>">TAMBAH PRODUK
                  </button>
                  </div>

                    <?php } ?>

                  </div>
                </div>
                <div id="frm">
                  <input type="hidden" id="pname" name=""><br>
                  <input type="hidden" id="pqty" name=""><br>
                  <input type="hidden" id="price" name=""><br>
                  <input type="hidden" id="harga" name=""><br>
                  <input type="hidden" id="pbarcode" name=""><br>
                </div>
          </div>
        </div>
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Daftar Belanja</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
              <div class="box-body" style="text-align: center;">

                <div class="col-md-3">PRODUK</div>
                <div class="col-md-3">QUANTITY</div>
                 <div class="col-md-3">HARGA TOTAL</div>
              </div>
              <div class="box-body">
              <div id="cart"></div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                
              </div>
          </div>
        </div>
      </div>
    </div>
      <!-- /.row -->
    </section>

    

  <!-- /.content-wrapper -->

  <?php $this->load->view('kasir/footer'); ?>
</div>
</body>
<script type="text/javascript" src="<?php echo base_url('assets/template/back/js') ?>/jquery-3.3.1.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
       $('#kode').on('input',function(){
                
                var kode=$(this).val();
                $.ajax({
                    type : "POST",
                    url  : "<?php echo base_url('keranjang/search')?>",
                    dataType : "JSON",
                    data : {kode: kode},
                    cache:false,
                    success: function(data){
                        $.each(data,function(Barcode, Nama_barang, Harga_barang){
                            $('[name="nama"]').val(data.Nama_barang);
                            $('[name="harga"]').val(data.Harga_barang);
                          
                            
                        });
                        
                    }
                });
                return false;
           });

    });
  </script>
<script type="text/javascript">

inames = []
iqytp = []
iprice = []
iharga = []
ibarcode = []

$(".test").on('click', function() {






  var x= $(this).attr("title");
  var id=$(this).attr("id");

  // var ribuan = document.getElementById("price").value = ((x.split('||')[1])) ;
  // var reverse = ribuan.toString().split('').reverse().join(''),
  // harga  = reverse.match(/\d{1,3}/g);
  // harga  = harga.join('.').split('').reverse().join('');


   var kuantiti = parseInt(document.getElementById("kty_"+id).value);
   var qty = document.getElementById("pqty").value = kuantiti;
   var nama = document.getElementById("pname").value = ((x.split('||')[0]));
  var harga = document.getElementById("harga").value = ((x.split('||')[1]));
   var barcode = document.getElementById("pbarcode").value = ((x.split('||')[2]));
//alert(kuantiti);


  inames.push(nama)
  iqytp.push(kuantiti)
  iprice.push(harga)
  iharga.push(harga)
  ibarcode.push(barcode)



  displayCart()
});

// function addItem() {
//  inames.push(document.getElementById('pname').value)
//  iqytp.push(parseInt(document.getElementById('pqty').value))
//  iprice.push(parseInt(document.getElementById('price').value))

//  displayCart()

// }

function displayCart() {
  cartdata = '<form role="form" action="<?php echo site_url();?>/kasir/keranjang/prosesBayar" method="post">';

  total = 0;
  jumlah = 0;
  for (i = 0; i<inames.length; i++) {

      var bilangan = iqytp[i] * iprice[i] 
      var reverse = bilangan.toString().split('').reverse().join(''),
        ribuan  = reverse.match(/\d{1,3}/g);
        ribuan  = ribuan.join('.').split('').reverse().join('');

    // total += iqytp[i] * iprice[i]



     var total = total + iqytp[i] * iprice[i] 
      var reverse = total.toString().split('').reverse().join(''),
        hasil  = reverse.match(/\d{1,3}/g);
        hasil  = hasil.join('.').split('').reverse().join('');

    jumlah += iqytp[i] 
    // cartdata += "<tr><td>" + inames[i] + "</td><td>" + iqytp[i] + "</td><td>" + iprice[i] + "</td><td>" + iqytp[i] * iprice[i] + "</td><td><button onclick='delElement(" + i +")'>DELETE</button></td></tr>"
    cartdata +='<div class="listitem"><input type="text" value=' + inames[i]  + ' name="nama[]" style="text-align: center" size="23" ><input type="text" value=' + iqytp[i] + ' name="quantiti[]" size="1"><input type="text" value=' + ribuan + ' name="" style="text-align:right"><input type="hidden" value=' + bilangan + ' name="harga[]" size="1"><input type="hidden" value=' + iprice[i] + ' name="hargasatuan[]" size="1"><input type="hidden" value=' + ibarcode[i] + ' name="barcode[]" size="15"><button class="glyphicon glyphicon-remove btn btn-danger" onclick="delElement(' + i +')""></button></div><div></div>'
  }


  cartdata +='<hr><div class="total"><input type="" name=""><input value="Total : " style="text-align:right;   font-weight: bold"><input type="text" style="font-weight : bold" value=' + hasil + ' size="15" name="total[]"><input type="hidden" value=' + total + ' name="total2[]" size="15"><input type="" name=""><input value="Jumlah Item : " style="text-align:right"><input type="text" value=' + jumlah + ' name="jumlah[]" size="15" ><br><br><button type="submit" class="btn btn-info">BAYAR</button></div></form>'

  document.getElementById('cart').innerHTML = cartdata

}

function delElement(a) {
  inames.splice(a, 1);
  iqytp.splice(a, 1)
  iprice.splice(a, 1)
  displayCart()
}


</script>
</html>
