<!DOCTYPE html>
<html>
<head>
  <style type="text/css">
    .thumbnail {
      height: 256px;
    }
    .caption {
      margin-top: -20px;
      text-align: center;
    }
    .caption p {
      margin-top: -10px;
    }
    input[type=text] {
      padding: 8px 2px;
      box-sizing: border-box;
      font-size: 15px;
    }


   .listitem {
      border: none;
    }
    .total input {
      border: none;
    }

    .col-md-3 {
      font-size: 16px;
    }

    hr {
      border-top: 20px;
    }

  
    .tt {
      margin-top: 15px;
    }

    #my_form {
      margin-bottom: 10px;
    }
  </style>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<?php $this->load->view('kasir/head'); ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php $this->load->view('kasir/header'); ?>
  <?php $this->load->view('kasir/leftbar'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
   
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-6">
               <div class="box">
            <div class="box-header">
              <h3 class="box-title">Daftar Item Barang</h3>

            <div class="box-header">

              <form id="my_form" name="form1"  action="<?php echo base_url() ?>/kasir/keranjang/serach_name">
                <div class="input-group">
                <div class="input-group-btn">
                  <button type="button" class="btn btn-danger">KATA KUNCI</button>
                </div>
                <input type="text" class="form-control" onkeyup="searchName();" placeholder="Cari dengan kata kunci" name="fname" style="width:355px">
              </div>              
               </form>
            <div id="search_table">
              <!-- Menampilkan data dari ajax dari view daftar_item-->
            </div>
            </div>
          </div>
        </div>
        </div>
         <div class="col-md-6">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Daftar Belanja</h3>
            </div>
              <div class="box-body" style="text-align: center;">

                <div class="col-md-3">PRODUK</div>
                <div class="col-md-3">QUANTITY</div>
                 <div class="col-md-3">HARGA TOTAL</div>
              </div>
              <div class="box-body">
              <div id="cart"></div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
              
              </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  </body>
   <?php $this->load->view('pengelola/footer') ?>

<script>
function searchName(){
  xmlhttp= new XMLHttpRequest();
  xmlhttp.open("GET", "keranjang/search_name?name="+document.form1.fname.value,false);
  xmlhttp.send(null);
  document.getElementById("search_table").innerHTML=xmlhttp.responseText;

  
}
$(document).ready(function(e) {
      $.ajaxSetup({cache:false});
      $('#search_table').load('keranjang/display_info');
    });
</script>  

     
</html>
