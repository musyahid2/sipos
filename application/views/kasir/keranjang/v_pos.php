<!DOCTYPE html>
<html>
<head>
    <title>Point</title>

</head>
<body>
    <div class="container">
        <div class="col-md-12 col-md-offset-1">
        <hr/>
            <form class="form-horizontal" action="<?php echo base_url('kasir/pos/get_barang') ?>" method="POST">
                <div class="form-group">
                    <label class="control-label col-xs-3" >Kode Barang</label>
                    <div class="col-xs-9">
                        <input name="kode" id="kode" class="form-control" type="text" placeholder="Kode Barang..." style="width:335px;">
                    <!--     <input type="submit" name=""> -->
                    </div>
                </div>
                <table>
                    <tr>
                        <td>Nama : </td>
                        <h1 id="namaku"></h1>
                    </tr>
                    <tr>
                        <td>Harga : </td>
                        <p id="harga"></p>
                    </tr>
                    <tr>
                        <td>Gambar : </td>
                        <td><img src=""></td>
                    </tr>
                </table>
        
            </form>
        </div>
    </div>

    <script type="text/javascript" src="<?php echo base_url().'assets/template/back/js/jquery.js'?>"></script>
    <script type="text/javascript">
        $(document).ready(function(){
             $('#kode').on('input',function(){
                
                var kode=$(this).val();
                $.ajax({
                    type : "POST",
                    url  : "<?php echo base_url('index.php/kasir/pos/get_barang')?>",
                    dataType : "JSON",
                    data : {kode: kode},
                    cache:false,
                    success: function(data){
                        $.each(data,function(Barcode, Nama_barang, Harga_barang, Gambar_barang){
                            $("#namaku").html(data.Nama_barang);
                            $("#harga").html(data.Harga_barang);
                            $("img").attr("src", data.Gambar_barang);
                        });
                        
                    }
                });
                return false;
           });

        });
    </script>
</body>
</html>