<!DOCTYPE html>
<html>
<?php $this->load->view('kasir/head') ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php $this->load->view('kasir/header') ?>
  <!-- Left side column. contains the logo and sidebar -->

<?php $this->load->view('kasir/leftbar') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard Transaksi
<!--         <small>Control panel</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="col-md-12">
                 <?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');} ?>
        </div>
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h4><b>BUKA KASIR</b></h4>

              <p>Masukkan Saldo Awal</p>
            </div>
            <div class="icon">
              <i class="ion ion-filing"></i>
            </div>
            <a href="<?=base_url();?>index.php/kasir/BukaKasir" class="small-box-footer">Buka Kasir <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h4>Tutup Kasir</h4>

              <p>Masukkan Saldo Akhir</p>
            </div>
            <div class="icon">
              <i class="ion ion-key"></i>
            </div>
            <a href="<?=base_url();?>index.php/kasir/TutupKasir" class="small-box-footer">Tutup Kasir & logout <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php $this->load->view('kasir/footer') ?>
</body>
</html>
