<!DOCTYPE html>
<html>
<?php $this->load->view('pengelola/head') ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php $this->load->view('pengelola/header') ?>
  <!-- Left side column. contains the logo and sidebar -->

<?php $this->load->view('pengelola/leftbar') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard Transaksi
<!--         <small>Control panel</small> -->
      </h1>
      <?php 


       ?>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
        <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <?php   $dataProduk = $this->session->jumlahProduk;
                foreach ($dataProduk as $data) {
               ?>
              <h3><?php echo $data->Jumlah ?></h3>
            <?php } ?>
              <p>PRODUK</p>
            </div>
            <div class="icon">
              <i class="ion ion-cube"></i>
            </div>
            <a href="<?php echo site_url('pengelola/produk') ?>" class="small-box-footer">List Produk <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <?php   $dataKaryawan = $this->session->jumlahKaryawan;
                foreach ($dataKaryawan as $data) {
               ?>
               <h3><?php echo $data->Jumlah ?></h3>
              <?php } ?>
              <p>KARYAWAN</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="<?php echo base_url('pengelola/karyawan') ?>" class="small-box-footer">List Karyawan <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <?php   $dataTransaksi = $this->session->dataTransaksi;
                foreach ($dataTransaksi as $data) {
               ?>
              <h3><?php echo $data->Jumlah ?></h3>
              <?php } ?>
              <p>Transaksi Hari Ini</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="<?php echo site_url() ?>pengelola/laporan/transaksiPenjualan" class="small-box-footer">List Transaksi <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
              <div class="inner">
               <?php   $totalTransaksi = $this->session->totalTransaksi;
                foreach ($totalTransaksi as $data) {
               ?>
              <h3>Rp. <?php echo number_format( $data->Jumlah, 0,",","." )?></h3>
               <?php } ?>
              <p>Omset Hari ini</p>
            </div>
            <!-- <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div> -->
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
                  
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
          <!-- Custom tabs (Charts with tabs)-->
           <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">5 Produk Terlaris Hari ini</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th style="text-align: center">Nama Produk</th>
                    <th style="text-align: center">Jumlah Pembelian</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php $produk_terlaris = $this->session->produkTerlaris; 
                  foreach ($produk_terlaris as $data) {
                  ?>
                  <tr>
                    <td style="text-align: center"><?php echo $data->Nama_barang; ?></td>
                    <td style="text-align: center"><?php echo $data->Jumlah; ?></td>
                  </tr>
                  <?php } ?>
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <a href="<?php echo site_url('pengelola/produk') ?>" class="btn btn-sm btn-info btn-flat pull-left">Lihat Semua Produk</a>
              <a href="<?php echo site_url() ?>pengelola/laporan/transaksiPenjualan" class="btn btn-sm btn-default btn-flat pull-right">Lihat Semua Transaksi</a>
            </div>
            <!-- /.box-footer -->
          </div>






        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
         <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Kategori Terlaris Hari ini</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th style="text-align: center">Nama Kategori</th>
                    <th style="text-align: center">Jumlah Pembelian</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php $kategori_terlaris = $this->session->kategoriTerlaris; 
                  foreach ($kategori_terlaris as $data) {
                  ?>
                  <tr>
                    <td style="text-align: center"><?php echo $data->Nama_kategori; ?></td>
                    <td style="text-align: center"><?php echo $data->Jumlah; ?></td>
                  </tr>
                  <?php } ?>
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
          </div>

        </section>
      </div>
    </section>



    <!-- Main content -->
   
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php $this->load->view('pengelola/footer') ?>

</body>
</html>
