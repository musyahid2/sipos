  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url('assets/images/pengelola/'.$this->session->userdata("foto")); ?>" class="user-image" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $this->session->userdata("nama"); ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="">
          <a href="<?php echo site_url() ?>pengelola/home">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>Laporan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo site_url() ?>pengelola/laporan/penjualanProduk"><i class="fa fa-circle-o"></i> Penjualan Produk</a></li>
            <li><a href="<?php echo site_url() ?>pengelola/laporan/transaksiPenjualan"><i class="fa fa-circle-o"></i> Transaksi Penjualan</a></li>
            <li><a href="<?php echo site_url() ?>pengelola/laporan/penjualanKategori"><i class="fa fa-circle-o"></i> Penjualan Kategori</a></li>
            <li><a href="<?php echo site_url() ?>pengelola/laporan/labaProduk"><i class="fa fa-circle-o"></i> Laba</a></li>
            <li><a href="<?php echo site_url() ?>pengelola/laporan/historykasir"><i class="fa fa-circle-o"></i> History Kasir</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Kelola Produk</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo site_url('pengelola/produk') ?>"><i class="fa fa-circle-o"></i> Produk</a></li>
            <li><a href="<?php echo site_url('pengelola/kategori') ?>"><i class="fa fa-circle-o"></i> Kategori</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Inventori Produk</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('pengelola/stok/kartu_stok') ?>"><i class="fa fa-circle-o"></i> Kartu Stok</a></li>
            <li><a href="<?php echo base_url('pengelola/stok/Data_stokmasuk')?>"><i class="fa fa-circle-o"></i> Stok Masuk</a></li>
            <li><a href="<?php echo base_url('pengelola/stok/Data_stokkeluar')?>"><i class="fa fa-circle-o"></i> Stok Keluar</a></li>
            <li><a href="<?php echo base_url('pengelola/stok/data_stokOpname') ?>"><i class="fa fa-circle-o"></i> Stok Opname</a></li>
          </ul>
        </li>
         <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Kelola Karyawan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
         <ul class="treeview-menu">
            <li><a href="<?php echo base_url('pengelola/karyawan') ?>"><i class="fa fa-circle-o"></i> Data Kasir</a></li>
            <li><a href="<?php echo base_url('pengelola/pengelola') ?>"><i class="fa fa-circle-o"></i> Data Pengelola</a></li>
        </ul>
      </li>
      <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Kelola Diskon</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
         <ul class="treeview-menu">
            <li><a href="<?php echo base_url('pengelola/diskon/index') ?>"><i class="fa fa-circle-o"></i> Input Diskon </a></li>
            <li><a href="<?php echo base_url('pengelola/diskon/daftar_diskon_pending') ?>"><i class="fa fa-circle-o"></i> Data Diskon Pending</a></li>
            <li><a href="<?php echo base_url('pengelola/diskon/daftar_diskon_aktif') ?>"><i class="fa fa-circle-o"></i> Data Diskon Aktif</a></li>
            
        </ul>
      </li>
            <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Metode Pembayaran</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
         <ul class="treeview-menu">
            <li><a href="<?php echo base_url('pengelola/metode_pembayaran/data_wallet') ?>"><i class="fa fa-circle-o"></i> Data Wallet </a></li>
            <li><a href="<?php echo base_url('pengelola/metode_pembayaran/data_debit') ?>"><i class="fa fa-circle-o"></i> Data Debit</a></li>
        </ul>
      </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
