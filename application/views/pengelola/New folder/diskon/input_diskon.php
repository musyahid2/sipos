<!DOCTYPE html>
<html>
<?php $this->load->view('pengelola/head') ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php $this->load->view('pengelola/header') ?>
  <!-- Left side column. contains the logo and sidebar -->

<?php $this->load->view('pengelola/leftbar') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Input Diskon
<!--         <small>Control panel</small> -->
      </h1>
      <?php 


       ?>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-4 col-xs-6">
         <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">              
              <h2>DISKON <br> SEMUA TRANSAKSI</h2>
              <p>Persentase (%)</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="<?php echo base_url()?>pengelola/diskon/all_diskon_persen" class="small-box-footer">Input Diskon</a>
          </div>
        </div>

        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
               <h2>DISKON <br> SEMUA TRANSAKSI</h2>
              <p>Potongan Harga (Rp)</p>
            </div>
            <div class="icon">
              <i class="ion ion-cube"></i>
            </div>
            <a href="<?php echo base_url()?>pengelola/diskon/all_diskon_rupiah" class="small-box-footer">Input Diskon</a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h2>DISKON <br> SEMUA TRANSAKSI</h2>
              <p>Persentase (%) & Maksimal Diskon (Rp) </p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="<?php echo base_url()?>pengelola/diskon/input_all_persen_max" class="small-box-footer">Input Diskon</a>
          </div>
        </div>

        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h2>DISKON <br> MINIMAL TRANSAKSI</h2>
              <p>Persentase (%)</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="<?php echo base_url()?>pengelola/diskon/min_trans_persen" class="small-box-footer">Input Diskon</a>
          </div>
        </div>

        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-purple">
            <div class="inner">
              <h2>DISKON <br> MINIMAL TRANSAKSI</h2>
              <p>Potongan Harga (Rp)</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="<?php echo base_url()?>pengelola/diskon/min_trans_rupiah" class="small-box-footer">Input Diskon</a>
          </div>
        </div>

        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-gray">
            <div class="inner">
              <h2>DISKON <br> MINIMAL TRANSAKSI</h2>
              <p>Persentase (%) & Maksimal Diskon (Rp)</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="<?php echo base_url()?>pengelola/diskon/min_trans_persen_max_diskon" class="small-box-footer">Input Diskon</a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
                  
      
              <!-- /.table-responsive -->
            </div>
          </div>

        </section>
      </div>
    </section>



    <!-- Main content -->
   
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php $this->load->view('pengelola/footer') ?>

</body>
</html>
