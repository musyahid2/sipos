<!DOCTYPE html>
<html>
<?php $this->load->view('pengelola/head') ?>
<head>
 
   <style>
    .example-modal .modal {
      position: relative;
      top: auto;
      bottom: auto;
      right: auto;
      left: auto;
      display: block;
      z-index: 1;
    }

    .example-modal .modal {
      background: transparent !important;
    }
  </style> 

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php $this->load->view('pengelola/header') ?>
  <!-- Left side column. contains the logo and sidebar -->

<?php $this->load->view('pengelola/leftbar') ?>

<div class="content-wrapper">

    <section class="content">
      <div class="row">

        <div class="col-xs-12">
          <div class="box">
             <div class="box-header">
              <h3 class="box-title">Data Diskon Pending</h3>
            </div>
              <div class="box-header">
              <a class="btn btn-info" href="<?php echo site_url('pengelola/diskon/index') ?>" role="button"><span class="glyphicon glyphicon-plus"> TAMBAH DISKON</span></a>
            </div>
          <div class="box-header">
            <div class="col-md-4">
                 <?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');} ?>
            </div>
          </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                 <thead>
                  <tr>
                    <th style="text-align: center" width="10">NO.</th>
                    <th style="text-align: center" >Kode Diskon</th>
                    <th style="text-align: center" >Nama Diskon</th>
                    <th style="text-align: center" >Status</th>
                    <th style="text-align: center" >Aksi</th>
                  </tr>
                </thead>
                <tbody>

                            <?php $pending = $this->session->diskon_pending;
              $i = 1;

                foreach ($pending as $data) {

               ?>
              <tr>
                <td style="text-align: center" ><?php echo $i++ ?></td>
                <td style="text-align: center" ><?php echo $data->Kode_Diskon ?></td>
                <td style="text-align: center" ><?php echo $data->Nama_Diskon ?></td>
                <td style="text-align: center" ><?php echo $data->Status ?></td>
                <td style="text-align: center" >
                  <center>
                  <!-- <form method="POST" action="<?php echo site_url() ?>pengelola/produk/approve_diskon?Kode_Diskon=<?php echo $data->Kode_Diskon; ?>">
                    <button type="submit" class="btn btn-round btn-check" name="edit">Aktifkan</button> 
                  </form> -->
                  <a href="<?php echo site_url() ?>pengelola/diskon/approve_diskon?Kode_Diskon=<?php echo $data->Kode_Diskon; ?>" class="btn btn-sm btn-success">Aktifkan <span class="glyphicon glyphicon-check"></span></a>

                  <Button data-toggle="modal" data-target="#confirm-delete<?php echo $i; ?>" class='btn btn-sm btn-danger'><span class="glyphicon glyphicon-trash"></a></Button>

                   <div class="modal fade" id="confirm-delete<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        Konfirmasi Hapus
                                    </div>
                                    <div class="modal-body">
                                        Apakah yakin ingin menghapus data?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                        <a href="<?php echo site_url() ?>/pengelola/dis/hapus_diskon_pending?Kode_Diskon=<?php echo $data->Kode_Diskon ?>" class="btn btn-danger btn-ok" >Delete</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                  </center>
                </td>
              </tr>
                   <?php } ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>
    </section>
</div>

<?php $this->load->view('pengelola/footer') ?>
</body>
</html>
