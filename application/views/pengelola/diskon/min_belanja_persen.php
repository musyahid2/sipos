<!DOCTYPE html>
<html>
<?php $this->load->view('pengelola/head') ?>
<head>
  <style type="text/css">
    .tombol {
      
      margin: 0 0 100px 400px;
    }
  </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php $this->load->view('pengelola/header') ?>
  <!-- Left side column. contains the logo and sidebar -->

<?php $this->load->view('pengelola/leftbar') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
    
        <!-- left column -->
        <div class="col-md-12">

          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">INPUT DISKON DALAM PERSENTASE (%) (MINIMAL TRANSAKSI)</h3>
            </div>

            <?php if (validation_errors()) : ?>
                <div class="alert alert-danger">
                  <?php echo validation_errors(); ?>
                </div>
            <?php endif; ?>
            <div class="box-header">
            <div class="col-md-4">
              <?php  if($this->session->flashdata('message')){echo $this->session->flashdata('message');} ?>
            </div>
          </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="<?php echo base_url(); ?>pengelola/diskon/input_min_trans_persen" method="POST">
              <div class="box-footer">
                <div class="col-md-6">
                  <div class="box-body">

                  <div class="form-group">
                    <label for="exampleInputEmail1">Kode Diskon</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Kode Diskon" name="kode_diskon">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Nama Diskon</label>
                    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Nama Diskon" name="nama_diskon">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Minimal Transaksi (Rp)</label>
                    <input type="text" class="form-control persentase rupiah" id="" placeholder="Minimal Transaksi" name="minimal_transaksi">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Persentase Diskon (%)</label>
                    <input type="text" class="form-control persentase" id="exampleInputPassword1" placeholder="Persentase Diskon" name="persentase_diskon">
                  </div>
                  

                </div>
              </div>
              <div class="col-md-6 tombol">
              <button type="submit" class="btn btn-info">INPUT DISKON</button>
            </div>

            </div>
         </form>
          <!-- /.box -->


        </div>

        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 <script src="<?php echo base_url('assets/template/back/dist') ?>/js/adminlte.min.js"></script>
<script type="text/javascript">
   $('.persentase').keypress(function(event){
            console.log(event.which);
        if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
            event.preventDefault();
        }});
   // Format mata uang.
    $( '.rupiah' ).mask('0.000.000.000', {reverse: true});
</script>

</body>
</html>
