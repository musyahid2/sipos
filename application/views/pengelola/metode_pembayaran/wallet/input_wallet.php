<!DOCTYPE html>
<html>
<?php $this->load->view('pengelola/head') ?>
<head>
  <style type="text/css">
    .tombol {
      
      margin: 0 0 100px 400px;
    }
  </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php $this->load->view('pengelola/header') ?>
  <!-- Left side column. contains the logo and sidebar -->

<?php $this->load->view('pengelola/leftbar') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">

          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">INPUT METODE PEMBAYARAN WALLET</h3>
            </div>
             <?php if (validation_errors()) : ?>
                <div class="alert alert-danger">
                  <?php echo validation_errors(); ?>
                </div>
            <?php endif; ?>
                      <div class="box-header">
          <div class="col-md-4">
               <?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');} ?>
      </div>
        </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="<?php echo base_url(); ?>pengelola/metode_pembayaran/proses_inputWallet" method="POST" enctype="multipart/form-data">
              <div class="box-footer">
                <div class="col-md-6">
                  <div class="box-body">

                  <div class="form-group">
                    <label for="exampleInputEmail1">ID Metode Pembayaran Wallet</label>
                      <input type="text" class="form-control" id="exampleInputEmail1" placeholder="ID Metode Pembayaran Wallet" name="id_metodePembayaran">
                      </div>
                      <div class="form-group">
                      <label for="exampleInputPassword1">Nama Pembayaran Wallet</label>
                    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Nama Metode Pembayaran Wallet" name="nama_metodePembayaran">
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="exampleInputFile">Foto / Barcode / QR Code</label>
                  <input type="file" name="filefoto" id="exampleInputFile">
                  <span class="badge badge-success">Ukuran maksimal 2 MB. Format file: jpeg, jpg, dan png.</span>
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Logo</label>
                  <input type="file" name="filelogo" id="exampleInputFile">
                  <span class="badge badge-success">Ukuran maksimal 2 MB. Format file: jpeg, jpg, dan png.</span>
                </div>
              </div>
              <div class="col-md-6 tombol">
              <button type="submit" class="btn btn-info">INPUT DATA</button>
            </div>

            </div>
         </form>
        </div>

        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
<?php $this->load->view('pengelola/footer') ?>
<script type="text/javascript">
   //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })
</script>
</body>
</html>
