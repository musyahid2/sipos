<!DOCTYPE html>
<html>
<?php $this->load->view('pengelola/head') ?>
<head>
  <style type="text/css">
    .tombol {
      
      margin: 0 0 100px 400px;
    }
  </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php $this->load->view('pengelola/header') ?>
  <!-- Left side column. contains the logo and sidebar -->

<?php $this->load->view('pengelola/leftbar') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">

          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">UBAH DATA METODE PEMBAYARAN DEBIT</h3>
            </div>
             <?php if (validation_errors()) : ?>
                <div class="alert alert-danger">
                  <?php echo validation_errors(); ?>
                </div>
            <?php endif; ?>
                      <div class="box-header">
          <div class="col-md-4">
               <?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');} ?>
      </div>
        </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="<?php echo base_url(); ?>pengelola/metode_pembayaran/proses_editDebit" method="POST" enctype="multipart/form-data">
              <div class="box-footer">
                <div class="col-md-6">
                  <div class="box-body">

                  <div class="form-group">
                    <label for="exampleInputEmail1">ID Metode Pembayaran Wallet</label>
                      <input type="text" class="form-control" id="exampleInputEmail1" placeholder="ID Metode Pembayaran Wallet" value="<?php echo $dataDebit[0]->Id_metodePembayaran ?>" name="id_metodePembayaran" readonly>
                      </div>
                      <div class="form-group">
                      <label for="exampleInputPassword1">Nama Pembayaran Wallet</label>
                    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Nama Metode Pembayaran Wallet" name="nama_metodePembayaran" value="<?php echo $dataDebit[0]->Nama ?>">
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                <label for="name">Foto</label>
                <div>
                  <img src="<?php echo base_url('assets/images/metode_pembayaran/'.$dataDebit[0]->Foto); ?>" width="100">
                </div><br>
                <input class="form-control-file <?php echo form_error('image') ? 'is-invalid':'' ?>"
                 type="file" name="filefoto" />
                <input type="hidden" name="old_imageFoto" value="<?php echo $dataDebit[0]->Foto ?>" />
                <div class="invalid-feedback">
                  <?php echo form_error('image') ?>
                </div>
              </div>
              <div class="form-group">
                <label for="name">Foto</label>
                <div>
                  <img src="<?php echo base_url('assets/images/metode_pembayaran/'.$dataDebit[0]->Logo); ?>" width="100">
                </div><br>
                <input class="form-control-file <?php echo form_error('image') ? 'is-invalid':'' ?>"
                 type="file" name="filelogo" />
                <input type="hidden" name="old_imageLogo" value="<?php echo $dataDebit[0]->Logo ?>" />
                <div class="invalid-feedback">
                  <?php echo form_error('image') ?>
                </div>
              </div>
              </div>
              <div class="col-md-6 tombol">
              <button type="submit" class="btn btn-info">UBAH DATA</button>
            </div>

            </div>
         </form>
        </div>

        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
<?php $this->load->view('pengelola/footer') ?>
<script type="text/javascript">
   //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })
</script>
</body>
</html>
