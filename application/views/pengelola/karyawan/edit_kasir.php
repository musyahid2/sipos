<!DOCTYPE html>
<html>
<?php $this->load->view('pengelola/head') ?>
<head>
  <style type="text/css">
    .tombol {
      
      margin: 0 0 100px 400px;
    }
  </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php $this->load->view('pengelola/header') ?>
  <!-- Left side column. contains the logo and sidebar -->

<?php $this->load->view('pengelola/leftbar') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
       <!--  <div class="col-md-4">
          <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                Danger alert preview. This alert is dismissable. A wonderful serenity has taken possession of my entire
                soul, like these sweet mornings of spring which I enjoy with my whole heart.
              </div>
        </div> -->
        <!-- left column -->
        <div class="col-md-12">

          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">EDIT DATA KASIR</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="<?php echo site_url(); ?>pengelola/karyawan/proses_editKasir" method="POST" enctype="multipart/form-data">
              <div class="box-footer">
                <div class="col-md-6">
                  <div class="box-body">

               
                   
                      <input type="hidden" class="form-control" id="exampleInputEmail1" placeholder="ID Kasir" name="id_kasir" value="<?php echo $dataKasir[0]->ID_kasir ?>">
               
                      <div class="form-group">
                      <label for="exampleInputPassword1">Nama Kasir</label>
                    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Nama Kasir" name="nama_kasir" value="<?php echo $dataKasir[0]->Nama_kasir ?>">
                  </div>
                 <div class="form-group">
                     <label for="namalengkap">Jenis Kelamin</label><br>
                        <div class="radio">
                          <label>
                              <input type="radio" name="Jenis_Kelamin" value="Laki-Laki" <?php if($dataKasir[0]->Jenis_kelamin=="Laki-Laki") echo "checked" ?>>LAKI - LAKI
                          </label>&nbsp&nbsp&nbsp&nbsp&nbsp
                          <label>
                           <input type="radio" name="Jenis_Kelamin" value="Perempuan" <?php if($dataKasir[0]->Jenis_kelamin=="Perempuan") echo "checked" ?>>PEREMPUAN
                      </label>
                  </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">NO HP</label>
                      <input type="text" class="form-control" id="exampleInputEmail1" placeholder="NO HP" name="no_hp" value="<?php echo $dataKasir[0]->Nomor_telp ?>">
                </div>
                <div class="form-group">
                <label for="name">Foto</label>
                <div>
                  <img src="<?php echo base_url('assets/images/kasir/'.$dataKasir[0]->Foto); ?>" width="100">
                </div><br>
                <input class="form-control-file <?php echo form_error('image') ? 'is-invalid':'' ?>"
                 type="file" name="filefoto" />
                <input type="hidden" name="old_image" value="<?php echo $dataKasir[0]->Foto ?>" />
                <div class="invalid-feedback">
                  <?php echo form_error('image') ?>
                </div>
              </div>
                </div>
              </div>
                <div class="col-md-6">
                  <div class="box-body">
                  <div class="form-group">
                      <label for="exampleInputPassword1">Alamat</label>
                   <textarea class="form-control" rows="3" placeholder="" name="alamat" value=""><?php echo $dataKasir[0]->Alamat ?></textarea>
                  </div>
                    <div class="form-group"> 
                  <label for="exampleInputPassword1">Status</label>
                  <select name="Status" class="form-control">
                    <option value="Aktif" <?php if($dataKasir[0]->Status=="Aktif") echo "selected" ?>>Aktif</option>
                     <option value="Tidak Aktif" <?php if($dataKasir[0]->Status=="Tidak Aktif") echo "selected" ?>>Tidak Aktif</option>
                  </select>
                </div>   
 
               
                </div>


              </div>
              <div class="col-md-6 tombol">
              <button type="submit" class="btn btn-info">UBAH DATA</button>
            </div>

            </div>
         </form>
          <!-- /.box -->


        </div>

        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
<?php $this->load->view('pengelola/footer') ?>
<script type="text/javascript">
   //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })
</script>

</body>
</html>
