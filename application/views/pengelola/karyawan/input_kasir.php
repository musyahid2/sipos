<!DOCTYPE html>
<html>
<?php $this->load->view('pengelola/head') ?>
<head>
  <style type="text/css">
    .tombol {
      
      margin: 0 0 100px 400px;
    }
  </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php $this->load->view('pengelola/header') ?>
  <!-- Left side column. contains the logo and sidebar -->

<?php $this->load->view('pengelola/leftbar') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
       <!--  <div class="col-md-4">
          <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                Danger alert preview. This alert is dismissable. A wonderful serenity has taken possession of my entire
                soul, like these sweet mornings of spring which I enjoy with my whole heart.
              </div>
        </div> -->
        <!-- left column -->
        <div class="col-md-12">

          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">INPUT DATA KASIR</h3>
            </div>
             <?php if (validation_errors()) : ?>
                <div class="alert alert-danger">
                  <?php echo validation_errors(); ?>
                </div>
            <?php endif; ?>
                      <div class="box-header">
          <div class="col-md-4">
               <?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');} ?>
      </div>
        </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="<?php echo base_url(); ?>pengelola/karyawan/proses_inputKasir" method="POST" enctype="multipart/form-data">
              <div class="box-footer">
                <div class="col-md-6">
                  <div class="box-body">

                  <div class="form-group">
                    <label for="exampleInputEmail1">ID Kasir</label>
                      <input type="text" class="form-control" id="exampleInputEmail1" placeholder="ID Kasir" name="id_kasir">
                      </div>
                      <div class="form-group">
                      <label for="exampleInputPassword1">Nama Kasir</label>
                    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Nama Kasir" name="nama_kasir">
                  </div>
                 <div class="form-group">
                     <label for="namalengkap">Jenis Kelamin</label><br>
                        <div class="radio">
                          <label>
                              <input type="radio" name="Jenis_Kelamin" value="Laki-Laki">LAKI - LAKI
                          </label>&nbsp&nbsp&nbsp&nbsp&nbsp
                          <label>
                           <input type="radio" name="Jenis_Kelamin" value="Perempuan">PEREMPUAN
                      </label>
                  </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">NO HP</label>
                      <input type="text" class="form-control" id="exampleInputEmail1" placeholder="NO HP" name="no_hp">
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Foto Karyawan</label>
                  <input type="file" name="filefoto" id="exampleInputFile">
                  <span class="badge badge-success">Ukuran maksimal 2 MB. Format file: jpeg, jpg, dan png.</span>
                </div>
                </div>
              </div>
                <div class="col-md-6">
                  <div class="box-body">
                  <div class="form-group">
                      <label for="exampleInputPassword1">Alamat</label>
                   <textarea class="form-control" rows="3" placeholder="Alamat ..." name="alamat"></textarea>
                  </div>
                    <div class="form-group"> 
                  <label for="exampleInputPassword1">Status</label>
                  <select name="Status" class="form-control">
                    <option value="Aktif">Aktif</option>
                     <option value="Tidak Aktif">Tidak Aktif</option>
                  </select>
                </div>   

                 <div class="form-group">
                      <label>PIN Akun</label>
                    <input type="password" class="form-control" placeholder="PIN Akun" name="pin">
                  </div>  
                  <div class="form-group">
                      <label>Konfirmasi PIN Akun</label>
                    <input type="password" class="form-control" placeholder="Konfirmasi PIN Akun" name="pin_confirm">
                  </div>  
               
                </div>

              </div>
              <div class="col-md-6 tombol">
              <button type="submit" class="btn btn-info">INPUT DATA</button>
            </div>

            </div>
         </form>
          <!-- /.box -->


        </div>

        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
<?php $this->load->view('pengelola/footer') ?>
<script type="text/javascript">
   //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })
</script>

</body>
</html>
