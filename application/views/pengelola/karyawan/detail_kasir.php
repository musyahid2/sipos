<!DOCTYPE html>
<html>
<?php $this->load->view('pengelola/head'); ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
	<?php $this->load->view('pengelola/header'); ?>
	<?php $this->load->view('pengelola/leftbar'); ?>

  <div class="content-wrapper">

     <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
             <div class="box-header">
              <tbody>
             <h3>Detail Data Kasir <span class="badge badge-secondary"><?php echo $dataKasir[0]->Nama_kasir ?></span></h3>
              </tbody>
            </div>
            <div class="text-center">
              <img src="<?php echo base_url('assets/images/kasir/'.$img[0]->Foto); ?>" width="200" class="img-thumbnail">
            </div>
        <table class="table table-striped">
            <tr>
              <th scope="row"></th>
              <td><b>ID KASIR</b></td>
              <td>: <?php echo $dataKasir[0]->ID_kasir ?></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <th scope="row"></th>
              <td><b>Nama Kasir</b></td>
              <td>: <?php echo $dataKasir[0]->Nama_kasir ?></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <th scope="row"></th>
              <td><b>Jenis Kelamin</b></td>
              <td>: <?php echo $dataKasir[0]->Jenis_kelamin ?></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <th scope="row"></th>
              <td><b>Nomor Telphone</b></td>
              <td>: <?php echo $dataKasir[0]->Nomor_telp ?></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <th scope="row"></th>
              <td><b>Tanggal Masuk</b></td>
              <td>: <?php echo $dataKasir[0]->Tanggal_masuk ?></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <th scope="row"></th>
              <td><b>Alamat</b></td>
              <td>: <?php echo $dataKasir[0]->Alamat ?></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>

            <tr>
              <th scope="row"></th>
              <td><b>Status</b></td>
              <td>: <?php echo $dataKasir[0]->Status ?></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <th scope="row"></th>
              <td><b>Didaftarkan Oleh</b></td>
              <td>: <?php echo $dataKasir[0]->Nama_pengelola ?></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
        </table>
                    </div>
                  </div>
                </div>
            </section>
          </div>
  
	<?php $this->load->view('pengelola/footer'); ?>
</div>
</body>
</html>