<!DOCTYPE html>
<html>
<?php $this->load->view('pengelola/head'); ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php $this->load->view('pengelola/header'); ?>
  <?php $this->load->view('pengelola/leftbar'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      
      <div class="box">
        <div class="box-header">
            <h1 class="box-title"><b> Data Transaksi Penjualan</b></h1>
        </div>

        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th><center>Kode Transaksi</center></th>
                <th><center>Tanggal Transaksi</center> </th>
                <th><center>Nama Kasir </center></th>
               <!--  <th>Nama Barang </th> -->
                <th><center>Total Harga</center></th>
                <th><center>Detail</center></th>
              </tr>
            </thead>

            <tbody>
              <?php $invoice = $this->session->Invoice;

                foreach ($invoice as $data) {
                  
               ?>
              <tr>
                <td><?php echo $data->Kode_transaksi ?></td>
                <td><?php echo $data->Tanggal_transaksi ?></td>
                <td><?php echo $data->Nama_kasir ?></td>
               <!--  <td><?php echo $data->Nama_barang ?></td> -->
                <td><?php echo ROUND(($data->total),2)?></td>
                <td>
                  <center>
                    <a href="<?php echo site_url() ?>/pengelola/transaksi/DetailInvoice?Kode=<?php echo $data->Kode_transaksi ?>" class="btn btn-sm btn-success">Show Details<span class="glyphicon glyphicon-search" aria-hidden="true"></span></a>
                  </center>
                </td>
                
              </tr>
              <?php } ?>
            </tbody>

          </table>
        </div>

      </div>

    </section>





    <!-- Main content -->
   
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <?php $this->load->view('pengelola/footer'); ?>
</div>
</body>
</html>