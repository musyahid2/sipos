<!DOCTYPE html>
<html>
<?php $this->load->view('pengelola/head') ?>
<head>
 
   <style>
    .example-modal .modal {
      position: relative;
      top: auto;
      bottom: auto;
      right: auto;
      left: auto;
      display: block;
      z-index: 1;
    }

    .example-modal .modal {
      background: transparent !important;
    }
  </style> 

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php $this->load->view('pengelola/header') ?>
  <!-- Left side column. contains the logo and sidebar -->

<?php $this->load->view('pengelola/leftbar') ?>

<div class="content-wrapper">


    <section class="content">
      <div class="row">

        <div class="col-xs-12">
          <div class="box">
             <div class="box-header">
              <h3 class="box-title">History Operasi Kasir</h3>
            </div>
              
          <div class="box-header">
          <div class="col-md-4">
               <?php 
                    if ($this->session->flashdata('message')) {
                      echo $this->session->flashdata('message');
                    }
                    else {
                      echo validation_errors();
                    }

                ?>
      </div>
        </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th style="text-align: center" width="10">No.</th>
                  <th style="text-align: center">Tanggal</th>
                  <th style="text-align: center">ID Kasir</th>
                  <th style="text-align: center">Saldo Awal</th>
                  <th style="text-align: center">Jam Buka</th>
                  <th style="text-align: center">Saldo Akhir</th>
                  <th style="text-align: center">Jam Tutup</th>
                  <th style="text-align: center">Pendapatan</th>
                </tr>
                </thead>
                <tbody>

                  <?php 

                $i = 1;
                foreach ($historykasir as $data) {
                  ?>
                     <tr>
                       <td style="text-align: center"><?php echo $i++ ?></td>
                       <td style="text-align: center"><?php echo $data->Tanggal ?></td>
                       <td style="text-align: center"><?php echo $data->ID_kasir ?></td>
                       <td style="text-align: center">Rp <?php echo number_format($data->Saldo_awal,0,',','.') ?></td>
                       <td style="text-align: center"><?php echo $data->Jam_buka ?></td>
                       <td style="text-align: center">Rp <?php echo number_format($data->Saldo_akhir,0,',','.') ?></td>
                       <td style="text-align: center"><?php echo $data->Jam_tutup ?></td>
                       <td style="text-align: center">Rp <?php echo number_format($data->Pendapatan,0,',','.') ?></td>
                    </tr>
                   <?php } ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>
    </section>
</div>

<?php $this->load->view('pengelola/footer') ?>
</body>
</html>
