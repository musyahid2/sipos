<!DOCTYPE html>
<html>
<?php $this->load->view('pengelola/head'); ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
	<?php $this->load->view('pengelola/header'); ?>
	<?php $this->load->view('pengelola/leftbar'); ?>

	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

      <div class="box">
        <div class="box-header">
            <h3 class="box-title"> Daftar Stok Keluar</h3>
        </div>

        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>ID Stok Keluar</th>
								<th>Nama Barang</th>
								<th>Jumlah Stok Keluar</th>
                <th>Tanggal Stok Keluar</th>
								<th>Keterangan</th>
								<th>Penanggung Jawab Stok Keluar</th>
								<!-- <th>Aksi</th> -->
              </tr>
            </thead>

            <tbody>
							<?php $dataproduk = $this->session->all_data;

								foreach ($dataproduk as $data) {

							 ?>
							<tr>
								<td style="text-align: center"><?php echo $data->ID_stok_keluar ?></td>
								<td style="text-align: center"><?php echo $data->Nama_barang ?></td>
								<td style="text-align: center"><?php echo $data->Jumlah_stok_keluar ?></td>
								<td style="text-align: center"><?php echo $data->Tanggal_stok_keluar ?></td>
								<td style="text-align: center"><?php echo $data->Keterangan_stok_keluar ?></td>
								<td style="text-align: center"><?php echo $data->Nama_pengelola?></td>
								<!-- <td>
									<center>
									<a href="<?php echo site_url() ?>pengelola/stok/update_stok?ID_stok=<?php echo $data->ID_stok ?>" class="btn btn-sm btn-success"><span class="glyphicon glyphicon-plus"></span></a>
									<a href="<?php echo base_url(); ?>pengelola/stok/hapus_stok?ID_stok=<?php echo $data->ID_stok ?>" class="btn btn-sm btn-danger"><span class="glyphicon glyphicon-trash"></span></a>
									</center>
								</td> -->
							</tr>
							<?php } ?>
							</tbody>

          </table>
        </div>

      </div>

    </section>





    <!-- Main content -->

    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

	<?php $this->load->view('pengelola/footer'); ?>
</div>
</body>
</html>
