<!DOCTYPE html>
<html>
<?php $this->load->view('pengelola/head'); ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
	<?php $this->load->view('pengelola/header'); ?>
	<?php $this->load->view('pengelola/leftbar'); ?>

	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

      <div class="box">
        <div class="box-header">
            <h3 class="box-title"> Daftar Stok Barang</h3>
        </div>
        <div class="box-header">
          <a class="btn btn-info" href="<?php echo site_url('pengelola/stok/input_stok') ?>" role="button"><span class="glyphicon glyphicon-plus"> TAMBAH DATA STOK BARANG</span></a>
        </div>
        <div class="box-header">
            <div class="col-md-4">
                 <?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');} ?>
            </div>
          </div>

        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th style="text-align: center">No. </th>
                <th style="text-align: center">Nama Barang</th>
                <th style="text-align: center">Jumlah Stok</th>
                <th style="text-align: center">Tanggal Dibuat</th>
								<th style="text-align: center">Penanggung Jawab</th>
								<th style="text-align: center">Aksi</th>
              </tr>
            </thead>

            <tbody>
							<?php $dataproduk = $this->session->all_data;
              $i = 1;
								foreach ($dataproduk as $data) {

							 ?>
							<tr>
                <td style="text-align: center"><?php echo $i++ ?></td>
								<td style="text-align: center"><?php echo $data->Nama_barang ?></td>
								<td style="text-align: center"><?php echo $data->Jumlah_stok_tersedia ?></td>
								<td style="text-align: center"><?php echo $data->Tanggal_stok ?></td>
								<td style="text-align: center"><?php echo $data->Nama_pengelola?></td>
								<td>
									<center>
									<a href="<?php echo site_url() ?>pengelola/stok/update_stok_masuk?ID_stok=<?php echo $data->ID_stok ?>" class="btn btn-sm btn-success"><span class="glyphicon glyphicon-plus"></span></a>
									<a href="<?php echo site_url() ?>pengelola/stok/update_stok_keluar?ID_stok=<?php echo $data->ID_stok ?>" class="btn btn-sm btn-success"><span class="glyphicon glyphicon-minus"></span></a>
                    <Button data-toggle="modal" data-target="#confirm-delete<?php echo $i; ?>" class='btn btn-sm btn-danger'><span class="glyphicon glyphicon-trash"></a></Button>

                             <div class="modal fade" id="confirm-delete<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                      <div class="modal-dialog">
                                          <div class="modal-content">
                                              <div class="modal-header">
                                                  Konfirmasi Hapus
                                              </div>
                                              <div class="modal-body">
                                                  Apakah yakin ingin menghapus data?
                                              </div>
                                              <div class="modal-footer">
                                                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                  <a href="<?php echo site_url() ?>pengelola/stok/hapus_stok?ID_stok=<?php echo $data->ID_stok ?>" class="btn btn-danger btn-ok" >Delete</a>
                                              </div>
                                          </div>
                                      </div>
                              </div>
									</center>
								</td>
							</tr>
							<?php } ?>
							</tbody>

          </table>
        </div>
      </div>

    </section>





    <!-- Main content -->

    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

	<?php $this->load->view('pengelola/footer'); ?>
</div>
</body>
</html>
