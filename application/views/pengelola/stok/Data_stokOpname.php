<!DOCTYPE html>
<html>
<?php $this->load->view('pengelola/head'); ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
	<?php $this->load->view('pengelola/header'); ?>
	<?php $this->load->view('pengelola/leftbar'); ?>

	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

      <div class="box">
        <div class="box-header">
            <h3 class="box-title"> Daftar Stok Opname</h3>
        </div>
        <div class="box-header">
          <a class="btn btn-info" href="<?php echo site_url('pengelola/stok/input_stokOpname') ?>" role="button"><span class="glyphicon glyphicon-plus"> TAMBAH STOK OPNAME</span></a>
        </div>
         <div class="box-header">
          <div class="col-md-4">
              <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker">
                </div>
          </div>
          <!-- <div class="col-md-4">
            <select class="form-control">
              <option> - Status - </option>
            </select>
          </div> -->
        </div>
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th style="text-align: center">ID Stok Opname</th>
                <th style="text-align: center">Tanggal</th>
                <th style="text-align: center">Keterangan</th>
                <th style="text-align: center">Penanggung Jawab</th>
								<th style="text-align: center">Aksi</th>
              </tr>
            </thead>

            <tbody>
							<?php $dataopname = $this->session->all_data;

								foreach ($dataopname as $data) {

							 ?>
							<tr>
								<td style="text-align: center" width="30"><?php echo $data->ID_stokopname ?></td>
								<td style="text-align: center"><?php echo $data->Tanggal ?></td>
								<td style="text-align: center"><?php echo $data->Keterangan_stok_opname ?></td>
								<td style="text-align: center"><?php echo $data->Nama_pengelola?></td>
								<td>
									<center>
										<a href="<?php echo site_url() ?>pengelola/stok/detail_stokOpname?ID=<?php echo $data->ID_stokopname ?>" class="btn btn-sm btn-success"><span class="glyphicon glyphicon-search"></span></a>
									</center>
								</td>
							</tr>
							<?php } ?>
            </tbody>

          </table>
        </div>

      </div>

    </section>





    <!-- Main content -->

    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

	<?php $this->load->view('pengelola/footer'); ?>
</div>
<script type="text/javascript">
    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })
</script>
</body>
</html>
