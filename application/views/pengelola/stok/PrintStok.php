<!DOCTYPE html>
<html>
<style type="text/css">
  table {
  border-collapse: collapse;
}

table, td, th {
  border: 1px solid black;
}
</style>
<body onload="window.print();">
<div class="wrapper">
	

	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

      <div class="box">
        <div class="box-header">
						<div class="form-group">
						<label>ID Stok Opname : <?php echo $opname[0]->ID_stokopname; ?></label><br>
						<label>ID Penanggung Jawab : <?php echo $opname[0]->ID_pengelola; ?></label><br>
						<label>Tanggal : <?php echo $opname[0]->Tanggal; ?></label><br>
						<label>Keterangan : <?php echo $opname[0]->Keterangan_stok_opname; ?></label>
					</div>
        </div>

        <div class="box-body">
          <table border="1">
            <thead>
              <tr>
                <th>Nama Barang</th>
                <th>Jumlah Barang (Sistem)</th>
                <th>Jumlah Barang (Aktual)</th>
                <th>Selisih Jumlah Barang</th>
                <th>Harga Per Unit (Sistem)</th>
                <th>Harga Per Unit (Baru)</th>
              </tr>
            </thead>

            <tbody>
              <?php
              foreach ($opname as $data) {
                
              ?>
              <tr>
                <td><?php echo $data->Nama_barang?></td>
                <td><?php echo $data->Jumlah_sistem?></td>
                <td><?php echo $data->Jumlah_aktual?></td>
                <td><?php echo $data->Selisih?></td>
                <td><?php echo $data->Harga_lama?></td>
                <td><?php echo $data->Harga_baru?></td>                
              </tr>
                <?php } ?>              
            </tbody>
          </table>
        </div>
      </div>
    </section>
  </div>

</div>
</body>
</html>
