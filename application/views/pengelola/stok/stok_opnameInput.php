<!DOCTYPE html>
<html>
<head>
<head>
 <?php $this->load->view('pengelola/head'); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php $this->load->view('pengelola/header'); ?>
  <?php $this->load->view('pengelola/leftbar'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

      <div class="box">
        <div class="box-header">
            <h3 class="box-title"> TAMBAH STOK OPNAME</h3>
        </div>
        <div class="box-header">
          <div class="col-md-4">
             <div class="form-group">
             
             </div>
          </div>
        </div>
        <div class="box-body">
          <form role="form" name="frm-example1" id="frm-example1" action="<?php echo base_url(); ?>pengelola/stok/proses_inputdata_stokOpname" method="POST">
              <label>Catatan</label>
             <textarea class="form-control" rows="3" name="Keterangan"><?php echo $Keterangan; ?></textarea>
             <br>
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>ID stok</th>
                <th>SKU</th>
                <th>NAMA</th>
                <th>JUMLAH BARANG (SISTEM)</th>
                <th>JUMLAH BARANG (AKTUAL)</th>
                <th>SATUAN</th>
                <th>HARGA PER UNIT(SISTEM)</th>
                <th>HARGA PER UNIT(BARU)</th>
              </tr>
            </thead>
            <tbody>

               <?php
               $dataStok = $this->session->all_data;
               $x = 1;
               foreach ($dataStok as $data) {
              ?>

                <tr>
                  <td><center><input type="text" class="form-control" value="<?php echo $data->ID_stok?>" name="<?php echo "ID_stok".$x?>" style="width: 60px;" readonly></center></td>
                  <td><?php echo $data->SKU?></td>
                  <td><?php echo $data->Nama_barang?></td>
                  <td><input type="text" class="form-control" value="<?php echo $data->Jumlah_stok_tersedia?>" name="Jumlah_tersedia<?php echo $x ?>" readonly ></td>
                  <td><input type="text" class="form-control" name="Jumlah_Aktual<?php echo $x ?>"></td>
                  <td><?php echo $data->Satuan_barang?></td>
                  <td><input type="text"  class="form-control" value="<?php echo $data->Harga_barang?>" name="Harga_lama<?php echo $x?>" readonly ></td>
                  <td><input type="text" class="form-control" name="Harga_Baru<?php echo $x ?>"></td>
                </tr>

            <?php $x++; } ?>

            </tbody>

          </table>
          <button type="submit" class="btn btn-info">SIMPAN</button>

          <!-- <p><b>Form data:</b></p>
          <div id="example-console-form"></div> -->
          </form>
        </div>

      </div>

    </section>
    <!-- Main content -->

    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php $this->load->view('pengelola/footer'); ?>

</div>
<script type="text/javascript">

      // $('#example1').DataTable( {
      //     "paging":   false
      // } );

    // $(document).ready(function() {
    // $('#example').DataTable();
    // } );


   var table = $('#example1').DataTable({
      pageLength: 10
   });

   // Handle form submission event
   $('#frm-example1').on('submit', function(e){
      var form = this;

      // Encode a set of form elements from all pages as an array of names and values
      var params = table.$('input').serializeArray();

      // Iterate over all form elements
      $.each(params, function(){
         // If element doesn't exist in DOM
         if(!$.contains(document, form[this.name])){
            // Create a hidden element
            $(form).append(
               $('<input>')
                  .attr('type', 'hidden')
                  .attr('name', this.name)
                  .val(this.value)
            );
         }
      });

      // $('#example-console-form').text($(form).serialize());
       
      // // Remove added elements
      // $('input[type="hidden"]', form).remove();

      // // Prevent actual form submission
      // e.preventDefault();

   });


</script>
</body>
</html>
