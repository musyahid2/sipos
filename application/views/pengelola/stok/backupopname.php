<!DOCTYPE html>
<html>
<?php $this->load->view('pengelola/head'); ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php $this->load->view('pengelola/header'); ?>
  <?php $this->load->view('pengelola/leftbar'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

      <div class="box">
        
        <div class="box-header">
            <h3 class="box-title"> TAMBAH STOK OPNAME</h3>
        </div>

        <div class="box-header">
          <div class="col-md-4">
             
             
             </div>
          
        </div>

        <div class="box-body">

           <label>Catatan</label>
           <textarea class="form-control" rows="3" name="Keterangan"></textarea><br><br>
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>SKU</th>
                <th>NAMA</th>
                <th>JUMLAH BARANG (SISTEM)</th>
                <th>JUMLAH BARANG (AKTUAL)</th>
                <th>SATUAN</th>
                <th>HARGA PER UNIT(SISTEM)</th>
                <th>HARGA PER UNIT(BARU)</th>
              </tr>
            </thead>

            <tbody>

                <tr>

                </tr>

            </tbody>

          </table>
          
          <button type="submit" class="btn btn-primary">SIMPAN</button>
          
        </div>

      </div>

    </section>
    <!-- Main content -->

    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php $this->load->view('pengelola/footer'); ?>
</div>
<script type="text/javascript">
    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })
</script>
</body>
</html>
