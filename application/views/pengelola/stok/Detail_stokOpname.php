<!DOCTYPE html>
<html>
<?php $this->load->view('pengelola/head'); ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php $this->load->view('pengelola/header'); ?>
  <?php $this->load->view('pengelola/leftbar'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

      <div class="box">
        <div class="box-header">
            <h3 class="box-title"> Detail Stok Opname</h3>
            <?php  ?>
            <div class="form-group">
            <label>ID Stok Opname : <?php echo $opname[0]->ID_stokopname; ?></label><br>
            <label>ID Penanggung Jawab : <?php echo $opname[0]->ID_pengelola; ?></label><br>
            <label>Tanggal : <?php echo $opname[0]->Tanggal; ?></label><br>
            <label>Keterangan : <?php echo $opname[0]->Keterangan_stok_opname; ?></label>
          </div>
        </div>

        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Nama Barang</th>
                <th>Jumlah Barang (Sistem)</th>
                <th>Jumlah Barang (Aktual)</th>
                <th>Selisih Jumlah Barang</th>
                <th>Harga Per Unit (Sistem)</th>
                <th>Harga Per Unit (Baru)</th>
              </tr>
            </thead>

            <tbody>
              <?php
              // $dataOpname = $this->session->all_data;
             //    foreach ($dataOpname as $data) {
              foreach ($opname as $data) {
                
              ?>
              <tr>
                <td><?php echo $data->Nama_barang?></td>
                <td><?php echo $data->Jumlah_sistem?></td>
                <td><?php echo $data->Jumlah_aktual?></td>
                <td><?php echo $data->Selisih?></td>
                <td><?php echo $data->Harga_lama?></td>
                <td><?php echo $data->Harga_baru?></td>
              </tr>
              <?php } ?>
              <tr>
                
              <center>
                <a href="<?php echo site_url() ?>/pengelola/stok/Print?ID=<?php echo $data->ID_stokopname;?>" class="btn btn-sm btn-success">Print Stok <span class="glyphicon glyphicon-print" aria-hidden="true"></span></a>
              </center>

              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </section>





    <!-- Main content -->

    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('pengelola/footer'); ?>
</div>
</body>
</html>
