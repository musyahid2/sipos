<!DOCTYPE html>
<html>
<?php $this->load->view('pengelola/head') ?>
<head>
 
   <style>
    .example-modal .modal {
      position: relative;
      top: auto;
      bottom: auto;
      right: auto;
      left: auto;
      display: block;
      z-index: 1;
    }

    .example-modal .modal {
      background: transparent !important;
    }
  </style> 

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php $this->load->view('pengelola/header') ?>
  <!-- Left side column. contains the logo and sidebar -->

<?php $this->load->view('pengelola/leftbar') ?>

<div class="content-wrapper">


    <section class="content">
      <div class="row">

        <div class="col-xs-12">
          <div class="box">
             <div class="box-header">
              <h3 class="box-title">Data Pengelola</h3>
            </div>
              <div class="box-header">
                <a class="btn btn-info" href="<?php echo site_url('pengelola/pengelola/input_pengelola') ?>" role="button"><span class="glyphicon glyphicon-plus"> TAMBAH PENGELOLA</span></a>
              </div>
          <div class="box-header">
          <div class="col-md-4">
               <?php 
                    if ($this->session->flashdata('message')) {
                      echo $this->session->flashdata('message');
                    }
                    else {
                      echo validation_errors();
                    }

                ?>
      </div>
        </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th style="text-align: center" width="10">No.</th>
                  <th style="text-align: center">ID Pengelola</th>
                  <th style="text-align: center">Nama Pengelola</th>
                  <th style="text-align: center">Foto</th>
                  <th style="text-align: center">Nomor Telephone</th>
                  <th style="text-align: center">Aksi</th>
                </tr>
                </thead>
                <tbody>

                  <?php 

                  $dataPengelola = $this->session->all_data;
                  $i = 1;
                  foreach ($dataPengelola as $data) { ?>
                      <tr>
                        <td style="text-align: center"><?php echo $i++ ?></td>
                        <td style="text-align: center"><?php echo $data->ID_pengelola ?></td>
                        <td style="text-align: center"><?php echo $data->Nama_pengelola ?></td>
                        <td style="text-align: center"><img src="<?php echo base_url('assets/images/pengelola/'.$data->Foto); ?>" width="70"></td>
                        <td style="text-align: center"><?php echo $data->Nomor_Telp ?></td>
                        <td>
                          <center>
                            <a href="<?php echo site_url() ?>pengelola/pengelola/detail_pengelola?ID_pengelola=<?php echo $data->ID_pengelola; ?>" class="btn btn-sm btn-success"> <span class="glyphicon glyphicon-search" aria-hidden="true"></span></a>
                            <a href="<?php echo site_url() ?>pengelola/pengelola/edit_pengelola?ID_pengelola=<?php echo $data->ID_pengelola; ?>" class="btn btn-sm btn-success"><span class="glyphicon glyphicon-pencil"></span></a>

                            <Button data-toggle="modal" data-target="#confirm-delete<?php echo $i; ?>" class='btn btn-sm btn-danger'><span class="glyphicon glyphicon-trash"></a></Button>

                             <div class="modal fade" id="confirm-delete<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                      <div class="modal-dialog">
                                          <div class="modal-content">
                                              <div class="modal-header">
                                                  Konfirmasi Hapus
                                              </div>
                                              <div class="modal-body">
                                                  Apakah yakin ingin menghapus data?
                                              </div>
                                              <div class="modal-footer">
                                                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                  <a href="<?php echo site_url() ?>/pengelola/pengelola/hapus_pengelola?ID_pengelola=<?php echo $data->ID_pengelola ?>" class="btn btn-danger btn-ok" >Delete</a>
                                              </div>
                                          </div>
                                      </div>
                                  </div>

                          </center>
                        </td>
                      </tr>
                   <?php } ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>
    </section>
</div>

<?php $this->load->view('pengelola/footer') ?>
</body>
</html>
