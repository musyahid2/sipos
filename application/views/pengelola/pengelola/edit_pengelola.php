<!DOCTYPE html>
<html>
<?php $this->load->view('pengelola/head') ?>
<head>
  <style type="text/css">
    .tombol {
      
      margin: 0 0 100px 400px;
    }
  </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php $this->load->view('pengelola/header') ?>
  <!-- Left side column. contains the logo and sidebar -->

<?php $this->load->view('pengelola/leftbar') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">

          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">UBAH DATA PENGELOLA</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="<?php echo base_url(); ?>pengelola/pengelola/proses_editPengelola" method="POST" enctype="multipart/form-data">
              <div class="box-footer">
                <div class="col-md-6">
                  <div class="box-body">
                      <input type="hidden" class="form-control" id="exampleInputEmail1" placeholder="ID Pengeloa" value="<?php echo $dataPengelola[0]->ID_pengelola ?>" name="id_pengelola">
                      <div class="form-group">
                      <label for="exampleInputPassword1">Nama Pengelola</label>
                    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Nama Pengelola" value="<?php echo $dataPengelola[0]->Nama_pengelola ?>" name="nama_pengelola">
                  </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Email</label>
                      <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Email" name="email" value="<?php echo $dataPengelola[0]->Email ?>">
                </div>
                </div>
              </div>
              <div class="col-md-6">
                  <div class="box-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Nomor Telephone</label>
                      <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Nomor Telephone" name="nomor_telp" value="<?php echo $dataPengelola[0]->Nomor_Telp ?>">
                </div>
                <div class="form-group">
                  <label for="name">Foto</label>
                  <div>
                    <img src="<?php echo base_url('assets/images/pengelola/'.$dataPengelola[0]->Foto); ?>" width="100">
                  </div><br>
                  <input class="form-control-file <?php echo form_error('image') ? 'is-invalid':'' ?>"
                   type="file" name="filefoto" />
                  <input type="hidden" name="old_image" value="<?php echo $dataPengelola[0]->Foto ?>" />
                  <div class="invalid-feedback">
                    <?php echo form_error('image') ?>
                  </div>
                </div>
                </div>
              </div>
                            <div class="col-md-6 tombol">
              <button type="submit" class="btn btn-primary">UBAH DATA</button>
            </div>
              </div>


            </div>
         </form>
          <!-- /.box -->


        </div>

        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
<?php $this->load->view('pengelola/footer') ?>
<script type="text/javascript">
   //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })
</script>

</body>
</html>
