<!DOCTYPE html>
<html>
<?php $this->load->view('pengelola/head'); ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
	<?php $this->load->view('pengelola/header'); ?>
	<?php $this->load->view('pengelola/leftbar'); ?>

  <div class="content-wrapper">

     <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
             <div class="box-header">
              <tbody>
             <h3>Profile Anda <span class="badge badge-secondary"><?php echo $dataPengelola[0]->Nama_pengelola ?></span></h3>
              </tbody>
            </div>
            <div class="text-center">
              <img src="<?php echo base_url('assets/images/pengelola/'.$dataPengelola[0]->Foto); ?>" width="200" class="img-thumbnail">
            </div>
        <table class="table table-striped">
            <tr>
              <th scope="row"></th>
              <td><b>ID PENGELOLA</b></td>
              <td>: <?php echo $dataPengelola[0]->ID_pengelola ?></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <th scope="row"></th>
              <td><b>Nama Kasir</b></td>
              <td>: <?php echo $dataPengelola[0]->Nama_pengelola ?></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <th scope="row"></th>
              <td><b>Alamat Email</b></td>
              <td>: <?php echo $dataPengelola[0]->Email ?></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <th scope="row"></th>
              <td><b>Nomor Telphone</b></td>
              <td>: <?php echo $dataPengelola[0]->Nomor_Telp ?></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            
        </table>
                    </div>
                  </div>
                </div>
            </section>
          </div>
  
	<?php $this->load->view('pengelola/footer'); ?>
</div>
</body>
</html>