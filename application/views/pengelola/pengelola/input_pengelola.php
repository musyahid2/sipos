<!DOCTYPE html>
<html>
<?php $this->load->view('pengelola/head') ?>
<head>
  <style type="text/css">
    .tombol {
      
      margin: 0 0 100px 400px;
    }
  </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php $this->load->view('pengelola/header') ?>
  <!-- Left side column. contains the logo and sidebar -->

<?php $this->load->view('pengelola/leftbar') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">

          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">INPUT DATA PENGELOLA</h3>
            </div>
            <?php if (validation_errors()) : ?>
                <div class="alert alert-danger">
                  <?php echo validation_errors(); ?>
                </div>
            <?php endif; ?>
            <!-- /.box-header -->
            <!-- form start -->
                      <div class="box-header">
          <div class="col-md-4">
               <?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');} ?>
      </div>
        </div>
            <form role="form" action="<?php echo base_url(); ?>pengelola/pengelola/proses_inputPengelola" method="POST" enctype="multipart/form-data">
              <div class="box-footer">
                
                <div class="col-md-6">
                  <div class="box-body">

                  <div class="form-group">
                    <label for="exampleInputEmail1">ID Pengelola</label>
                      <input type="text" class="form-control" id="exampleInputEmail1" placeholder="ID Pengeloa" name="id_pengelola">
                      </div>
                      <div class="form-group">
                      <label for="exampleInputPassword1">Nama Pengelola</label>
                    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Nama Pengelola" name="nama_pengelola">
                  </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Email</label>
                      <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Email" name="email">
                </div>
                </div>
              </div>
              <div class="col-md-6">
                  <div class="box-body">
                 <div class="form-group">
                    <label for="exampleInputEmail1">Password</label>
                      <input type="password" class="form-control" id="exampleInputEmail1" placeholder="Password" name="password">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Konfirmasi Password</label>
                      <input type="password" class="form-control" id="exampleInputEmail1" placeholder="Konfirmasi Password" name="password_confirm">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Nomor Telephone</label>
                      <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Nomor Telephone" name="nomor_telp">
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Foto Pengelola</label>
                  <input type="file" name="filefoto" id="exampleInputFile">
                  <span class="badge badge-success">Ukuran maksimal 2 MB. Format file: jpeg, jpg, dan png.</span>
                </div>
                </div>
              </div>
                            <div class="col-md-6 tombol">
              <button type="submit" class="btn btn-primary">INPUT DATA</button>
            </div>
              </div>


            </div>
         </form>
          <!-- /.box -->


        </div>

        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
<?php $this->load->view('pengelola/footer') ?>
<script type="text/javascript">
   //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })
</script>

</body>
</html>
