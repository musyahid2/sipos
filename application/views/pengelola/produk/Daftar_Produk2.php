<!DOCTYPE html>
<html>
<?php $this->load->view('pengelola/head') ?>
<head>
 
   <style>
    .example-modal .modal {
      position: relative;
      top: auto;
      bottom: auto;
      right: auto;
      left: auto;
      display: block;
      z-index: 1;
    }

    .example-modal .modal {
      background: transparent !important;
    }
  </style> 

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php $this->load->view('pengelola/header') ?>
  <!-- Left side column. contains the logo and sidebar -->

<?php $this->load->view('pengelola/leftbar') ?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Tables
        <small>advanced tables</small>
      </h1>

      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>

    </section>

      <section class="content-header">

      <div class="box">
        <div class="box-header">
            <h1 class="box-title"><b> Daftar Produk</b></h1>
        </div>
        <div class="box-header">
          <a class="btn btn-info" href="<?php echo site_url('pengelola/produk/input_produk') ?>" role="button"><span class="glyphicon glyphicon-plus"> TAMBAH PRODUK</span></a>
        </div>
        <div class="box-header">
      <div class="col-md-4">
               <?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');} ?>
      </div>
        </div>

        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th style="text-align: center" width="10">NO.</th>
                <th style="text-align: center" >Barcode</th>
                <th style="text-align: center" >Nama Barang</th>
                <th style="text-align: center" >Gambar Barang</th>
                <th style="text-align: center" >Harga</th>
                <th style="text-align: center" >Aksi</th>
              </tr>
            </thead>

            <tbody>
              <?php $dataproduk = $this->session->all_data;
              $i = 1;

                foreach ($dataproduk as $data) {

               ?>
              <tr>
                <td style="text-align: center" ><?php echo $i++ ?></td>
                <td style="text-align: center" ><?php echo $data->Barcode ?></td>
                <td style="text-align: center" ><?php echo $data->Nama_barang ?></td>
                <td style="text-align: center" ><img src="<?php echo base_url('assets/images/produk/'.$data->Gambar_barang); ?>" width="70"></td>
                <td style="text-align: center" >Rp <?php echo $data->Harga_barang ?></td>
                <td style="text-align: center" >
                  <center>
                   <a href="<?php echo base_url() ?>/pengelola/produk/detail_produk?Barcode=<?php echo $data->Barcode; ?>" class="btn btn-sm btn-success"> <span class="glyphicon glyphicon-search" aria-hidden="true"></span></a>
                  <a href="<?php echo site_url() ?>pengelola/produk/update_produk?BARCODE=<?php echo $data->Barcode; ?>" class="btn btn-sm btn-success"><span class="glyphicon glyphicon-pencil"></span></a>
                  <Button data-toggle="modal" data-target="#confirm-delete<?php echo $i; ?>" class='btn btn-sm btn-danger'><span class="glyphicon glyphicon-trash"></a></Button>

                   <div class="modal fade" id="confirm-delete<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        Konfirmasi Hapus
                                    </div>
                                    <div class="modal-body">
                                        Apakah yakin ingin menghapus data?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                        <a href="<?php echo site_url() ?>/pengelola/produk/hapus_produk?barcode=<?php echo $data->Barcode ?>" class="btn btn-danger btn-ok" >Delete</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                  </center>
                </td>
              </tr>
              <?php } ?>
            </tbody>

          </table>
        </div>

      </div>


    </section>
</div>

<?php $this->load->view('pengelola/footer') ?>
</body>
</html>
