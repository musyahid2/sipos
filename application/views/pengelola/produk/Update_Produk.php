<!DOCTYPE html>
<html>
<?php $this->load->view('pengelola/head') ?>
<head>
  <style type="text/css">
    .tombol {

      margin: 0 0 100px 400px;
    }
  </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php $this->load->view('pengelola/header') ?>
  <!-- Left side column. contains the logo and sidebar -->

<?php $this->load->view('pengelola/leftbar') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
       <!--  <div class="col-md-4">
          <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                Danger alert preview. This alert is dismissable. A wonderful serenity has taken possession of my entire
                soul, like these sweet mornings of spring which I enjoy with my whole heart.
              </div>
        </div> -->
        <!-- left column -->
        <div class="col-md-12">

          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">EDIT DATA BARANG</h3>
            </div>
                        <?php if (validation_errors()) : ?>
                <div class="alert alert-danger">
                  <?php echo validation_errors(); ?>
                </div>
            <?php endif; ?>
                      <div class="box-header">
          <div class="col-md-4">
               <?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');} ?>
      </div>
        </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="<?php echo base_url(); ?>pengelola/produk/proses_updateBarang" method="POST" enctype="multipart/form-data">
              <div class="box-footer">
                <div class="col-md-6">
                  <div class="box-body">

  
                      <input type="hidden" class="form-control" id="exampleInputEmail1" value="<?php echo $dataProduk[0]->Barcode ?>" name="barcode">
    
                      <div class="form-group">
                      <label for="exampleInputPassword1">SKU</label>
                    <input type="text" class="form-control" id="exampleInputPassword1" value="<?php echo $dataProduk[0]->SKU ?>" name="sku">
                  </div>
                  <div class="form-group">
                      <label for="exampleInputPassword1">Nama Barang</label>
                    <input type="text" class="form-control" id="exampleInputPassword1" value="<?php echo $dataProduk[0]->Nama_barang ?>" name="nama_barang">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">HARGA</label>
                      <input type="text" class="form-control rupiah" id="exampleInputEmail1" value="<?php echo $dataProduk[0]->Harga_barang ?>" name="harga">
                      </div>
          
                </div>
              </div>
                <div class="col-md-6">
                  <div class="box-body">
                  <div class="form-group">
                      <label for="exampleInputPassword1">SATUAN BARANG</label>
                      <select name="satuan" class="form-control">
                        <option value="PCS" <?php if($dataProduk[0]->Satuan_barang=="PCS") echo "selected" ?>>PCS</option>
                        <option value="Gram" <?php if($dataProduk[0]->Satuan_barang=="Gram") echo "selected" ?>>Gram</option>
                        <option value="Kg" <?php if($dataProduk[0]->Satuan_barang=="Kg") echo "selected" ?>>Kg</option>
                        <option value="Porsi" <?php if($dataProduk[0]->Satuan_barang=="Porsi") echo "selected" ?>>Porsi</option>
                        <option value="Botol" <?php if($dataProduk[0]->Satuan_barang=="Botol") echo "selected" ?>>Botol</option>
                        <option value="Lainnya" <?php if($dataProduk[0]->Satuan_barang=="Lainnya") echo "selected" ?>>Lainnya</option>
                      </select>
                  </div>
                    <div class="form-group">
                  <label for="exampleInputPassword1">Kategori</label>
                  <select name="kategori" class="form-control">
                    <?php
                          $dataBarang = $this->session->all_data;
                          foreach ($dataBarang as $data) { ?>
                             <option value="<?php echo $data->Kode_kategori ?>"
                            <?php if($dataProduk[0]->Kode_kategori==$data->Kode_kategori)
                             echo "selected" ?> > <?php echo $data->Kode_kategori." - ". $data->Nama_kategori?> </option>
                           <?php } ?>
                    </select>
                </div>
                <div class="form-group">
                <label for="name">Photo</label>
                                <div>
                  <img src="<?php echo base_url('assets/images/produk/'.$dataProduk[0]->Gambar_barang); ?>" width="100">
                </div><br>
                <input class="form-control-file <?php echo form_error('image') ? 'is-invalid':'' ?>"
                 type="file" name="filefoto" />
                <input type="hidden" name="old_image" value="<?php echo $dataProduk[0]->Gambar_barang ?>" />
                <div class="invalid-feedback">
                  <?php echo form_error('image') ?>
                </div>
              </div>

                </div>

              </div>
              <div class="col-md-6 tombol">
              <button type="submit" class="btn btn-info">UBAH DATA</button>
            </div>

            </div>
         </form>
          <!-- /.box -->


        </div>

        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>

<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('assets/template/back/bower_components') ?>/bootstrap/dist/js/bootstrap.min.js"></script>


<script src="<?php echo base_url('assets/template/back/dist') ?>/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url('assets/template/back/dist') ?>/js/pages/dashboard.js"></script>

<script type="text/javascript">
      $(document).ready(function(){

    // Format mata uang.
    $( '.rupiah' ).mask('0.000.000.000', {reverse: true});

    // Format nomor HP.
    $( '.no_hp' ).mask('0000−0000−0000');

    // Format tahun pelajaran.
    $( '.tapel' ).mask('0000/0000');
})


$('form').bind("keypress", function(e) {
  if (e.keyCode == 13) {               
    e.preventDefault();
    return false;
  }
});


</script>

</body>
</html>
