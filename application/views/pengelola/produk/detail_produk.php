<!DOCTYPE html>
<html>
<?php $this->load->view('pengelola/head'); ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
	<?php $this->load->view('pengelola/header'); ?>
	<?php $this->load->view('pengelola/leftbar'); ?>

  <div class="content-wrapper">

     <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
             <div class="box-header">
              <tbody>
             <h3>Detail Data Produk <span class="badge badge-secondary"><?php echo $dataProduk[0]->Nama_barang ?></span></h3>
              </tbody>
            </div>
        <table class="table table-striped">
            <tr>
              <th scope="row"></th>
              <td><b>Gambar Produk</b></td>
              <td><img src="<?php echo base_url('assets/images/produk/'.$img[0]->Gambar_barang); ?>" width="200" class="img-thumbnail"</td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <th scope="row"></th>
              <td><b>Barcode</b></td>
              <td>: <?php echo $dataProduk[0]->Barcode ?></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <th scope="row"></th>
              <td><b>SKU</b></td>
              <td>: <?php echo $dataProduk[0]->SKU ?></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <th scope="row"></th>
              <td><b>Nama Barang</b></td>
              <td>: <?php echo $dataProduk[0]->Nama_barang ?></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <th scope="row"></th>
              <td><b>Harga Barang</b></td>
              <td>: Rp <?php echo number_format($dataProduk[0]->Harga_barang,0,',','.') ?></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <th scope="row"></th>
              <td><b>Satuan Barang</b></td>
              <td>: <?php echo $dataProduk[0]->Satuan_barang ?></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <th scope="row"></th>
              <td><b>Diinputkan oleh</b></td>
              <td>: <?php echo $dataProduk[0]->Nama_pengelola ?></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td colspan="5"> 
                <center>
                    <a href="<?php echo site_url('pengelola/produk') ?>" class="btn btn-sm btn-success"> <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Kembali</a>
                </center>
              </td>
            </tr>
        </table>
                    </div>
                  </div>
                </div>
            </section>
          </div>
  
	<?php $this->load->view('pengelola/footer'); ?>
</div>
</body>
</html>