<!DOCTYPE html>
<html>
<?php $this->load->view('pengelola/head') ?>
<head>

  <style type="text/css">
    .tombol {

      margin: 0 0 100px 400px;
    }
  </style>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php $this->load->view('pengelola/header') ?>
  <!-- Left side column. contains the logo and sidebar -->

<?php $this->load->view('pengelola/leftbar') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">

          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">INPUT DATA BARANG</h3>
            </div>
            <?php if (validation_errors()) : ?>
                <div class="alert alert-danger">
                  <?php echo validation_errors(); ?>
                </div>
            <?php endif; ?>
                      <div class="box-header">
          <div class="col-md-4">
               <?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');} ?>
      </div>
        </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="<?php echo base_url(); ?>pengelola/produk/prosesinputProduk" method="POST" enctype="multipart/form-data">
              <div class="box-footer">
                <div class="col-md-6">
                  <div class="box-body">

                      <div class="form-group">
                      <label for="exampleInputPassword1">SKU</label>
                    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="SKU" name="sku">
                  </div>
                  <div class="form-group">
                      <label for="exampleInputPassword1">Nama Barang</label>
                    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Nama Barang" name="nama_barang">
                  </div>
                 <div class="form-group">
                      <label for="exampleInputPassword1">Gambar</label><span class="badge badge-success">Ukuran maksimal 2 MB. Format file: jpeg, jpg, dan png.</span>
                    <input type="file" class="form-control" id="exampleInputPassword1" placeholder="Pilih Gambar" name="filefoto">
                  </div>
                    <div class="form-group">
                    <label for="exampleInputEmail1">HARGA</label>
                      <input type="text" class="form-control rupiah" placeholder="Harga"  name="harga">     
                      </div>
                </div>
              </div>
                <div class="col-md-6">
                  <div class="box-body">
                  <div class="form-group">
                      <label for="exampleInputPassword1">SATUAN BARANG</label>
                      <select name="satuan" class="form-control">
                        <option value="PCS">PCS</option>
                        <option value="Gram">Gram</option>
                        <option value="Kg">Kg</option>
                        <option value="Porsi">Porsi</option>
                        <option value="Botol">Botol</option>
                        <option value="Lainnya">Lainnya</option>
                      </select>
                  </div>
                    <div class="form-group">
                  <label for="exampleInputPassword1">Kategori</label>
                  <select name="kategori" class="form-control">
                    <?php

                          $dataBarang = $this->session->all_data;
                          //print_r($dataSiswa);

                          foreach ($dataBarang as $data) { //ngabsen data
                            echo "<option value='". $data->Kode_kategori."'>".$data->Kode_kategori." - ". $data->Nama_kategori."</option>";
                          }
                      ?>

                    ?>
                  </select>
                </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">BARCODE</label>
                      <input type="text" class="form-control" id="exampleInputEmail1" placeholder="BARCODE" name="barcode">
                      </div>

                </div>

              </div>
              <div class="col-md-6 tombol">
              <button type="submit" class="btn btn-info">INPUT DATA</button>
            </div>

            </div>
         </form>
          <!-- /.box -->


        </div>

        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>

<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('assets/template/back/bower_components') ?>/bootstrap/dist/js/bootstrap.min.js"></script>


<script src="<?php echo base_url('assets/template/back/dist') ?>/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url('assets/template/back/dist') ?>/js/pages/dashboard.js"></script>

<script type="text/javascript">
      $(document).ready(function(){

    // Format mata uang.
    $( '.rupiah' ).mask('0.000.000.000', {reverse: true});

    // Format nomor HP.
    $( '.no_hp' ).mask('0000−0000−0000');

    // Format tahun pelajaran.
    $( '.tapel' ).mask('0000/0000');
})


$('form').bind("keypress", function(e) {
  if (e.keyCode == 13) {               
    e.preventDefault();
    return false;
  }
});


</script>
</body>
</html>
