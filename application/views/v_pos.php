<!DOCTYPE html>
<html>
<head>
    <title>Point</title>

</head>
<body>
    <div class="container">
        <div class="col-md-12 col-md-offset-1">
        <hr/>
            <form class="form-horizontal">
                <div class="form-group">
                    <label class="control-label col-xs-3" >Kode Barang</label>
                    <div class="col-xs-9">
                        <input name="kode" id="kode" class="form-control" type="text" placeholder="Kode Barang..." style="width:335px;">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-xs-3" >Nama Barang</label>
                    <div class="col-xs-9">
                        <input name="nama" class="form-control" type="text" placeholder="Nama Barang..." style="width:335px;" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-xs-3" >Harga</label>
                    <div class="col-xs-9">
                        <input name="harga" class="form-control" type="text" placeholder="Harga..." style="width:335px;" readonly>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script type="text/javascript" src="<?php echo base_url().'assets/template/back/js/jquery.js'?>"></script>
    <script type="text/javascript">
        $(document).ready(function(){
             $('#kode').on('input',function(){
                
                var kode=$(this).val();
                $.ajax({
                    type : "POST",
                    url  : "<?php echo base_url('index.php/pos/get_barang')?>",
                    dataType : "JSON",
                    data : {kode: kode},
                    cache:false,
                    success: function(data){
                        $.each(data,function(Barcode, Nama_barang, Harga_barang){
                            $('[name="nama"]').val(data.Nama_barang);
                            $('[name="harga"]').val(data.Harga_barang);
                            
                        });
                        
                    }
                });
                return false;
           });

        });
    </script>
</body>
</html>