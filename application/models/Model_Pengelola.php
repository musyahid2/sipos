<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Model_pengelola extends CI_Model {


	public $ID_pengelola;

	public function getAllPengelola()
	{
		$this->db->from('pengelola');

		$query = $this->db->get();
		return $query->result();
	}

	public function getById($id_sesi)
	{
		$this->db->from('pengelola');
		$this->db->where('ID_pengelola', $id_sesi);

		$query = $this->db->get();
		return $query->result();
	}

	public function getDetailPengelola($id_pengelola)
	{
		$this->db->from('pengelola');
		$this->db->where('ID_pengelola', $id_pengelola);

		$query = $this->db->get(); 
    	return $query->result();
	}

	public function getProfilepengelola()
	{
		 $pengelola = $this->session->userdata("id_pen");
		 $this->db->from('pengelola');
		 $this->db->where('ID_pengelola', $pengelola);

		$query = $this->db->get(); 
    	return $query->result();
	}


	public function input_pengelola($dataKaryawan)
	{
		$this->db->insert('pengelola', $dataKaryawan);
	}

	public function save()
    {
        $post = $this->input->post();
        $this->ID_pengelola = $post["id_pengelola"];
        $this->Email = $post["email"];
        $this->Password = MD5($post["password"]);
		$this->Foto = $this->_uploadImage();
        $this->Nama_pengelola = $post["nama_pengelola"];
        $this->Nomor_telp = $post["nomor_telp"];
        $this->db->insert('pengelola', $this);
    }

    public function update()
    {
        $post = $this->input->post();

		if (!empty($_FILES["filefoto"]["name"])) {
        $this->Foto = $this->_uploadImage();
   		 } else {
        $this->Foto = $post["old_image"];
		}

        $this->ID_pengelola = $post["id_pengelola"];
        $this->Email = $post["email"];
        $this->Nama_pengelola = $post["nama_pengelola"];
        $this->Nomor_telp = $post["nomor_telp"];
    	$this->db->update('pengelola', $this, array('ID_pengelola' => $post['id_pengelola']));
    }


	public function getPengelola($id_pengelola)
	{
		$this->db->from('pengelola');
		$this->db->where('ID_pengelola', $id_pengelola);

		$query = $this->db->get();
		return $query->result();
	}

	// public function edit_pengelola($id_pengelola, $email, $password, $nama_pengelola, $nomor_telp)
	// {
	// 	$data = array(
	// 		'ID_pengelola' => $id_pengelola,
	// 		'Email'=> $email,
	// 		'Password' => $password,
	// 		'Nama_pengelola' => $nama_pengelola,
	// 		'Nomor_Telp' => $nomor_telp,
	// 	);

	// 	$this->db->where('ID_pengelola', $id_pengelola);
	// 	$this->db->update('pengelola', $data);
	// }

	public function hapus_pengelola($id_pengelola){
		$this->db->where('ID_pengelola', $id_pengelola);
		$this->db->delete('pengelola');
	}

	private function _uploadImage()
		{
			$config['upload_path']          = './assets/images/pengelola';
			$config['allowed_types']        = 'gif|jpg|png';
			$config['file_name']            = $this->ID_pengelola;;
			$config['overwrite']			= true;
			$config['max_size']             = 1024; // 1MB
			// $config['max_width']            = 1024;
			// $config['max_height']           = 768;

			$this->load->library('upload', $config);

			if ($this->upload->do_upload('filefoto')) {
				return $this->upload->data("file_name");
			}
			
			return "default.jpg";
		}


}



 ?>