<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Model_Karyawan extends CI_Model {

	public $ID_kasir;
	public function getAllKasir()
	{
		$this->db->from('kasir');

		$query = $this->db->get();
		return $query->result();
	}

	public function getDetailKasir($id_kasir)
	{
		$this->db->from('kasir k');
		$this->db->where('ID_kasir', $id_kasir);
		$this->db->join('pengelola p', 'k.ID_pengelola = p.ID_pengelola');

		$query = $this->db->get(); 
    	return $query->result();
	}



	public function getimg($id_kasir)
	{
		$this->db->select('*');
		$this->db->from('kasir');
		$this->db->where('ID_kasir', $id_kasir);
		$query = $this->db->get(); 
    	return $query->result();
	}


	// public function input_kasir($dataKaryawan)
	// {
	// 	$this->db->insert('kasir', $dataKaryawan);
	// }

	public function save()
    {
        $post = $this->input->post();
        $this->ID_kasir = $post["id_kasir"];
        $this->PIN = MD5($post["pin"]);
		$this->Foto = $this->_uploadImage();
       	$this->Nama_kasir = $post["nama_kasir"];
        $this->Jenis_kelamin = $post["Jenis_Kelamin"];
        $this->Nomor_telp = $post["no_hp"];
        $this->Tanggal_masuk =  mdate('%Y-%m-%d %H:%i:%s', now());
        $this->Alamat = $post["alamat"];
        $this->Status = $post["Status"];
        $this->ID_pengelola = $this->session->userdata("id_pen");
        $this->db->insert('kasir', $this);
    }

    public function update()
    {
        $post = $this->input->post();
        $this->ID_kasir = $post["id_kasir"];
		if (!empty($_FILES["filefoto"]["name"])) {
        $this->Foto = $this->_uploadImage();
   		 } else {
        $this->Foto = $post["old_image"];
		}

		$this->Nama_kasir = $post["nama_kasir"];
    	$this->Jenis_kelamin = $post["Jenis_Kelamin"];
    	$this->Nomor_telp = $post["no_hp"];
    	$this->Tanggal_masuk =  mdate('%Y-%m-%d %H:%i:%s', now());
    	$this->Alamat = $post["alamat"];
    	$this->Status = $post["Status"];
    	$this->ID_pengelola = $this->session->userdata("id_pen");
    	$this->db->update('kasir', $this, array('ID_kasir' => $post['id_kasir']));
    }


	public function getKasir($id_kasir)
	{
		$this->db->from('kasir');
		$this->db->where('ID_kasir', $id_kasir);

		$query = $this->db->get();
		return $query->result();
	}

	public function edit_kasir($id_kasir, $pin, $nama_kasir, $Jenis_Kelamin, $nomor_telp,  $status, $id_pengelola)
	{
		$data = array(
			'ID_kasir' => $id_kasir,
			'PIN'=> $pin,
			'Nama_kasir' => $nama_kasir,
			'Jenis_kelamin' => $Jenis_Kelamin,
			'Nomor_telp' => $nomor_telp,
			// 'Alamat' => $alamat,
			'Status' => $status,
			'ID_pengelola'=> $id_pengelola
		);

		$this->db->where('ID_kasir', $id_kasir);
		$this->db->update('kasir', $data);
	}

	public function hapus_kasir($id_kasir){
		$this->db->where('ID_kasir', $id_kasir);
		$this->db->delete('kasir');
	}

	private function _uploadImage()
		{
			$config['upload_path']          = './assets/images/kasir';
			$config['allowed_types']        = 'gif|jpg|png';
			$config['file_name']            = $this->ID_kasir;
			$config['overwrite']			= true;
			$config['max_size']             = 1024; // 1MB
			// $config['max_width']            = 1024;
			// $config['max_height']           = 768;

			$this->load->library('upload', $config);

			if ($this->upload->do_upload('filefoto')) {
				return $this->upload->data("file_name");
			}
			
			return "default.jpg";
		}

	public function getJumlahkaryawan()
	{
		$query = $this->db->query("SELECT COUNT('ID_kasir') as 'Jumlah' FROM `kasir`")->result();
	 return $query;
	}
}

?>