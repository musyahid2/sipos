<?php 

/**
 * 
 */
class Model_Transaksi extends CI_Model
{
	
	public function getInvoice()
	{
		$this->db->select('t.Kode_transaksi');
		$this->db->select('t.Tanggal_transaksi');
		$this->db->select('t.Waktu_transaksi');
		$this->db->select('k.Nama_kasir');
			// $this->db->select('b.Nama_barang');
		$this->db->select('(t.Estimasi_pembayaran - ((s.Total_diskon * t.Estimasi_pembayaran) / 100)) as total');
		$this->db->from('transaksi t');
		$this->db->JOIN('diskon s', 't.Kode_diskon=s.Kode_diskon');
		$this->db->join('kasir k', 't.ID_Kasir=k.ID_Kasir');
		$this->db->join('detail_transaksi d', 't.Kode_transaksi=d.Kode_transaksi');
		$this->db->join('barang b', 'd.Barcode=b.Barcode');
		$this->db->where('t.Kode_transaksi=d.Kode_transaksi and t.ID_Kasir=k.ID_kasir');
		$this->db->group_by('t.Kode_transaksi');


		$query = $this->db->get();
		return $query->result();

	}

	public function detailInvoice($kode) 
		{
			$this->db->select('t.Kode_transaksi');
			$this->db->select('t.Tanggal_transaksi');
			$this->db->select('t.Waktu_transaksi');
			$this->db->select('k.Nama_kasir');
			$this->db->select('b.Nama_barang');
			$this->db->select('d.Jumlah');
			$this->db->select('d.Harga_satuan');
			$this->db->select('(d.Jumlah * d.Harga_satuan) as Harga_Total');
			$this->db->select('t.Estimasi_pembayaran');
			$this->db->select('s.Total_diskon');
			$this->db->select('((s.Total_diskon * t.Estimasi_pembayaran) / 100) as diskon');
			$this->db->select('(t.Estimasi_pembayaran - ((s.Total_diskon * t.Estimasi_pembayaran) / 100)) as total');
			$this->db->select('t.tunai');
			$this->db->select('(t.tunai -  (t.Estimasi_pembayaran - ((s.Total_diskon * t.Estimasi_pembayaran) / 100))) as kembalian');
			$this->db->from('transaksi t');
			$this->db->JOIN('diskon s', 't.Kode_diskon=s.Kode_diskon');
			$this->db->JOIN('kasir k', 't.ID_Kasir=k.ID_Kasir');
			$this->db->JOIN('detail_transaksi d', 't.Kode_transaksi=d.Kode_transaksi');
			$this->db->JOIN('barang b', 'd.Barcode=b.Barcode');
			$this->db->where('t.Kode_transaksi', $kode);
			$this->db->where('t.Kode_transaksi=d.Kode_transaksi and t.ID_Kasir=k.ID_Kasir and d.Barcode=b.Barcode');
			$this->db->group_by('d.Barcode');

			$query = $this->db->get();
			return $query->result();
		}



		public function inputProduk($namaProduk, $quantiti, $hargaSatuan, $hargaTotal, $barcode, $jumlah, $total, $id_kasir) 
		{
			$jumlah = count($barcode);

		for($x=0; $x < $jumlah; $x++) {
	

			$dataBarang = array(

				'Kode_transaksi' => $this->get_kd_transaksi(),
				'barcode' => $barcode[$x],
				'Jumlah' => $quantiti[$x],
				'Harga_total' => $hargaTotal[$x] * $quantiti[$x],
				'Harga_satuan' => $hargaSatuan[$x],
				'tanggal_transaksi' => mdate('%Y-%m-%d', now()),
				'Waktu_transaksi' => date('H:i:s'),
				'ID_Kasir' => $id_kasir

			);


			
			$this->db->insert('detail_transaksi', $dataBarang);
		}
	}

	function kurangistok($barcode, $quantiti)
	{
		$jumlah = count($barcode);

		for($x=0; $x < $jumlah; $x++) {
	
			$sql = "UPDATE stok SET Jumlah_stok_tersedia = Jumlah_stok_tersedia - '$quantiti[$x]' WHERE Barcode = '$barcode[$x]'";
			$query = $this->db->query($sql);
		}
	}

	function get_kd_transaksi()
	{
        $q = $this->db->query("SELECT MAX(RIGHT(Kode_transaksi,6)) AS kd_max FROM transaksi WHERE DATE(Tanggal_transaksi)=CURDATE()");
        $kd = "";
        if($q->num_rows()>0){
            foreach($q->result() as $k){
                $tmp = ((int)$k->kd_max)+1;
                $kd = sprintf("%04s", $tmp);
            }
        }else{
            $kd = "0001";
        }
        date_default_timezone_set('Asia/Jakarta');
        return date('dmy').$kd."TRX";
    }

    public function getTotaltransaksi()
	{
		$query = $this->db->query("SELECT SUM(Harga_total) as 'Jumlah' FROM detail_transaksi WHERE tanggal_transaksi = curdate()")->result();

		return $query;

	}
	

    public function getJumlahtransaksi()
	{
		$query = $this->db->query("SELECT COUNT(Kode_transaksi) as 'Jumlah' FROM transaksi WHERE Tanggal_transaksi = curdate()")->result();

		return $query;

	}

	public function getTotaltransaksiTunai()
	{
		$query = $this->db->query("SELECT SUM(Jumlah_pembayaran) as 'Jumlah' FROM pembayaran_tunai a JOIN pembayaran b ON a.ID_pembayaran = b.ID_pembayaran  WHERE Tanggal_pembayaran = curdate()")->result();

		return $query;

	}

	public function getTotaltransaksiWallet()
	{
		$query = $this->db->query("SELECT SUM(Total_tagihan) as 'Jumlah' FROM pembayaran_wallet a JOIN pembayaran b ON a.ID_pembayaran = b.ID_pembayaran WHERE Tanggal_pembayaran = curdate()")->result();

		return $query;

	}

	public function getTotaltransaksiDebit()
	{
		$query = $this->db->query("SELECT SUM(Total_tagihan) as 'Jumlah' FROM pembayaran_debit a JOIN pembayaran b ON a.ID_pembayaran = b.ID_pembayaran  WHERE Tanggal_pembayaran = curdate()")->result();

		return $query;

	}




}
 ?>