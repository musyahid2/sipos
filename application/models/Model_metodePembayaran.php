<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Model_metodePembayaran extends CI_Model {


	public function getAllWallet()
	{
		$this->db->from('metode_pembayaran');
		$this->db->where('Jenis', 'Wallet');
		$query = $this->db->get();
		return $query->result();
	}

	public function getAllDebit()
	{
		$this->db->from('metode_pembayaran');
		$this->db->where('Jenis', 'Debit');
		$query = $this->db->get();
		return $query->result();
	}


	public function getWallet($Id_metodePembayaran)
	{
		$this->db->from('metode_pembayaran');
		$this->db->where('Id_metodePembayaran', $Id_metodePembayaran);
		$query = $this->db->get();
		return $query->result();
	}

	public function getDebit($Id_metodePembayaran)
	{
		$this->db->from('metode_pembayaran');
		$this->db->where('Id_metodePembayaran', $Id_metodePembayaran);
		$query = $this->db->get();
		return $query->result();
	}



	public function saveWallet()
    {
        $post = $this->input->post();
        $this->Id_metodePembayaran = $post["id_metodePembayaran"];
        $this->Nama = $post["nama_metodePembayaran"];
        $this->Jenis =  "Wallet";
		$this->Foto = $this->_uploadFoto();
       	$this->Logo = $this->_uploadLogo();
        $this->db->insert('metode_pembayaran', $this);
    }

    public function saveDebit()
    {
        $post = $this->input->post();
        $this->Id_metodePembayaran = $post["id_metodePembayaran"];
        $this->Nama = $post["nama_metodePembayaran"];
        $this->Jenis =  "Debit";
		$this->Foto = $this->_uploadFoto();
       	$this->Logo = $this->_uploadLogo();
        $this->db->insert('metode_pembayaran', $this);
    }


    public function updatemetodePembayaran()
    {
        $post = $this->input->post();
        $this->Nama = $post["nama_metodePembayaran"];
		if (!empty($_FILES["filefoto"]["name"])) {
        	$this->Foto = $this->_uploadFoto();
   		 } else {
        	$this->Foto = $post["old_imageFoto"];
		}

		if (!empty($_FILES["filelogo"]["name"])) {
			$this->Logo = $this->_uploadLogo();
		} else {
			$this->Logo = $post["old_imageLogo"];
		}

    	$this->db->update('metode_pembayaran', $this, array('id_metodePembayaran' => $post['id_metodePembayaran']));
    }



	public function hapus_wallet($Id_metodePembayaran){
		$this->db->where('Id_metodePembayaran', $Id_metodePembayaran);
		$this->db->delete('metode_pembayaran');
	}

	private function _uploadFoto()
		{
			$config['upload_path']          = './assets/images/metode_pembayaran';
			$config['allowed_types']        = 'gif|jpg|png|jpeg';
			$config['overwrite']			= true;
			$config['max_size']             = 1024; // 1MB
			// $config['max_width']            = 1024;
			// $config['max_height']           = 768;

			$this->load->library('upload', $config);

			if ($this->upload->do_upload('filefoto')) {
				return $this->upload->data("file_name");
			}
			
			return "default.jpg";
		}

		private function _uploadLogo()
		{
			$config['upload_path']          = './assets/images/metode_pembayaran';
			$config['allowed_types']        = 'gif|jpg|png|jpeg';
			$config['overwrite']			= true;
			$config['max_size']             = 1024; // 1MB
			// $config['max_width']            = 1024;
			// $config['max_height']           = 768;

			$this->load->library('upload', $config);

			if ($this->upload->do_upload('filelogo')) {
				return $this->upload->data("file_name");
			}
			
			return "default.jpg";
		}
}

?>