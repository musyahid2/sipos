<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_Diskon extends CI_Model {


	public function input_all_diskon_persen($kode_diskon, $nama_diskon, $persentase_diskon, $id_pengelola, $status)
	{
		$data = array(
			'Kode_Diskon' => $kode_diskon,
			'Nama_Diskon' => $nama_diskon,
			'Persentase' => $persentase_diskon,
			'ID_pengelola' => $id_pengelola,
			'Status' => $status
		);

		$this->db->insert('diskon', $data);
	}

	public function input_all_diskon_rupiah($kode_diskon, $nama_diskon, $potongan_harga, $id_pengelola, $status)
	{
		$data = array(
			'Kode_Diskon' => $kode_diskon,
			'Nama_Diskon' => $nama_diskon,
			'Potongan_Harga' => $potongan_harga,
			'ID_pengelola' => $id_pengelola,
			'Status' => $status
		);

		$this->db->insert('diskon', $data);
	}

	public function input_all_persen_max($kode_diskon, $nama_diskon, $persentase_diskon, $maksimal_diskon, $id_pengelola, $status)
	{
		$data = array(
			'Kode_Diskon' => $kode_diskon,
			'Nama_Diskon' => $nama_diskon,
			'Persentase' => $persentase_diskon,
			'Max_Diskon' => $maksimal_diskon,
			'ID_pengelola' => $id_pengelola,
			'Status' => $status
		);

		$this->db->insert('diskon', $data);
	}

	public function input_min_trans_persen($kode_diskon, $nama_diskon, $persentase_diskon, $minimal_transaksi, $id_pengelola, $status)
	{
		$data = array(
			'Kode_Diskon' => $kode_diskon,
			'Nama_Diskon' => $nama_diskon,
			'Persentase' => $persentase_diskon,
			'Min_Transaksi' => $minimal_transaksi,
			'ID_pengelola' => $id_pengelola,
			'Status' => $status
		);

		$this->db->insert('diskon', $data);
	}

	public function input_min_trans_rupiah($kode_diskon, $nama_diskon, $potongan_harga, $minimal_transaksi, $id_pengelola, $status)
	{
		$data = array(
			'Kode_Diskon' => $kode_diskon,
			'Nama_Diskon' => $nama_diskon,
			'Potongan_Harga' => $potongan_harga,
			'Min_Transaksi' => $minimal_transaksi,
			'ID_pengelola' => $id_pengelola,
			'Status' => $status
		);

		$this->db->insert('diskon', $data);
	}

	public function input_min_trans_persen_max_diskon($kode_diskon, $nama_diskon, $persentase_diskon, $maksimal_diskon, $minimal_transaksi, $id_pengelola, $status)
	{
		$data = array(
			'Kode_Diskon' => $kode_diskon,
			'Nama_Diskon' => $nama_diskon,
			'Max_Diskon' => $maksimal_diskon,
			'Persentase' => $persentase_diskon,
			'Min_Transaksi' => $minimal_transaksi,
			'ID_pengelola' => $id_pengelola,
			'Status' => $status
		);

		$this->db->insert('diskon', $data);
	}

	public function daftar_diskon_pending()
	{
		$query = "SELECT * FROM diskon WHERE Status='Pending'";

		return $this->db->query($query)->result();
	}

	public function approve_diskon($kode_diskon)
	{
		$data = array(
			'Status' => 'Aktif'
		);
		$this->db->where('Kode_Diskon', $kode_diskon);

		$this->db->update('diskon', $data);
	}

	public function hapus_diskon_pending($kode_diskon)
	{
		$this->db->where('Kode_Diskon', $kode_diskon);
		$this->db->delete('diskon');

	}

	public function daftar_diskon_aktif()
	{
		$query = "SELECT * FROM diskon WHERE Status='Aktif'";

		return $this->db->query($query)->result();
	}

	public function diskon_selesai($kode_diskon)
	{
		$data = array(
			'Status' => 'Selesai'
		);
		$this->db->where('Kode_Diskon', $kode_diskon);

		$this->db->update('diskon', $data);
	}

	public function history_diskon()
	{
		$query = "SELECT * FROM diskon WHERE Status='Selesai'";

		return $this->db->query($query)->result();
	}

}

/* End of file Model_Diskon.php */
/* Location: ./application/models/Model_Diskon.php */