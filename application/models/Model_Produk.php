<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Model_Produk extends CI_Model {



    public $Barcode;

		function getAllProduk() { //untuk pengelola

		    $this->db->from('barang a');
		   	$this->db->join('kategori c', 'a.Kode_kategori = c.Kode_kategori');
		    $query = $this->db->get();
		    return $query->result();

		}


		function getAllProdukNotInStok(){
			$query = $this->db->query("SELECT * FROM barang
					WHERE Barcode NOT IN (SELECT Barcode FROM stok)")->result() ;

			return $query;
		}

		function getdaftarProduk() { //untuk kasir
			return $this->db->get('barang')->result(); 
		}



		public function getProduksearch($name){
			$this->db->select('*');
			$this->db->from('barang');
			$this->db->like('Barcode',$name);
			$this->db->or_like('Nama_barang',$name);
				$this->db->or_like('SKU',$name);
			return $this->db->get()->result();
		}

			function get_data_barang_bykode($kode){
		$hsl=$this->db->query("SELECT * FROM barang WHERE Barcode='$kode'");
		if($hsl->num_rows()>0){
			foreach ($hsl->result() as $data) {
				$hasil=array(
					'Barcode' => $data->Barcode,	
					'Nama_barang' => $data->Nama_barang,
					'Harga_barang' => $data->Harga_barang,
					);
			}
		}
		return $hasil;
	}


		function getAllKategori() {
		   $this->db->from('kategori');

		   $query = $this->db->get();
		   return $query->result();
		  }

		public function save()
	    {
	        $post = $this->input->post();
	        $this->Barcode = $post["barcode"];
	        $this->SKU = $post["sku"];
			$this->Nama_barang = $post["nama_barang"];
			$this->Gambar_barang = $this->_uploadImage();
			$hargaBarang = $post["harga"];
			$cutHarga = preg_replace('/[^A-Za-z0-9]/', '', $hargaBarang);
	        $this->Harga_barang = (int) $cutHarga;
	        $this->Satuan_barang = $post["satuan"];
	        $this->ID_pengelola = $this->session->userdata("id_pen");
	        $this->Kode_kategori = $post["kategori"];
	        	$this->db->insert('barang', $this);
	    }

	    public function update()
	    {
	        $post = $this->input->post();
	        $this->Barcode = $post["barcode"];
	        $this->SKU = $post["sku"];
			$this->Nama_barang = $post["nama_barang"];
			$this->Gambar_barang = $this->_uploadImage();

			if (!empty($_FILES["filefoto"]["name"])) {
            $this->Gambar_barang = $this->_uploadImage();
       		 } else {
            $this->Gambar_barang = $post["old_image"];
			}

			$hargaBarang = $post["harga"];
			$cutHarga = preg_replace('/[^A-Za-z0-9]/', '', $hargaBarang);
	        $this->Harga_barang = (int) $cutHarga;
	        $this->Satuan_barang = $post["satuan"];
	        $this->ID_pengelola = $this->session->userdata("id_pen");
	        $this->Kode_kategori = $post["kategori"];
	        $this->db->update('barang', $this, array('Barcode' => $post['barcode']));
	    }

		private function _uploadImage()
		{
			$config['upload_path']          = './assets/images/produk';
			$config['allowed_types']        = 'gif|jpg|png';
			$config['file_name']            = $this->Barcode;
			$config['overwrite']			= true;
			$config['max_size']             = 1024; // 1MB
			// $config['max_width']            = 1024;
			// $config['max_height']           = 768;

			$this->load->library('upload', $config);

			if ($this->upload->do_upload('filefoto')) {
				return $this->upload->data("file_name");
			}
			
			return "default.jpg";
		}


		public function hapus_produk($Barcode)
		{
				$this->db->where('Barcode', $Barcode);
				$this->db->delete('barang');
		}

		public function getProduk($Barcode)
		{
			$this->db->from('barang');
			$this->db->where('BARCODE', $Barcode);

			$query = $this->db->get();
			return $query->result();
		}

		public function getimg($Barcode)
		{
			$this->db->select('*');
			$this->db->from('barang');
			$this->db->where('Barcode', $Barcode);
			$query = $this->db->get(); 
	    	return $query->result();
		}

		public function getDetailProduk($Barcode)
		{
			$this->db->from('barang b');
			$this->db->where('Barcode', $Barcode);
			$this->db->join('pengelola p', 'b.ID_pengelola = p.ID_pengelola');

			$query = $this->db->get(); 
	    	return $query->result();
		}

		public function update_harga($Harga,$ID_stok){
			$data = array(
				'Harga_barang' => $Harga,
			);
			// $this->db->where('ID_stok',$ID_stok);
			// $this->db->join('barang', 'barang.Barcode = stok.Barcode');
			// $this->db->update('barang',$data);

			$sql = "UPDATE barang a JOIN stok u USING(Barcode) SET a.Harga_barang = $Harga WHERE u.ID_stok = $ID_stok";
			$this->db->query($sql);
		}

		public function getJumlahproduk()
		{
			$query = $this->db->query("SELECT COUNT('Barcode') as 'Jumlah' FROM `barang`")->result();
		 return $query;
		}

		public function getBarangterlaris()
		{
			$query = $this->db->query("SELECT Nama_barang, SUM(Jumlah) as 'Jumlah' FROM detail_transaksi d INNER JOIN barang b on d.Barcode = b.Barcode WHERE tanggal_transaksi = curdate() GROUP BY b.Barcode ORDER BY 2 DESC LIMIT 5 ")->result();

			return $query;
		}

		public function getKategoriterlaris()
		{
			$query = $this->db->query("SELECT Nama_kategori, SUM(Jumlah) as 'Jumlah' FROM detail_transaksi d INNER JOIN barang b on d.Barcode = b.Barcode INNER JOIN kategori k ON k.Kode_kategori = b.Kode_kategori WHERE tanggal_transaksi = curdate() GROUP BY k.Kode_kategori ORDER BY 2 DESC LIMIT 5 ")->result();

			return $query;
		}



}

?>
