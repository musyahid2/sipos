<?php
class Ajaxsearch_model extends CI_Model
{
	function fetch_data($query)
	{
		$this->db->select("*");
		$this->db->from("barang");
		if($query != '')
		{
			$this->db->like('Barcode', $query);
			$this->db->or_like('Nama_barang', $query);
			$this->db->or_like('Harga_barang', $query);
		}
		$this->db->order_by('Barcode', 'DESC');
		return $this->db->get();
	}

}
?>