<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Model_LoginKasir extends CI_Model {


		function cekLogin($id_kasir, $pin) {
			
			$this->db->from('kasir');
			$this->db->where('ID_kasir', $id_kasir);
			$this->db->where('PIN', $pin);

			  $query = $this->db->get();
			return $query->result();

		}

		function getKasir($id_kasir) {
			$this->db->select('*');
			$this->db->from('kasir');
			$this->db->where('ID_kasir', $id_kasir);
			$query = $this->db->get(); 
	    	return $query->result();
		}

}

?>