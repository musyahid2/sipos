<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Model_Stok extends CI_Model {


		function getAllStok() {

		$this->db->from('stok a');
		   	$this->db->join('barang c', 'a.Barcode = c.Barcode');
		   	$this->db->join('pengelola p','c.ID_pengelola = p.ID_pengelola');

		    $query = $this->db->get();
		    return $query->result();

		}

		public function getstok($ID_stok)
		{
			$this->db->from('stok a');
			$this->db->join('barang c', 'a.Barcode = c.Barcode');
			$this->db->where('ID_stok', $ID_stok);

			$query = $this->db->get();
			return $query->result();
		}

		function getAllProduk() {
   		$this->db->from('barang');
   		$query = $this->db->get();
   		return $query->result();
  	}

		function getAllStokMasuk() {
   		$this->db->from('stok_masuk a');
			$this->db->join('stok b', 'a.ID_stok = b.ID_stok');
			$this->db->join('barang c', 'c.Barcode = b.Barcode');
			$this->db->join('pengelola p','c.ID_pengelola = p.ID_pengelola');
   		$query = $this->db->get();
   		return $query->result();
  	}

		function getAllStokKeluar() {
   		$this->db->from('stok_keluar a');
			$this->db->join('stok b', 'a.ID_stok = b.ID_stok');
			$this->db->join('barang c', 'c.Barcode = b.Barcode');
			$this->db->join('pengelola p','c.ID_pengelola = p.ID_pengelola');
   		$query = $this->db->get();
   		return $query->result();
  	}

		function input_stok($Jumlah, $Barcode, $id_pengelola){

			$data = array(
				'Tanggal_stok'=>  date('Y-m-d H:i:s'),
				'Jumlah_stok_tersedia'=> $Jumlah,
				'Barcode' => $Barcode,
				'ID_pengelola_stok'=> $id_pengelola
			);

			$this->db->insert('stok', $data);
		}

		public function hapus_stok($ID_stok)
		{
				$this->db->where('ID_stok', $ID_stok);
				$this->db->delete('stok');
		}

		public function update_stok($stok_baru, $ID_stok)
		{
			$data = array(
				'Jumlah_stok_tersedia' => $stok_baru,
				'ID_stok' => $ID_stok
			);
			$this->db->where('ID_stok',$ID_stok);
			$this->db->update('stok', $data);
		}

		function input_stok_masuk( $Jumlah_Masuk, $Harga_beli, $ID_stok, $id_pengelola){

			$data = array(
				'Tanggal_stok_masuk'=>  date('Y-m-d H:i:s'),
				'Jumlah_stok_masuk'=> $Jumlah_Masuk,
				'Harga_beli' => $Harga_beli,
				'Total_harga_beli' => ($Jumlah_Masuk*$Harga_beli),
				'ID_stok' => $ID_stok,
				'ID_pengelola_stok_masuk'=> $id_pengelola
			);

			$this->db->insert('stok_masuk', $data);

		}

		function input_stok_keluar($Jumlah_Keluar, $Keterangan, $ID_stok, $id_pengelola){

			$data = array(
				'Tanggal_stok_keluar'=>  date('Y-m-d H:i:s'),
				'Jumlah_stok_keluar'=> $Jumlah_Keluar,
				'Keterangan_stok_keluar'=> $Keterangan,
				'ID_stok' => $ID_stok,
				'ID_pengelola_stok_keluar'=> $id_pengelola
			);

			$this->db->insert('stok_keluar', $data);

		}

		function getAllStokOpname(){
			$this->db->from('stok_opname s');
			$this->db->join('pengelola p','s.ID_pengelola = p.ID_pengelola');
			$query = $this->db->get();
			return $query->result();
		}

		function getDetailStokOpname($ID){
			$this->db->select('a.ID_stokopname');
			$this->db->select('a.ID_pengelola');
			$this->db->select('a.Keterangan_stok_opname');
			$this->db->select('a.Tanggal');
			$this->db->select('d.Nama_barang');
			$this->db->select('b.Jumlah_sistem');
			$this->db->select('b.Jumlah_aktual');
			$this->db->select('b.Selisih');
			$this->db->select('b.Harga_lama');
			$this->db->select('b.Harga_baru');
			$this->db->from('stok_opname a');
			$this->db->join('stok_relation b', 'a.ID_stokopname = b.ID_stokopname');
			$this->db->join('stok c', 'c.ID_stok = b.ID_stok');
			$this->db->join('barang d', ' c.Barcode = d.Barcode');
			$this->db->where('a.ID_stokopname', $ID);

			$query = $this->db->get();
			return $query->result();
		}

		function getDetailOpname($ID){
			$this->db->select('a.ID_stokopname');
			$this->db->select('a.ID_pengelola');
			$this->db->select('a.Keterangan_stok_opname');
			$this->db->select('a.Tanggal');
			$this->db->from('stok_opname a');
			$this->db->join('stok_relation b', 'a.ID_stokopname = b.ID_stokopname');
			$this->db->join('stok c', 'c.ID_stok = b.ID_stok');
			$this->db->join('barang d', ' c.Barcode = d.Barcode');
			$this->db->where('a.ID_stokopname', $ID);

			$query = $this->db->get();
			return $query->result();
		}

		function input_stokopname($Keterangan, $id_pengelola){

			$data = array(
				'Tanggal'=>  date('Y-m-d H:i:s'),
				'Keterangan_stok_opname'=> $Keterangan,
				'ID_pengelola'=> $id_pengelola
			);

			$this->db->insert('stok_opname', $data);
		}

		public function last_record(){
			return $this->db->select('ID_stokopname')->order_by('ID_stokopname',"desc")->limit(1)->get('stok_opname')->row();
		}

		public function get_obj_index($last_key, $ID_stokopname){
			return $last_key->$ID_stokopname;
		}

		function input_stokopname_relasi($ID_stok, $stokopname, $Stok_tersedia, $Stok_Aktual, $Selisih, $Harga_lama, $Harga_baru){

			$data = array(
				'ID_stok'=>  $ID_stok,
				'ID_stokopname'=> $stokopname,
				'Jumlah_sistem'=> $Stok_tersedia,
				'Jumlah_Aktual'=> $Stok_Aktual,
				'Selisih'=> $Selisih,
				'Harga_lama' => $Harga_lama,
				'Harga_baru'=> $Harga_baru
			);

			$this->db->insert('stok_relation', $data);

		}
}

?>
