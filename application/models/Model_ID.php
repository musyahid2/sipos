<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Model_ID extends CI_Model{

	public function get_IDpembayaran(){
		$q = $this->db->query("SELECT MAX(RIGHT(ID_pembayaran,6)) AS kd_max FROM pembayaran WHERE DATE(Tanggal_pembayaran)=CURDATE()");
        $kd = "";
        if($q->num_rows()>0){
            foreach($q->result() as $k){
                $tmp = ((int)$k->kd_max)+1;
                $kd = sprintf("%04s", $tmp);
            }
        }else{
            $kd = "0001";
        }
        date_default_timezone_set('Asia/Jakarta');
        return date('dmy').$kd."PBY";
	}



    function get_kd_transaksi(){
        $q = $this->db->query("SELECT MAX(RIGHT(Kode_transaksi,6)) AS kd_max FROM transaksi WHERE DATE(Tanggal_transaksi)=CURDATE()");
        $kd = "";
        if($q->num_rows()>0){
            foreach($q->result() as $k){
                $tmp = ((int)$k->kd_max)+1;
                $kd = sprintf("%04s", $tmp);
            }
        }else{
            $kd = "0001";
        }
        date_default_timezone_set('Asia/Jakarta');
        return date('dmy').$kd."TRX";
    }

    function get_kd_detailTrx(){
        $q = $this->db->query("SELECT MAX(RIGHT(Kode_transaksi,6)) AS kd_max FROM detail_transaksi WHERE DATE(Tanggal_transaksi)=CURDATE()");
        $kd = "";
        if($q->num_rows()>0){
            foreach($q->result() as $k){
                $tmp = ((int)$k->kd_max)+1;
                $kd = sprintf("%04s", $tmp);
            }
        }else{
            $kd = "0001";
        }
        date_default_timezone_set('Asia/Jakarta');
        return date('dmy').$kd."DTL";
    }


} 

