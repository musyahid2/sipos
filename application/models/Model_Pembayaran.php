<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Model_Pembayaran extends CI_Model {


		public function getAllDiskon()
		{
			
			$this->db->select('*');
			$this->db->from('diskon');
			$this->db->where('Status', 'Aktif');
			$query = $this->db->get();
			return $query->result();

		}

		public function inputDiskon($kode, $keterangan, $total, $jumlah, $jenis, $id_kasir) {
			
		    $data = array (
		    	'Kode_diskon' => $kode,
		    	'Keterangan' => $keterangan,
		    	'Total_diskon' => $total,
		    	'Jumlah' => $jumlah,
		    	'Jenis' => $jenis,
		    	'ID_Kasir' => $id_kasir
		    );

		    $this->db->insert('diskon', $data);

		}

		

		public function getDiskon($Kode)
		{
			$this->db->from('diskon');
			$this->db->where('Kode_diskon', $Kode);

			$query = $this->db->get();
			return $query->result();
		}

		public function updateDiskon($kode, $keterangan, $total, $jumlah, $jenis, $id_kasir)
		{
			$data = array (
		    	'Kode_diskon' => $kode,
		    	'Keterangan' => $keterangan,
		    	'Total_diskon' => $total,
		    	'Jumlah' => $jumlah,
		    	'Jenis' => $jenis,
		    	'ID_Kasir' => $id_kasir
		    );

		    $this->db->where('Kode_diskon', $kode);
		    $this->db->update('diskon', $data);
		}

		public function hapusDiskon($kode)
		{
			$this->db->where('Kode_diskon', $kode);
			$this->db->delete('diskon');
		}

		public function getStrukTunai()
		{
			$this->db->select('t.Kode_transaksi');
			$this->db->select('t.Tanggal_transaksi');
			$this->db->select('t.Waktu_transaksi');
			$this->db->select('k.Nama_kasir');
			$this->db->select('b.Total_bayar');
			$this->db->from('transaksi t');
			$this->db->join('kasir k', 't.ID_Kasir=k.ID_kasir');
			$this->db->join('pembayaran b', 't.Kode_transaksi=b.Kode_transaksi');
			$this->db->join('pembayaran_tunai y', 'b.ID_pembayaran=y.ID_pembayaran');
			$this->db->where('t.Kode_transaksi=b.Kode_transaksi and t.ID_Kasir=k.ID_kasir');
			$this->db->group_by('t.Kode_transaksi');
			$this->db->order_by('t.tanggal_transaksi', 'DESC');


			$query = $this->db->get();
			return $query->result();

		}

		public function getStrukWallet()
		{
			$this->db->select('t.Kode_transaksi');
			$this->db->select('t.Tanggal_transaksi');
			$this->db->select('t.Waktu_transaksi');
			$this->db->select('k.Nama_kasir');
			$this->db->select('b.Total_bayar');
			$this->db->from('transaksi t');
			$this->db->join('kasir k', 't.ID_Kasir=k.ID_kasir');
			$this->db->join('pembayaran b', 't.Kode_transaksi=b.Kode_transaksi');
			$this->db->join('pembayaran_wallet y', 'b.ID_pembayaran=y.ID_pembayaran');
			$this->db->where('t.Kode_transaksi=b.Kode_transaksi and t.ID_Kasir=k.ID_kasir');
			$this->db->group_by('t.Kode_transaksi');
			$this->db->order_by('t.Kode_transaksi', 'DESC');


			$query = $this->db->get();
			return $query->result();

		}

		public function getStrukDebit()
		{
			$this->db->select('t.Kode_transaksi');
			$this->db->select('t.Tanggal_transaksi');
			$this->db->select('t.Waktu_transaksi');
			$this->db->select('k.Nama_kasir');
			$this->db->select('b.Total_bayar');
			$this->db->from('transaksi t');
			$this->db->join('kasir k', 't.ID_Kasir=k.ID_kasir');
			$this->db->join('pembayaran b', 't.Kode_transaksi=b.Kode_transaksi');
			$this->db->join('pembayaran_debit y', 'b.ID_pembayaran=y.ID_pembayaran');
			$this->db->where('t.Kode_transaksi=b.Kode_transaksi and t.ID_Kasir=k.ID_kasir');
			$this->db->group_by('t.Kode_transaksi');
			$this->db->order_by('t.Kode_transaksi', 'DESC');


			$query = $this->db->get();
			return $query->result();

		}

	public function detailStrukTunai($Kode_transaksi) 
	{
	   $this->db->select('t.Kode_transaksi as Kode_transaksi');
	   $this->db->select('t.Tanggal_transaksi');
	   $this->db->select('t.Waktu_transaksi');
	   $this->db->select('k.Nama_kasir');
	   $this->db->select('b.Nama_barang');
	   $this->db->select('SUM(d.Jumlah) as Jumlah');
	   $this->db->select('format(b.Harga_barang,0,"de_DE") as Harga_barang');
	   $this->db->select('format(SUM(d.Harga_total),0,"de_DE") as Harga_Total');
	   $this->db->select('format(t.Estimasi_pembayaran,0,"de_DE") as Estimasi_pembayaran');
	   $this->db->select('format((t.Estimasi_pembayaran-p.Total_bayar),0,"de_DE") as diskon');
	   $this->db->select('format(p.Total_bayar,0,"de_DE") as Total_bayar');
	   $this->db->select('format(n.tunai,0,"de_DE") as tunai');
	   $this->db->select('format((n.tunai-p.Total_bayar),0,"de_DE") as kembalian');

	   $this->db->from('transaksi t');

	   $this->db->JOIN('kasir k', 't.ID_Kasir=k.ID_Kasir');
	   $this->db->JOIN('detail_transaksi d', 't.Kode_transaksi=d.Kode_transaksi');
	   $this->db->JOIN('barang b', 'b.Barcode=d.Barcode');
	   $this->db->JOIN('pembayaran p', 't.Kode_transaksi=p.Kode_transaksi');
	   $this->db->JOIN('pembayaran_tunai n','p.ID_pembayaran=n.ID_pembayaran');

	   $this->db->where('t.Kode_transaksi', $Kode_transaksi);

	   $this->db->group_by('d.Barcode');

	   $query = $this->db->get();
	   return $query->result();
	}

	public function detailStrukTunaiPrint($Kode_transaksi) 
	{
	   $this->db->select('t.Kode_transaksi as Kode_transaksi');
	   $this->db->select('t.Tanggal_transaksi');
	   $this->db->select('t.Waktu_transaksi');
	   $this->db->select('k.Nama_kasir');
	   $this->db->select('b.Nama_barang');
	   $this->db->select('SUM(d.Jumlah) as Jumlah');
	   $this->db->select('format(b.Harga_barang,0,"de_DE") as Harga_barang');
	   $this->db->select('format(SUM(d.Harga_total),0,"de_DE") as Harga_Total');
	   $this->db->select('format(t.Estimasi_pembayaran,0,"de_DE") as Estimasi_pembayaran');
	   $this->db->select('format((t.Estimasi_pembayaran-p.Total_bayar),0,"de_DE") as diskon');
	   $this->db->select('format(p.Total_bayar,0,"de_DE") as Total_bayar');
	   $this->db->select('format(n.tunai,0,"de_DE") as tunai');
	   $this->db->select('format((n.tunai-p.Total_bayar),0,"de_DE") as kembalian');

	   $this->db->from('transaksi t');

	   $this->db->JOIN('kasir k', 't.ID_Kasir=k.ID_Kasir');
	   $this->db->JOIN('detail_transaksi d', 't.Kode_transaksi=d.Kode_transaksi');
	   $this->db->JOIN('barang b', 'b.Barcode=d.Barcode');
	   $this->db->JOIN('pembayaran p', 't.Kode_transaksi=p.Kode_transaksi');
	   $this->db->JOIN('pembayaran_tunai n','p.ID_pembayaran=n.ID_pembayaran');

	   $this->db->where('t.Kode_transaksi', $Kode_transaksi);

	   $this->db->group_by('d.Barcode');

	   $query = $this->db->get();
	   return $query->row();
	}

	public function detailStrukWallet($Kode_transaksi) 
	{
	   $this->db->select('t.Kode_transaksi');
	   $this->db->select('t.Tanggal_transaksi');
	   $this->db->select('t.Waktu_transaksi');
	   $this->db->select('k.Nama_kasir');
	   $this->db->select('b.Nama_barang');
	   $this->db->select('SUM(d.Jumlah) as Jumlah');
	   $this->db->select('n.Nama_pembayaran');
	   $this->db->select('format(b.Harga_barang,0,"de_DE") as Harga_barang');
	   $this->db->select('format(SUM(d.Harga_total),0,"de_DE") as Harga_Total');
	   $this->db->select('format(t.Estimasi_pembayaran,0,"de_DE") as Estimasi_pembayaran');
	   $this->db->select('format((t.Estimasi_pembayaran-p.Total_bayar),0,"de_DE") as diskon');
	   $this->db->select('format(p.Total_bayar,0,"de_DE") as Total_bayar');

	   $this->db->from('transaksi t');

	   $this->db->JOIN('kasir k', 't.ID_Kasir=k.ID_Kasir');
	   $this->db->JOIN('detail_transaksi d', 't.Kode_transaksi=d.Kode_transaksi');
	   $this->db->JOIN('barang b', 'b.Barcode=d.Barcode');
	   $this->db->JOIN('pembayaran p', 't.Kode_transaksi=p.Kode_transaksi');
	   $this->db->JOIN('pembayaran_wallet n','p.ID_pembayaran=n.ID_pembayaran');

	   $this->db->where('t.Kode_transaksi', $Kode_transaksi);

	   $this->db->group_by('d.Barcode');

	   $query = $this->db->get();
	   return $query->result();
	}

	public function detailStrukWalletPrint($Kode_transaksi) 
	{
	   $this->db->select('t.Kode_transaksi');
	   $this->db->select('t.Tanggal_transaksi');
	   $this->db->select('t.Waktu_transaksi');
	   $this->db->select('k.Nama_kasir');
	   $this->db->select('b.Nama_barang');
	   $this->db->select('SUM(d.Jumlah) as Jumlah');
	   $this->db->select('n.Nama_pembayaran');
	   $this->db->select('format(b.Harga_barang,0,"de_DE") as Harga_barang');
	   $this->db->select('format(SUM(d.Harga_total),0,"de_DE") as Harga_Total');
	   $this->db->select('format(t.Estimasi_pembayaran,0,"de_DE") as Estimasi_pembayaran');
	   $this->db->select('format((t.Estimasi_pembayaran-p.Total_bayar),0,"de_DE") as diskon');
	   $this->db->select('format(p.Total_bayar,0,"de_DE") as Total_bayar');

	   $this->db->from('transaksi t');

	   $this->db->JOIN('kasir k', 't.ID_Kasir=k.ID_Kasir');
	   $this->db->JOIN('detail_transaksi d', 't.Kode_transaksi=d.Kode_transaksi');
	   $this->db->JOIN('barang b', 'b.Barcode=d.Barcode');
	   $this->db->JOIN('pembayaran p', 't.Kode_transaksi=p.Kode_transaksi');
	   $this->db->JOIN('pembayaran_wallet n','p.ID_pembayaran=n.ID_pembayaran');

	   $this->db->where('t.Kode_transaksi', $Kode_transaksi);

	   $this->db->group_by('d.Barcode');

	   $query = $this->db->get();
	   return $query->row();
	}


	public function detailStrukDebit($Kode_transaksi) 
	{
	   $this->db->select('t.Kode_transaksi');
	   $this->db->select('t.Tanggal_transaksi');
	   $this->db->select('t.Waktu_transaksi');
	   $this->db->select('k.Nama_kasir');
	   $this->db->select('b.Nama_barang');
	   $this->db->select('SUM(d.Jumlah) as Jumlah');
	   $this->db->select('n.Nama_bank');
	   $this->db->select('format(b.Harga_barang,0,"de_DE") as Harga_barang');
	   $this->db->select('format(SUM(d.Harga_total),0,"de_DE") as Harga_Total');
	   $this->db->select('format(t.Estimasi_pembayaran,0,"de_DE") as Estimasi_pembayaran');
	   $this->db->select('format((t.Estimasi_pembayaran-p.Total_bayar),0,"de_DE") as diskon');
	   $this->db->select('format(p.Total_bayar,0,"de_DE") as Total_bayar');

	   $this->db->from('transaksi t');

	   $this->db->JOIN('kasir k', 't.ID_Kasir=k.ID_Kasir');
	   $this->db->JOIN('detail_transaksi d', 't.Kode_transaksi=d.Kode_transaksi');
	   $this->db->JOIN('barang b', 'b.Barcode=d.Barcode');
	   $this->db->JOIN('pembayaran p', 't.Kode_transaksi=p.Kode_transaksi');
	   $this->db->JOIN('pembayaran_debit n','p.ID_pembayaran=n.ID_pembayaran');

	   $this->db->where('t.Kode_transaksi', $Kode_transaksi);

	   $this->db->group_by('d.Barcode');

	   $query = $this->db->get();
	   return $query->result();
	}

	public function detailStrukDebitPrint($Kode_transaksi) 
	{
	   $this->db->select('t.Kode_transaksi');
	   $this->db->select('t.Tanggal_transaksi');
	   $this->db->select('t.Waktu_transaksi');
	   $this->db->select('k.Nama_kasir');
	   $this->db->select('b.Nama_barang');
	   $this->db->select('SUM(d.Jumlah) as Jumlah');
	   $this->db->select('n.Nama_bank');
	   $this->db->select('format(b.Harga_barang,0,"de_DE") as Harga_barang');
	   $this->db->select('format(SUM(d.Harga_total),0,"de_DE") as Harga_Total');
	   $this->db->select('format(t.Estimasi_pembayaran,0,"de_DE") as Estimasi_pembayaran');
	   $this->db->select('format((t.Estimasi_pembayaran-p.Total_bayar),0,"de_DE") as diskon');
	   $this->db->select('format(p.Total_bayar,0,"de_DE") as Total_bayar');

	   $this->db->from('transaksi t');

	   $this->db->JOIN('kasir k', 't.ID_Kasir=k.ID_Kasir');
	   $this->db->JOIN('detail_transaksi d', 't.Kode_transaksi=d.Kode_transaksi');
	   $this->db->JOIN('barang b', 'b.Barcode=d.Barcode');
	   $this->db->JOIN('pembayaran p', 't.Kode_transaksi=p.Kode_transaksi');
	   $this->db->JOIN('pembayaran_debit n','p.ID_pembayaran=n.ID_pembayaran');

	   $this->db->where('t.Kode_transaksi', $Kode_transaksi);

	   $this->db->group_by('d.Barcode');

	   $query = $this->db->get();
	   return $query->row();
	}


	public function getTunai()
	{

		$this->db->select('t.Kode_transaksi');
		$this->db->select('(t.Estimasi_pembayaran - ((s.Total_diskon * t.Estimasi_pembayaran) / 100)) as total');

		$this->db->from('transaksi t');
		$this->db->JOIN('diskon s', 't.Kode_diskon=s.Kode_diskon');

		$query = $this->db->get();
		return $query->result();
	}

	public function  get_Diskon($get_diskon)
	{
		$query  = "SELECT * FROM diskon WHERE Kode_diskon='$get_diskon'";

		return $this->db->query($query)->result();
	}


	public function input_transaksi($kode_trx, $estimasi_pembayaran, $tanggal, $waktu, $id_kasir, $jumlah_barang)
	{
		$data = array (
		    	'Kode_transaksi' => $kode_trx,
		    	'Tanggal_transaksi' => $tanggal,
		    	'Waktu_transaksi' => $waktu,
		    	'Estimasi_pembayaran' => $estimasi_pembayaran,
		    	'Jumlah_barang' => $jumlah_barang,
		    	'ID_Kasir' => $id_kasir,
		    );

		    $this->db->insert('transaksi', $data);
	}

	public function input_pembayarann($idBayar, $estimasi_pembayaran, $tanggal, $waktu, $kode_trx)
	{
		$data = array (
		    	'ID_pembayaran' => $idBayar,
		    	'Total_bayar' => $estimasi_pembayaran,
		    	'Waktu_pembayaran' => $waktu,
		    	'Tanggal_pembayaran' => $tanggal,
		    	'Kode_transaksi' => $kode_trx
		    );

		    $this->db->insert('pembayaran', $data);
	}

	public function input_pembayaran($idBayar, $bayar, $tanggal, $waktu, $kode_trx)
	{
		$data = array (
		    	'ID_pembayaran' => $idBayar,
		    	'Total_bayar' => $bayar,
		    	'Waktu_pembayaran' => $waktu,
		    	'Tanggal_pembayaran' => $tanggal,
		    	'Kode_transaksi' => $kode_trx
		    );

		    $this->db->insert('pembayaran', $data);
	}

	public function input_pembayaranTunai($idBayar, $jenis_diskon, $estimasi_pembayaran, $tunai)
	{
		$kd_tunai = rand(1,1000);
		$data = array (
		    	'ID_pembayaran' => $idBayar,
		    	'ID_tunai' => $kd_tunai,
		    	'Kode_diskon' => $jenis_diskon,
		    	'Jumlah_pembayaran' => $estimasi_pembayaran,
		    	'Tunai' => $tunai
		    );

		    $this->db->insert('pembayaran_tunai', $data);
	}


	public function input_pembayaranWallet($idBayar, $nomor_transaksi, $jenis_diskon, $estimasi_pembayaran, $nama_pembayaran)
	{
		$kd_tunai = rand(1,1000);
		$data = array (
		    	'ID_pembayaran' => $idBayar,
		    	'Nomor_transaksi' => $nomor_transaksi,
		    	'Kode_diskon' => $jenis_diskon,
		    	'Total_tagihan' => $estimasi_pembayaran,
		    	'Nama_pembayaran' => $nama_pembayaran
		    );

		    $this->db->insert('pembayaran_wallet', $data);
	}

	public function input_pembayaranDebit($idBayar, $nomor_kartu, $jenis_diskon, $estimasi_pembayaran, $nama_bank)
	{
		$data = array (
		    	'ID_pembayaran' => $idBayar,
		    	'Nomor_kartu' => $nomor_kartu,
		    	'Kode_diskon' => $jenis_diskon,
		    	'Total_tagihan' => $estimasi_pembayaran,
		    	'Nama_bank' => $nama_bank
		    );

		    $this->db->insert('pembayaran_debit', $data);
	}

}

?>