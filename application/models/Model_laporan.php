<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Model_laporan extends CI_Model
{
	
	public function getTransaksipenjualan()
	{
		
		$this->db->from('detail_transaksi t');
		$this->db->join('barang p', 't.Barcode = p.Barcode');
		$this->db->join('kasir k', 't.ID_Kasir = k.ID_kasir');
		$query = $this->db->get();
		return $query->result();
	}

	public function getDatapenjualan()
	{
		$this->db->select('b.Nama_barang');
		$this->db->select('b.SKU');
		$this->db->select('k.Nama_kategori');
		$this->db->select('SUM(d.Jumlah) as Terjual');
		$this->db->select('SUM(d.Harga_satuan * d.Jumlah) as Total');
		$this->db->from('detail_transaksi d');
		$this->db->join('barang b', 'd.Barcode = b.Barcode');
		$this->db->join('kategori k', 'k.Kode_kategori = b.Kode_kategori');
		$this->db->group_by('b.Barcode');
		$query = $this->db->get();
		return $query->result();
	}

	public function getGrandtotalpenjualan()
	{
		$this->db->select('SUM(Harga_total) as Grandtotal');
		$this->db->select('SUM(Jumlah) as Granditem');
		$this->db->from('detail_transaksi');
		$query = $this->db->get();
		return $query->result();
	}

	public function getGrandtotaltransaksi()
	{
		$this->db->select('SUM(Harga_satuan) as Grandtotal');
		$this->db->select('SUM(Jumlah) as Granditem');
		$this->db->from('detail_transaksi');
		$query = $this->db->get();
		return $query->result();
	}

	public function getGrandtotalpenjualanbySearch($tgl)
	{
		$pecah = explode( "-",$tgl);
		$tanggal1 = $pecah['0'];
		$pecah1 = explode("/", $tanggal1);
		$hasiltgl1 = str_replace(' ','', $pecah1['2']."-".$pecah1['0']."-".$pecah1['1']);
		$tes = str_replace(' ', '', $hasiltgl1);

		$tanggal2 = $pecah['1'];
		$pecah2 = explode("/", $tanggal2);
		$hasiltgl2 = str_replace(' ', '', $pecah2['2']."-".$pecah2['0']."-".$pecah2['1']);

		$this->db->select('SUM(Harga_total) as Grandtotal');
		$this->db->select('SUM(Jumlah) as Granditem');
		$this->db->from('detail_transaksi');
		// $this->db->where('tanggal_transaksi BETWEEN "2019-04-14" AND "2019-04-15"');
		$this->db->where('tanggal_transaksi BETWEEN "'.$hasiltgl1.'" AND "'.$hasiltgl2.'"');
		$query = $this->db->get();
		return $query->result();
	}

	public function getRentangwaktu($tgl)
	{
		$pecah = explode( "-",$tgl);
		$tanggal1 = $pecah['0'];
		$pecah1 = explode("/", $tanggal1);
		$hasiltgl1 = str_replace(' ','', $pecah1['2']."-".$pecah1['0']."-".$pecah1['1']);
		$tes = str_replace(' ', '', $hasiltgl1);

		$tanggal2 = $pecah['1'];
		$pecah2 = explode("/", $tanggal2);
		$hasiltgl2 = str_replace(' ', '', $pecah2['2']."-".$pecah2['0']."-".$pecah2['1']);

		$this->db->select('b.Nama_barang');
		$this->db->select('b.SKU');
		$this->db->select('k.Nama_kategori');
		$this->db->select('SUM(d.Jumlah) as Terjual');
		$this->db->select('SUM(d.Harga_satuan * d.Jumlah) as Total');
		$this->db->from('detail_transaksi d');
		$this->db->join('barang b', 'd.Barcode = b.Barcode');
		$this->db->join('kategori k', 'k.Kode_kategori = b.Kode_kategori');
		// $this->db->where('tanggal_transaksi BETWEEN "2019-04-14" AND "2019-04-18"');
		$this->db->where('tanggal_transaksi BETWEEN "'.$hasiltgl1.'" AND "'.$hasiltgl2.'"');
		$this->db->group_by('b.Barcode');
		$query = $this->db->get();
		return $query->result();

	}

	public function getRentangwaktutransaksi($tgl)
	{
		$pecah = explode( "-",$tgl);
		$tanggal1 = $pecah['0'];
		$pecah1 = explode("/", $tanggal1);
		$hasiltgl1 = str_replace(' ','', $pecah1['2']."-".$pecah1['0']."-".$pecah1['1']);
		$tes = str_replace(' ', '', $hasiltgl1);

		$tanggal2 = $pecah['1'];
		$pecah2 = explode("/", $tanggal2);
		$hasiltgl2 = str_replace(' ', '', $pecah2['2']."-".$pecah2['0']."-".$pecah2['1']);

		$this->db->from('detail_transaksi t');
		$this->db->join('barang p', 't.Barcode = p.Barcode');
		$this->db->join('kasir k', 't.ID_Kasir = k.ID_kasir');
		$this->db->where('tanggal_transaksi BETWEEN "'.$hasiltgl1.'" AND "'.$hasiltgl2.'"');
		$query = $this->db->get();
		return $query->result();

	}

	public function getGrandtotaltransaksibySearch($tgl)
	{
		$pecah = explode( "-",$tgl);
		$tanggal1 = $pecah['0'];
		$pecah1 = explode("/", $tanggal1);
		$hasiltgl1 = str_replace(' ','', $pecah1['2']."-".$pecah1['0']."-".$pecah1['1']);
		$tes = str_replace(' ', '', $hasiltgl1);

		$tanggal2 = $pecah['1'];
		$pecah2 = explode("/", $tanggal2);
		$hasiltgl2 = str_replace(' ', '', $pecah2['2']."-".$pecah2['0']."-".$pecah2['1']);

		$this->db->select('SUM(Harga_satuan) as Grandtotal');
		$this->db->select('SUM(Jumlah) as Granditem');
		$this->db->from('detail_transaksi');
		// $this->db->where('tanggal_transaksi BETWEEN "2019-04-14" AND "2019-04-15"');
		$this->db->where('tanggal_transaksi BETWEEN "'.$hasiltgl1.'" AND "'.$hasiltgl2.'"');
		$query = $this->db->get();
		return $query->result();
	}

	public function labaProduk()
	{
		$this->db->select('b.Nama_barang');
		$this->db->select('b.SKU');
		$this->db->select('m.Tanggal_stok_masuk');
		$this->db->select('SUM(d.Harga_satuan * d.Jumlah) as Penjualan');
		$this->db->select('SUM(m.Harga_beli * d.Jumlah) as Pembelian');
		$this->db->select('SUM(d.Harga_satuan * d.Jumlah) - SUM(m.Harga_beli * d.Jumlah) as Laba');
		$this->db->from('detail_transaksi d');
		$this->db->join('barang b','d.barcode = b.Barcode');
		$this->db->join('stok s','s.Barcode = b.Barcode');
		$this->db->join('stok_masuk m','s.ID_stok = m.ID_stok');
		$this->db->group_by('m.ID_stok_masuk');

		$query = $this->db->get();
		return $query->result();
	}

	public function getGrandtotalLaba()
	{
		$this->db->select('b.Nama_barang');
		$this->db->select('b.SKU');
		$this->db->select('SUM(d.Harga_satuan * d.Jumlah) as Penjualan');
		$this->db->select('SUM(m.Harga_beli * d.Jumlah) as Pembelian');
		$this->db->select('SUM(b.Harga_barang - m.Harga_beli) as Laba');
		$this->db->from('detail_transaksi d');
		$this->db->join('barang b','d.Barcode = b.Barcode');
		$this->db->join('stok s','s.Barcode = b.Barcode');
		$this->db->join('stok_masuk m','s.ID_stok = m.ID_stok');

		$query = $this->db->get();
		return $query->result();
	}

	public function getRentangwaktulaba($tgl)
	{
		$pecah = explode( "-",$tgl);
		$tanggal1 = $pecah['0'];
		$pecah1 = explode("/", $tanggal1);
		$hasiltgl1 = str_replace(' ','', $pecah1['2']."-".$pecah1['0']."-".$pecah1['1']);
		$tes = str_replace(' ', '', $hasiltgl1);

		$tanggal2 = $pecah['1'];
		$pecah2 = explode("/", $tanggal2);
		$hasiltgl2 = str_replace(' ', '', $pecah2['2']."-".$pecah2['0']."-".$pecah2['1']);

		$this->db->select('b.Nama_barang');
		$this->db->select('b.SKU');
		$this->db->select('SUM(d.Harga_satuan * d.Jumlah) as Penjualan');
		$this->db->select('SUM(m.Harga_beli * d.Jumlah) as Pembelian');
		$this->db->select('SUM(d.Harga_satuan * d.Jumlah) - SUM(m.Harga_beli * d.Jumlah) as Laba');
		$this->db->from('detail_transaksi d');
		$this->db->join('barang b','d.Barcode = b.Barcode');
		$this->db->join('stok s','s.Barcode = b.Barcode');
		$this->db->join('stok_masuk m','s.ID_stok = m.ID_stok');
		$this->db->where('tanggal_transaksi BETWEEN "'.$hasiltgl1.'" AND "'.$hasiltgl2.'"');
		$this->db->group_by('d.Barcode');

		$query = $this->db->get();
		return $query->result();
	}

	public function getGrandtotallababySearch($tgl)
	{

		$pecah = explode( "-",$tgl);
		$tanggal1 = $pecah['0'];
		$pecah1 = explode("/", $tanggal1);
		$hasiltgl1 = str_replace(' ','', $pecah1['2']."-".$pecah1['0']."-".$pecah1['1']);
		$tes = str_replace(' ', '', $hasiltgl1);

		$tanggal2 = $pecah['1'];
		$pecah2 = explode("/", $tanggal2);
		$hasiltgl2 = str_replace(' ', '', $pecah2['2']."-".$pecah2['0']."-".$pecah2['1']);
		$this->db->select('b.Nama_barang');
		$this->db->select('b.SKU');
		$this->db->select('SUM(d.Harga_satuan * d.Jumlah) as Penjualan');
		$this->db->select('SUM(m.Harga_beli * d.Jumlah) as Pembelian');
		$this->db->select('SUM(b.Harga_barang - m.Harga_beli) as Laba');
		$this->db->from('detail_transaksi d');
		$this->db->join('barang b','d.Barcode = b.Barcode');
		$this->db->join('stok s','s.Barcode = b.Barcode');
		$this->db->join('stok_masuk m','s.ID_stok = m.ID_stok');
		$this->db->where('d.tanggal_transaksi BETWEEN "'.$hasiltgl1.'" AND "'.$hasiltgl2.'"');

		$query = $this->db->get();
		return $query->result();
	}

	public function getpenjualanKategori()
	{
		$this->db->select('Nama_kategori');
		$this->db->select('SUM(Jumlah) as Terjual');
		$this->db->select('SUM(Harga_total) as Total');
		$this->db->select('SUM(Harga_total) / SUM(Jumlah) as Rata');
		$this->db->from('detail_transaksi d');
		$this->db->join('barang b','d.Barcode = b.Barcode');
		$this->db->join('kategori k','b.Kode_kategori = k.Kode_kategori');
		$this->db->group_by('k.Kode_kategori ');
		$query = $this->db->get();
		return $query->result();
	}

	public function getGrandtotalPenjualankategori()
	{
		$this->db->select('Nama_kategori');
		$this->db->select('SUM(Jumlah) as Terjual');
		$this->db->select('SUM(Harga_total) as Total');
		$this->db->select('SUM(Harga_total) / SUM(Jumlah) as Rata');
		$this->db->from('detail_transaksi d');
		$this->db->join('barang b','d.Barcode = b.Barcode');
		$this->db->join('kategori k','b.Kode_kategori = k.Kode_kategori');

		$query = $this->db->get();
		return $query->result();
	}

	public function getRentangwaktupenjualanKategori($tgl)
 	{
	 	$pecah = explode( "-",$tgl);
	    $tanggal1 = $pecah['0'];
	    $pecah1 = explode("/", $tanggal1);
	    $hasiltgl1 = str_replace(' ','', $pecah1['2']."-".$pecah1['0']."-".$pecah1['1']);
	    $tes = str_replace(' ', '', $hasiltgl1);

	    $tanggal2 = $pecah['1'];
	    $pecah2 = explode("/", $tanggal2);
	    $hasiltgl2 = str_replace(' ', '', $pecah2['2']."-".$pecah2['0']."-".$pecah2['1']);

	    $this->db->select('Nama_kategori');
	    $this->db->select('SUM(Jumlah) as Terjual');
	    $this->db->select('SUM(Harga_total) as Total');
	    $this->db->select('SUM(Harga_total) / SUM(Jumlah) as Rata');
	    $this->db->from('detail_transaksi d');
	    $this->db->join('barang b','d.Barcode = b.Barcode');
	    $this->db->join('kategori k','b.Kode_kategori = k.Kode_kategori');
	    $this->db->where('d.tanggal_transaksi BETWEEN "'.$hasiltgl1.'" AND "'.$hasiltgl2.'"');
	    $this->db->group_by('Nama_kategori');

	    $query = $this->db->get();
	    return $query->result();
  	}

	public function getGrandtotalpenjualanKategoribySearch($tgl)
	{
		$pecah = explode( "-",$tgl);
		$tanggal1 = $pecah['0'];
		$pecah1 = explode("/", $tanggal1);
		$hasiltgl1 = str_replace(' ','', $pecah1['2']."-".$pecah1['0']."-".$pecah1['1']);
		$tes = str_replace(' ', '', $hasiltgl1);

		$tanggal2 = $pecah['1'];
		$pecah2 = explode("/", $tanggal2);
		$hasiltgl2 = str_replace(' ', '', $pecah2['2']."-".$pecah2['0']."-".$pecah2['1']);

		$this->db->select('Nama_kategori');
		$this->db->select('SUM(Jumlah) as Terjual');
		$this->db->select('SUM(Harga_total) as Total');
		$this->db->select('SUM(Harga_total) / SUM(Jumlah) as Rata');
		$this->db->from('detail_transaksi d');
		$this->db->join('barang b','d.Barcode = b.Barcode');
		$this->db->join('kategori k','b.Kode_kategori = k.Kode_kategori');
		$this->db->where('d.tanggal_transaksi BETWEEN "'.$hasiltgl1.'" AND "'.$hasiltgl2.'"');

		$query = $this->db->get();
		return $query->result();

	}

	public function getHistoryKasir(){
		$this->db->select('Tanggal');
		$this->db->select('Jam_buka');
		$this->db->select('Saldo_awal');
		$this->db->select('Jam_tutup');
		$this->db->select('Saldo_akhir');
		$this->db->select('(Saldo_akhir - Saldo_awal) as Pendapatan');
		$this->db->select('ID_kasir');
		$this->db->from('oprkasir');

		$query = $this->db->get();
		return $query->result();

	}
	
}


 ?>