<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Model_Login extends CI_Model {


		function cekLogin($id_pengelola, $pass) {
			
			$this->db->from('pengelola');
			$this->db->where('ID_pengelola', $id_pengelola);
			$this->db->where('Password', $pass);

			  $query = $this->db->get();
			return $query->result();

		}

		function getPengelola($id_pengelola) {
			$this->db->select('*');
			$this->db->from('pengelola');
			$this->db->where('ID_pengelola', $id_pengelola);
			$query = $this->db->get(); 
	    	return $query->result();
		}

}

?>