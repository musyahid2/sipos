<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Model_OprKasir extends CI_Model
{

	public function getAllOprKasir()
	{

		$this->db->from('oprkasir o ');
		$this->db->join('kasir k', 'o.ID_kasir = k.ID_kasir');
		$query = $this->db->get();
		return $query->result();

	}

	public function getOpr($ID_opr)
	{
		$this->db->from('oprkasir');
		$this->db->where('ID_opr', $ID_opr);

		$query = $this->db->get();
		return $query->result();
	}

	public function input_OprKasir($Saldo_awal, $ID_kasir)
	{
		$data = array (
		'Tanggal' => date('Y-m-d'),
		'Jam_buka' => date('H:i:s'),
		'Saldo_awal' => $Saldo_awal,
    'ID_kasir' => $ID_kasir
	);

	$this->db->insert('oprkasir', $data);
	}

	public function update_OprKasir($Saldo_akhir, $ID_kasir)
	{
		$data = array (
      'Jam_tutup' => date('H:i:s'),
			'Saldo_akhir' => $Saldo_akhir
    );

		$this->db->where('Tanggal', date('Y-m-d'));
		$this->db->where('ID_kasir', $ID_kasir);
		$this->db->update('oprkasir', $data);
	}

	public function hapus_OprKasir($ID_opr){
		$this->db->where('ID_opr', $ID_opr);
		$this->db->delete('oprkasir');
	}

}


 ?>
