<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Model_Kategori extends CI_Model
{
	
	public function getAllKategori()
	{
		
		$this->db->from('kategori k');
		$this->db->join('pengelola p', 'k.Id_pengelola = p.Id_pengelola');
		$query = $this->db->get();
		return $query->result();

	}

	public function getKategori($Kode_kategori)
	{
		$this->db->from('kategori');
		$this->db->where('Kode_kategori', $Kode_kategori);

		$query = $this->db->get();
		return $query->result();
	}

	public function input_kategori($Kode_kategori, $Nama_kategori, $Id_pengelola)
	{
		$data = array (
		'Kode_kategori' => $Kode_kategori,
		'Nama_kategori' => $Nama_kategori,
		'ID_pengelola' => $Id_pengelola
	);

	$this->db->insert('kategori', $data);
	}

	public function update_kategori($kode, $namaKat, $Id_pengelola)
	{
		$data = array (
			'Kode_kategori' => $kode,
			'Nama_kategori' => $namaKat,
			'ID_pengelola' => $Id_pengelola
		);

		$this->db->where('Kode_kategori', $kode);
		$this->db->update('kategori', $data);
	}

	public function hapus_kategori($Kode_Kategori){
		$this->db->where('Kode_kategori', $Kode_Kategori);
		$this->db->delete('kategori');
	}
	
}


 ?>