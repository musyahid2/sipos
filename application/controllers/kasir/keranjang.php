<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class keranjang extends CI_Controller {


	public function __construct()
	  {
	    parent::__construct();
	    $this->load->model('Model_LoginKasir');
	     $this->load->model('Model_Produk');
	     $this->load->model('Model_Transaksi');
	     $this->load->model('Model_ID');
	    if(is_null($this->session->userdata('id_kas'))){
      	redirect('kasir/auth/login');
    	}
  }


	public function index()
	{
		// $data = $this->Model_Produk->getdaftarProduk();
		// $this->session->set_userdata('all_data', $data);
		$this->load->view('kasir/keranjang/daftar_itemBarang');
	}


	function display_info(){
		
		$data['dataProduk'] = $this->Model_Produk->getdaftarProduk();	
		$this->load->view('kasir/keranjang/daftar_item', $data);
	}

	function search_name(){
		if(isset($_GET['name'])){
			$name=$_GET['name'];
		}

		$data['dataProduk'] = $this->Model_Produk->getProduksearch($name);
		
		$this->load->view('kasir/keranjang/daftar_item', $data);
	}

		function get_barang(){
		$kode=$this->input->post('kode');
		$data=$this->Model_Produk->get_data_barang_bykode($kode);
		echo json_encode($data);
	}


	public function prosesBayar()
	{
		$namaProduk = $this->input->post("nama");
		$hargaTotal = $this->input->post("harga");
		$quantiti = $this->input->post("quantiti");

		$jumlah = count($namaProduk);
		$grandtotal = 0;
		$qty = 0;
		for($x=0; $x < $jumlah; $x++) {
	    $hrg_total =  $hargaTotal[$x] * $quantiti[$x];
	   	$grandtotal = $grandtotal +  $hrg_total;
	   		
		}


		$total = $grandtotal;

		$hargaSatuan = $this->input->post("hargasatuan");
		$barcode = $this->input->post("barcode");
		$id_kasir = $this->session->userdata("id_kas");

		$sessionBarang = array (
				'total2' => $total,
				'total' => $total,
				'jumlah' => array_sum($quantiti)
			);

		// print_r($sessionBarang);
			
		$this->session->set_userdata($sessionBarang);

				
		$this->Model_Transaksi->inputProduk($namaProduk, $quantiti, $hargaSatuan, $hargaTotal, $barcode, $jumlah, $total, $id_kasir);

		$this->Model_Transaksi->kurangistok($barcode, $quantiti);

		redirect('kasir/pembayaran');
	}

}

