<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class pembayaran extends CI_Controller
{
	
	public function __construct()
  {
    parent::__construct();
    $this->load->model('Model_Pembayaran');
    $this->load->model('Model_ID');
    $this->load->model('Model_metodePembayaran');
    $this->load->library('pdf_report');
  }

  public function index()
  {
    $data['metode_pembayaranWallet'] = $this->Model_metodePembayaran->getAllWallet();
    $data['metode_pembayaranDebit'] = $this->Model_metodePembayaran->getAllDebit();
    $this->load->view('kasir/pembayaran/metode_pembayaran/MetodePembayaran', $data);
  }

  public function pembayarantunai()
  {
    $data['id_pembayaran']=$this->Model_ID->get_IDpembayaran();
    $data['kode_transaksi']=$this->Model_ID->get_kd_transaksi();
    $diskon = $this->Model_Pembayaran->getAllDiskon();
    $this->session->set_userdata('diskon', $diskon);
    $this->load->view('kasir/pembayaran/metode_pembayaran/BayarTunai', $data);  
  }

  public function pembayaranWallet()
  {
    $Id_metodePembayaran = $_GET['Id_metodePembayaran'];
    $data['id_pembayaran']=$this->Model_ID->get_IDpembayaran();
    $data['kode_transaksi']=$this->Model_ID->get_kd_transaksi();
    $data['pembayaranWallet'] = $this->Model_metodePembayaran->getWallet($Id_metodePembayaran);
    $diskon = $this->Model_Pembayaran->getAllDiskon();
    $this->session->set_userdata('diskon', $diskon);
    $this->load->view('kasir/pembayaran/metode_pembayaran/BayarWallet', $data);  
  }

  public function pembayaranDebit()
  {
    $Id_metodePembayaran = $_GET['Id_metodePembayaran'];
    $data['id_pembayaran']=$this->Model_ID->get_IDpembayaran();
    $data['kode_transaksi']=$this->Model_ID->get_kd_transaksi();
    $data['pembayaranDebit'] = $this->Model_metodePembayaran->getDebit($Id_metodePembayaran);
    $diskon = $this->Model_Pembayaran->getAllDiskon();
    $this->session->set_userdata('diskon', $diskon);
    $this->load->view('kasir/pembayaran/metode_pembayaran/BayarDebit', $data);  
  }

  public function inputTunai()
  {
  	$idBayar = $this->input->post('id_pembayaran');
    $kode_trx = $this->input->post('kode_transaksi');
    $estimasi_pembayaran = $this->input->post('total_tagihan');
    $tanggal = date("Y-m-d");
    $waktu = date("H:i:s");
    $bayar_tunai = $this->input->post('tunai');
    $byr_tunai = preg_replace('/[^A-Za-z0-9]/', '', $bayar_tunai);
    $tunai = (int) $byr_tunai;
    $id_kasir = $this->session->userdata("id_kas");
    $jumlah_barang = $this->input->post('jumlah_barang');
    $jenis_diskon = $this->input->post('diskon');
    $data = $this->Model_Pembayaran->get_diskon($jenis_diskon);
    $this->session->set_userdata('diskon', $data);

    $get = $this->session->userdata('diskon');
    foreach ($get as $cek) {
      $persen = $cek->Persentase;
      $potongan = $cek->Potongan_Harga;
      $min_trans = $cek->Min_Transaksi;
      $max_dis = $cek->Max_Diskon;

      if ($tunai < $estimasi_pembayaran) {
        $this->session->set_flashdata('message', '
          <div class="alert alert-block alert-danger"></i><a  class="close" data-dismiss="alert" aria-label="close">&times;</a></button>
          <i class="ace-icon fa fa-bullhorn danger"></i> Uang tunai kurang dari Jumlah Pembayaran
          </div>');
        redirect('kasir/pembayaran/pembayarantunai','refresh');
      }
      else{
        if ($persen!=0 && $potongan==0 && $min_trans==0 && $max_dis==0) { //all nominal  pakai persen (sudah)
          $bayar = $estimasi_pembayaran-($estimasi_pembayaran*$persen)/100;

          $this->Model_Pembayaran->input_transaksi($kode_trx, $estimasi_pembayaran, $tanggal, $waktu, $id_kasir, $jumlah_barang);
          $this->Model_Pembayaran->input_pembayaran($idBayar, $bayar, $tanggal, $waktu,  $kode_trx);
         $this->Model_Pembayaran->input_pembayaranTunai($idBayar, $jenis_diskon, $estimasi_pembayaran, $tunai);
        }

       elseif //tidak ada diskon

       ($persen==0 && $potongan==0 && $min_trans==0 && $max_dis==0) {
        $bayar = $estimasi_pembayaran;
        
        $this->Model_Pembayaran->input_transaksi($kode_trx, $estimasi_pembayaran, $tanggal, $waktu, $id_kasir, $jumlah_barang);
        $this->Model_Pembayaran->input_pembayaran($idBayar, $bayar, $tanggal, $waktu,  $kode_trx);
        $this->Model_Pembayaran->input_pembayaranTunai($idBayar, $jenis_diskon, $estimasi_pembayaran, $tunai);
      }


      elseif //all nominal potongan harga (sudah)
      
      ($persen==0 && $potongan!=0 && $min_trans==0 && $max_dis==0) {
        $bayar = $estimasi_pembayaran-$potongan;
        
        $this->Model_Pembayaran->input_transaksi($kode_trx, $estimasi_pembayaran, $tanggal, $waktu, $id_kasir, $jumlah_barang);
        $this->Model_Pembayaran->input_pembayaran($idBayar, $bayar, $tanggal, $waktu,  $kode_trx);
        $this->Model_Pembayaran->input_pembayaranTunai($idBayar, $jenis_diskon, $estimasi_pembayaran, $tunai);
      }

      elseif // all nominal, max diskon, persen (sudah)
      
      ($persen!=0 && $potongan==0 && $min_trans==0 && $max_dis!=0) {
        $diskon = ($estimasi_pembayaran*$persen)/100;

        if ($diskon > $max_dis) {
          $bayar = $estimasi_pembayaran-$max_dis;

          $this->Model_Pembayaran->input_transaksi($kode_trx, $estimasi_pembayaran, $tanggal, $waktu, $id_kasir, $jumlah_barang);
          $this->Model_Pembayaran->input_pembayaran($idBayar, $bayar, $tanggal, $waktu,  $kode_trx);
          $this->Model_Pembayaran->input_pembayaranTunai($idBayar, $jenis_diskon, $estimasi_pembayaran, $tunai);
        }

        else
        {
          $bayar = $estimasi_pembayaran-$diskon;

          $this->Model_Pembayaran->input_transaksi($kode_trx, $estimasi_pembayaran, $tanggal, $waktu, $id_kasir, $jumlah_barang);
          $this->Model_Pembayaran->input_pembayaran($idBayar, $bayar, $tanggal, $waktu,  $kode_trx);
          $this->Model_Pembayaran->input_pembayaranTunai($idBayar, $jenis_diskon, $estimasi_pembayaran, $tunai);
        }
      }


      elseif //min nominal persen (sudah)
      
      ($persen!=0 && $potongan==0 && $min_trans!=0 && $max_dis==0) {
        if ($estimasi_pembayaran >= $min_trans) {

          $bayar = $estimasi_pembayaran-($estimasi_pembayaran*$persen)/100;

          $this->Model_Pembayaran->input_transaksi($kode_trx, $estimasi_pembayaran, $tanggal, $waktu, $id_kasir, $jumlah_barang);
          $this->Model_Pembayaran->input_pembayaran($idBayar, $bayar, $tanggal, $waktu,  $kode_trx);
          $this->Model_Pembayaran->input_pembayaranTunai($idBayar, $jenis_diskon, $estimasi_pembayaran, $tunai);
        }
        else
        {
          $this->Model_Pembayaran->input_transaksi($kode_trx, $estimasi_pembayaran, $tanggal, $waktu, $id_kasir, $tunai, $jenis_diskon, $jumlah_barang);
          $this->Model_Pembayaran->input_pembayarann($idBayar, $estimasi_pembayaran, $tanggal, $waktu,  $kode_trx);
        }
      }

      elseif //min nominal potongan harga (sudah)
      
      ($persen==0 && $potongan!=0 && $min_trans!=0 && $max_dis==0) {
        if ($estimasi_pembayaran >= $min_trans) {

          $bayar = $estimasi_pembayaran-$potongan;

          $this->Model_Pembayaran->input_transaksi($kode_trx, $estimasi_pembayaran, $tanggal, $waktu, $id_kasir, $jumlah_barang);
          $this->Model_Pembayaran->input_pembayaran($idBayar, $bayar, $tanggal, $waktu,  $kode_trx);
          $this->Model_Pembayaran->input_pembayaranTunai($idBayar, $jenis_diskon, $estimasi_pembayaran, $tunai);
        }
        else
        {
          $this->Model_Pembayaran->input_transaksi($kode_trx, $estimasi_pembayaran, $tanggal, $waktu, $id_kasir, $tunai, $jenis_diskon, $jumlah_barang);
          $this->Model_Pembayaran->input_pembayarann($idBayar, $estimasi_pembayaran, $tanggal, $waktu,  $kode_trx);
        }
      }

      elseif // min nominal, max diskon, persen
      
      ($persen!=0 && $potongan==0 && $min_trans!=0 && $max_dis!=0) {
        $diskon = ($estimasi_pembayaran*$persen)/100;
        if ($estimasi_pembayaran >= $min_trans) {
          if ($diskon > $max_dis) {
            $bayar = $estimasi_pembayaran-$max_dis;
            $this->Model_Pembayaran->input_transaksi($kode_trx, $estimasi_pembayaran, $tanggal, $waktu, $id_kasir, $jumlah_barang);
            $this->Model_Pembayaran->input_pembayaran($idBayar, $bayar, $tanggal, $waktu,  $kode_trx);
            $this->Model_Pembayaran->input_pembayaranTunai($idBayar, $jenis_diskon, $estimasi_pembayaran, $tunai);
          }
          else{
            $bayar = $estimasi_pembayaran-$diskon;
            $this->Model_Pembayaran->input_transaksi($kode_trx, $estimasi_pembayaran, $tanggal, $waktu, $id_kasir, $jumlah_barang);
            $this->Model_Pembayaran->input_pembayaran($idBayar, $bayar, $tanggal, $waktu,  $kode_trx);
            $this->Model_Pembayaran->input_pembayaranTunai($idBayar, $jenis_diskon, $estimasi_pembayaran, $tunai);
          }
        }
      }
      redirect('kasir/pembayaran/StrukTunai');
    }

  }

}


public function inputWallet()
{
  $idBayar = $this->input->post('id_pembayaran');
  $kode_trx = $this->input->post('kode_transaksi');
  $estimasi_pembayaran = $this->input->post('total_tagihan');
  $tanggal = date("Y-m-d");
  $waktu = date("H:i:s");
  $tunai = $this->input->post('tunai');
  $nomor_transaksi = $this->input->post('nomor_transaksi');
  $nama_pembayaran = $this->input->post('nama_pembayaran');
  $id_kasir = $this->session->userdata("id_kas");
  $jumlah_barang = $this->input->post('jumlah_barang');
  $jenis_diskon = $this->input->post('diskon');

  $data = $this->Model_Pembayaran->get_diskon($jenis_diskon);
  $this->session->set_userdata('diskon', $data);

  $get = $this->session->userdata('diskon');
  foreach ($get as $cek) {
    $persen = $cek->Persentase;
    $potongan = $cek->Potongan_Harga;
    $min_trans = $cek->Min_Transaksi;
    $max_dis = $cek->Max_Diskon;

      if ($persen!=0 && $potongan==0 && $min_trans==0 && $max_dis==0) { //all nominal  pakai persen (sudah)
        $bayar = $estimasi_pembayaran-($estimasi_pembayaran*$persen)/100;

        $this->Model_Pembayaran->input_transaksi($kode_trx, $estimasi_pembayaran, $tanggal, $waktu, $id_kasir, $jumlah_barang);
        $this->Model_Pembayaran->input_pembayaran($idBayar, $bayar, $tanggal, $waktu,  $kode_trx);
        $this->Model_Pembayaran->input_pembayaranWallet($idBayar, $nomor_transaksi, $jenis_diskon, $estimasi_pembayaran, $nama_pembayaran);
      }


       elseif //tidak ada diskon

       ($persen==0 && $potongan==0 && $min_trans==0 && $max_dis==0) {
        $bayar = $estimasi_pembayaran;
        
        $this->Model_Pembayaran->input_transaksi($kode_trx, $estimasi_pembayaran, $tanggal, $waktu, $id_kasir, $jumlah_barang);
        $this->Model_Pembayaran->input_pembayaran($idBayar, $bayar, $tanggal, $waktu,  $kode_trx);
        $this->Model_Pembayaran->input_pembayaranWallet($idBayar, $nomor_transaksi, $jenis_diskon, $estimasi_pembayaran, $nama_pembayaran);
      }

      elseif //all nominal potongan harga (sudah)
      
      ($persen==0 && $potongan!=0 && $min_trans==0 && $max_dis==0) {
        $bayar = $estimasi_pembayaran-$potongan;
        
        $this->Model_Pembayaran->input_transaksi($kode_trx, $estimasi_pembayaran, $tanggal, $waktu, $id_kasir, $jumlah_barang);
        $this->Model_Pembayaran->input_pembayaran($idBayar, $bayar, $tanggal, $waktu,  $kode_trx);
        $this->Model_Pembayaran->input_pembayaranWallet($idBayar, $nomor_transaksi, $jenis_diskon, $estimasi_pembayaran, $nama_pembayaran);
      }

      elseif // all nominal, max diskon, persen (sudah)
      
      ($persen!=0 && $potongan==0 && $min_trans==0 && $max_dis!=0) {
        $diskon = ($estimasi_pembayaran*$persen)/100;

        if ($diskon > $max_dis) {
          $bayar = $estimasi_pembayaran-$max_dis;

          $this->Model_Pembayaran->input_transaksi($kode_trx, $estimasi_pembayaran, $tanggal, $waktu, $id_kasir, $jumlah_barang);
          $this->Model_Pembayaran->input_pembayaran($idBayar, $bayar, $tanggal, $waktu,  $kode_trx);
          $this->Model_Pembayaran->input_pembayaranWallet($idBayar, $nomor_transaksi, $jenis_diskon, $estimasi_pembayaran, $nama_pembayaran);
        }

        else
        {
          $bayar = $estimasi_pembayaran-$diskon;

          $this->Model_Pembayaran->input_transaksi($kode_trx, $estimasi_pembayaran, $tanggal, $waktu, $id_kasir, $jumlah_barang);
          $this->Model_Pembayaran->input_pembayaran($idBayar, $bayar, $tanggal, $waktu,  $kode_trx);
          $this->Model_Pembayaran->input_pembayaranWallet($idBayar, $nomor_transaksi, $jenis_diskon, $estimasi_pembayaran, $nama_pembayaran);
        }
      }

      elseif //min nominal persen (sudah)
      
      ($persen!=0 && $potongan==0 && $min_trans!=0 && $max_dis==0) {
        if ($estimasi_pembayaran >= $min_trans) {

          $bayar = $estimasi_pembayaran-($estimasi_pembayaran*$persen)/100;

          $this->Model_Pembayaran->input_transaksi($kode_trx, $estimasi_pembayaran, $tanggal, $waktu, $id_kasir, $jumlah_barang);
          $this->Model_Pembayaran->input_pembayaran($idBayar, $bayar, $tanggal, $waktu,  $kode_trx);
          $this->Model_Pembayaran->input_pembayaranWallet($idBayar, $nomor_transaksi, $jenis_diskon, $estimasi_pembayaran, $nama_pembayaran);
        }
        else
        {
          $this->Model_Pembayaran->input_transaksi($kode_trx, $estimasi_pembayaran, $tanggal, $waktu, $id_kasir, $tunai, $jenis_diskon, $jumlah_barang);
          $this->Model_Pembayaran->input_pembayarann($idBayar, $estimasi_pembayaran, $tanggal, $waktu,  $kode_trx);
        }
      }

      elseif //min nominal potongan harga (sudah)
      
      ($persen==0 && $potongan!=0 && $min_trans!=0 && $max_dis==0) {
        if ($estimasi_pembayaran >= $min_trans) {

          $bayar = $estimasi_pembayaran-$potongan;

          $this->Model_Pembayaran->input_transaksi($kode_trx, $estimasi_pembayaran, $tanggal, $waktu, $id_kasir, $jumlah_barang);
          $this->Model_Pembayaran->input_pembayaran($idBayar, $bayar, $tanggal, $waktu,  $kode_trx);
          $this->Model_Pembayaran->input_pembayaranWallet($idBayar, $nomor_transaksi, $jenis_diskon, $estimasi_pembayaran, $nama_pembayaran);
        }
        else
        {
          $this->Model_Pembayaran->input_transaksi($kode_trx, $estimasi_pembayaran, $tanggal, $waktu, $id_kasir, $tunai, $jenis_diskon, $jumlah_barang);
          $this->Model_Pembayaran->input_pembayarann($idBayar, $estimasi_pembayaran, $tanggal, $waktu,  $kode_trx);
        }
      }

      elseif // min nominal, max diskon, persen
      
      ($persen!=0 && $potongan==0 && $min_trans!=0 && $max_dis!=0) {
        $diskon = ($estimasi_pembayaran*$persen)/100;
        if ($estimasi_pembayaran >= $min_trans) {
          if ($diskon > $max_dis) {
            $bayar = $estimasi_pembayaran-$max_dis;
            $this->Model_Pembayaran->input_transaksi($kode_trx, $estimasi_pembayaran, $tanggal, $waktu, $id_kasir, $jumlah_barang);
            $this->Model_Pembayaran->input_pembayaran($idBayar, $bayar, $tanggal, $waktu,  $kode_trx);
            $this->Model_Pembayaran->input_pembayaranWallet($idBayar, $nomor_transaksi, $jenis_diskon, $estimasi_pembayaran, $nama_pembayaran);
          }
          else{
            $bayar = $estimasi_pembayaran-$diskon;
            $this->Model_Pembayaran->input_transaksi($kode_trx, $estimasi_pembayaran, $tanggal, $waktu, $id_kasir, $jumlah_barang);
            $this->Model_Pembayaran->input_pembayaran($idBayar, $bayar, $tanggal, $waktu,  $kode_trx);
            $this->Model_Pembayaran->input_pembayaranWallet($idBayar, $nomor_transaksi, $jenis_diskon, $estimasi_pembayaran, $nama_pembayaran);
          }
        }
      }
      redirect('kasir/pembayaran/StrukWallet');

    }

  }


  public function inputDebit()
  {
    $idBayar = $this->input->post('id_pembayaran');
    $kode_trx = $this->input->post('kode_transaksi');
    $estimasi_pembayaran = $this->input->post('total_tagihan');
    $tanggal = date("Y-m-d");
    $waktu = date("H:i:s");
    $tunai = $this->input->post('tunai');
    $nomor_kartu = $this->input->post('nomor_kartu');
    $nama_bank = $this->input->post('nama_bank');
    $id_kasir = $this->session->userdata("id_kas");
    $jumlah_barang = $this->input->post('jumlah_barang');
    $jenis_diskon = $this->input->post('diskon');

    $data = $this->Model_Pembayaran->get_diskon($jenis_diskon);
    $this->session->set_userdata('diskon', $data);

    $get = $this->session->userdata('diskon');
    foreach ($get as $cek) {
      $persen = $cek->Persentase;
      $potongan = $cek->Potongan_Harga;
      $min_trans = $cek->Min_Transaksi;
      $max_dis = $cek->Max_Diskon;

      if ($persen!=0 && $potongan==0 && $min_trans==0 && $max_dis==0) { //all nominal  pakai persen (sudah)
        $bayar = $estimasi_pembayaran-($estimasi_pembayaran*$persen)/100;

        $this->Model_Pembayaran->input_transaksi($kode_trx, $estimasi_pembayaran, $tanggal, $waktu, $id_kasir, $jumlah_barang);
        $this->Model_Pembayaran->input_pembayaran($idBayar, $bayar, $tanggal, $waktu,  $kode_trx);
        $this->Model_Pembayaran->input_pembayaranDebit($idBayar, $nomor_kartu, $jenis_diskon, $estimasi_pembayaran, $nama_bank);
      }

      
       elseif //tidak ada diskon

       ($persen==0 && $potongan==0 && $min_trans==0 && $max_dis==0) {
        $bayar = $estimasi_pembayaran;
        
        $this->Model_Pembayaran->input_transaksi($kode_trx, $estimasi_pembayaran, $tanggal, $waktu, $id_kasir, $jumlah_barang);
        $this->Model_Pembayaran->input_pembayaran($idBayar, $bayar, $tanggal, $waktu,  $kode_trx);
        $this->Model_Pembayaran->input_pembayaranDebit($idBayar, $nomor_kartu, $jenis_diskon, $estimasi_pembayaran, $nama_bank);
      }

      elseif //all nominal potongan harga (sudah)
      
      ($persen==0 && $potongan!=0 && $min_trans==0 && $max_dis==0) {
        $bayar = $estimasi_pembayaran-$potongan;
        
        $this->Model_Pembayaran->input_transaksi($kode_trx, $estimasi_pembayaran, $tanggal, $waktu, $id_kasir, $jumlah_barang);
        $this->Model_Pembayaran->input_pembayaran($idBayar, $bayar, $tanggal, $waktu,  $kode_trx);
        $this->Model_Pembayaran->input_pembayaranDebit($idBayar, $nomor_kartu, $jenis_diskon, $estimasi_pembayaran, $nama_bank);
      }

      elseif // all nominal, max diskon, persen (sudah)
      
      ($persen!=0 && $potongan==0 && $min_trans==0 && $max_dis!=0) {
        $diskon = ($estimasi_pembayaran*$persen)/100;

        if ($diskon > $max_dis) {
          $bayar = $estimasi_pembayaran-$max_dis;

          $this->Model_Pembayaran->input_transaksi($kode_trx, $estimasi_pembayaran, $tanggal, $waktu, $id_kasir, $jumlah_barang);
          $this->Model_Pembayaran->input_pembayaran($idBayar, $bayar, $tanggal, $waktu,  $kode_trx);
          $this->Model_Pembayaran->input_pembayaranDebit($idBayar, $nomor_kartu, $jenis_diskon, $estimasi_pembayaran, $nama_bank);
        }

        else
        {
          $bayar = $estimasi_pembayaran-$diskon;

          $this->Model_Pembayaran->input_transaksi($kode_trx, $estimasi_pembayaran, $tanggal, $waktu, $id_kasir, $jumlah_barang);
          $this->Model_Pembayaran->input_pembayaran($idBayar, $bayar, $tanggal, $waktu,  $kode_trx);
          $this->Model_Pembayaran->input_pembayaranDebit($idBayar, $nomor_kartu, $jenis_diskon, $estimasi_pembayaran, $nama_bank);
        }
      }

      elseif //min nominal persen (sudah)
      
      ($persen!=0 && $potongan==0 && $min_trans!=0 && $max_dis==0) {
        if ($estimasi_pembayaran >= $min_trans) {

          $bayar = $estimasi_pembayaran-($estimasi_pembayaran*$persen)/100;

          $this->Model_Pembayaran->input_transaksi($kode_trx, $estimasi_pembayaran, $tanggal, $waktu, $id_kasir, $jumlah_barang);
          $this->Model_Pembayaran->input_pembayaran($idBayar, $bayar, $tanggal, $waktu,  $kode_trx);
          $this->Model_Pembayaran->input_pembayaranDebit($idBayar, $nomor_kartu, $jenis_diskon, $estimasi_pembayaran, $nama_bank);
        }
        else
        {
          $this->Model_Pembayaran->input_transaksi($kode_trx, $estimasi_pembayaran, $tanggal, $waktu, $id_kasir, $tunai, $jenis_diskon, $jumlah_barang);
          $this->Model_Pembayaran->input_pembayarann($idBayar, $estimasi_pembayaran, $tanggal, $waktu,  $kode_trx);
        }
      }

      elseif //min nominal potongan harga (sudah)
      
      ($persen==0 && $potongan!=0 && $min_trans!=0 && $max_dis==0) {
        if ($estimasi_pembayaran >= $min_trans) {

          $bayar = $estimasi_pembayaran-$potongan;

          $this->Model_Pembayaran->input_transaksi($kode_trx, $estimasi_pembayaran, $tanggal, $waktu, $id_kasir, $jumlah_barang);
          $this->Model_Pembayaran->input_pembayaran($idBayar, $bayar, $tanggal, $waktu,  $kode_trx);
          $this->Model_Pembayaran->input_pembayaranDebit($idBayar, $nomor_kartu, $jenis_diskon, $estimasi_pembayaran, $nama_bank);
        }
        else
        {
          $this->Model_Pembayaran->input_transaksi($kode_trx, $estimasi_pembayaran, $tanggal, $waktu, $id_kasir, $tunai, $jenis_diskon, $jumlah_barang);
          $this->Model_Pembayaran->input_pembayarann($idBayar, $estimasi_pembayaran, $tanggal, $waktu,  $kode_trx);
        }
      }

      elseif // min nominal, max diskon, persen
      
      ($persen!=0 && $potongan==0 && $min_trans!=0 && $max_dis!=0) {
        $diskon = ($estimasi_pembayaran*$persen)/100;
        if ($estimasi_pembayaran >= $min_trans) {
          if ($diskon > $max_dis) {
            $bayar = $estimasi_pembayaran-$max_dis;
            $this->Model_Pembayaran->input_transaksi($kode_trx, $estimasi_pembayaran, $tanggal, $waktu, $id_kasir, $jumlah_barang);
            $this->Model_Pembayaran->input_pembayaran($idBayar, $bayar, $tanggal, $waktu,  $kode_trx);
            $this->Model_Pembayaran->input_pembayaranDebit($idBayar, $nomor_kartu, $jenis_diskon, $estimasi_pembayaran, $nama_bank);
          }
          else{
            $bayar = $estimasi_pembayaran-$diskon;
            $this->Model_Pembayaran->input_transaksi($kode_trx, $estimasi_pembayaran, $tanggal, $waktu, $id_kasir, $jumlah_barang);
            $this->Model_Pembayaran->input_pembayaran($idBayar, $bayar, $tanggal, $waktu,  $kode_trx);
            $this->Model_Pembayaran->input_pembayaranDebit($idBayar, $nomor_kartu, $jenis_diskon, $estimasi_pembayaran, $nama_bank);
          }
        }
      }
      redirect('kasir/pembayaran/StrukDebit');

    }

  }


  public function StrukTunai()
  {
    $data = $this->Model_Pembayaran->getStrukTunai();
    $this->session->set_userdata('TampilStruk', $data);
    $this->load->view('kasir/pembayaran/struk/pembayaran_tunai/ListStruk');
  }

  public function StrukWallet()
  {
    $data = $this->Model_Pembayaran->getStrukWallet();
    $this->session->set_userdata('TampilStruk', $data);
    $this->load->view('kasir/pembayaran/struk/pembayaran_wallet/ListStruk');
  }

  public function StrukDebit()
  {
    $data = $this->Model_Pembayaran->getStrukDebit();
    $this->session->set_userdata('TampilStruk', $data);
    $this->load->view('kasir/pembayaran/struk/pembayaran_debit/ListStruk');
  }

  public function DetailStrukTunai()
  {
    $data = $this->Model_Pembayaran->detailStrukTunai($_GET['Kode_transaksi']);
    $this->session->set_userdata('detail_struk', $data);
    $this->load->view('kasir/pembayaran/struk/pembayaran_tunai/DetailStruk');
  }

  public function DetailStrukWallet()
  {

    $data = $this->Model_Pembayaran->detailStrukWallet($_GET['Kode_transaksi']);
    $this->session->set_userdata('detail_struk', $data);
    $this->load->view('kasir/pembayaran/struk/pembayaran_wallet/DetailStruk');
  }

  public function DetailStrukDebit()
  {

    $data = $this->Model_Pembayaran->detailStrukDebit($_GET['Kode_transaksi']);
    $this->session->set_userdata('detail_struk', $data);
    $this->load->view('kasir/pembayaran/struk/pembayaran_debit/DetailStruk');
  }

  public function PrintStrukTunai()
  {
    $id = $_GET['Kode'];
    $row = $this->Model_Pembayaran->detailStrukTunaiPrint($_GET['Kode']);
    if ($row) {
      $data = array(
        'Kode_transaksi' => $row->Kode_transaksi,
        'Tanggal_transaksi' => $row->Tanggal_transaksi,
        'Waktu_transaksi' => $row->Waktu_transaksi,
        'Nama_kasir' => $row->Nama_kasir,
        'Harga_total' => $row->Harga_Total,
        'diskon' => $row->diskon,
        'Total_bayar' => $row->Total_bayar,
        'tunai' => $row->tunai,
        'data_produk' => $this->db->query("SELECT a.Kode_transaksi, d.Nama_barang, SUM(c.Jumlah) as Jumlah, d.Harga_barang, SUM(c.Harga_total) as Harga_total, a.Estimasi_pembayaran as Estimasi_pembayaran, (a.Estimasi_pembayaran-e.Total_bayar) as diskon, e.Total_bayar, f.tunai, (f.tunai-e.Total_bayar) as kembali FROM transaksi a JOIN kasir b ON a.ID_Kasir=b.ID_kasir JOIN detail_transaksi c ON a.Kode_transaksi=c.Kode_transaksi JOIN barang d ON c.Barcode=d.Barcode JOIN pembayaran e ON a.Kode_transaksi=e.Kode_transaksi JOIN pembayaran_tunai f ON e.ID_pembayaran=f.ID_pembayaran WHERE a.Kode_transaksi='$id' GROUP BY c.Barcode")->result(),

      );
      $this->load->view('kasir/pembayaran/struk/pembayaran_tunai/PrintStruk', $data);
    } else {
      $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
      redirect(site_url('detailStrukTunai'));
    }
  }

  public function PrintStrukWallet()
  {
    $id = $_GET['Kode'];
    $row = $this->Model_Pembayaran->detailStrukWalletPrint($_GET['Kode']);
    if ($row) {
      $data = array(
        'Kode_transaksi' => $row->Kode_transaksi,
        'Tanggal_transaksi' => $row->Tanggal_transaksi,
        'Waktu_transaksi' => $row->Waktu_transaksi,
        'Nama_kasir' => $row->Nama_kasir,
        'Harga_total' => $row->Harga_Total,
        'diskon' => $row->diskon,
        'Total_bayar' => $row->Total_bayar,
        'data_produk' => $this->db->query("SELECT a.Kode_transaksi, d.Nama_barang, SUM(c.Jumlah) as Jumlah, d.Harga_barang, c.Harga_total, a.Estimasi_pembayaran as Estimasi_pembayaran, (a.Estimasi_pembayaran-e.Total_bayar) as diskon, e.Total_bayar FROM transaksi a JOIN kasir b ON a.ID_Kasir=b.ID_kasir JOIN detail_transaksi c ON a.Kode_transaksi=c.Kode_transaksi JOIN barang d ON c.Barcode=d.Barcode JOIN pembayaran e ON a.Kode_transaksi=e.Kode_transaksi JOIN pembayaran_wallet f ON e.ID_pembayaran=f.ID_pembayaran WHERE a.Kode_transaksi='$id' GROUP BY c.Barcode")->result(),

      );
      $this->load->view('kasir/pembayaran/struk/pembayaran_wallet/PrintStruk', $data);
    } else {
      $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
      redirect(site_url('detailStrukWallet '));
    }

      // $data['struk'] = $this->Model_Pembayaran->detailStrukWallet($_GET['Kode']);
      // $this->load->view('kasir/pembayaran/struk/pembayaran_wallet/PrintStruk', $data);
  }

  public function PrintStrukDebit()
  {
    $id = $_GET['Kode'];
    $row = $this->Model_Pembayaran->detailStrukDebitPrint($_GET['Kode']);
    if ($row) {
      $data = array(
        'Kode_transaksi' => $row->Kode_transaksi,
        'Tanggal_transaksi' => $row->Tanggal_transaksi,
        'Waktu_transaksi' => $row->Waktu_transaksi,
        'Nama_kasir' => $row->Nama_kasir,
        'Harga_total' => $row->Harga_Total,
        'diskon' => $row->diskon,
        'Total_bayar' => $row->Total_bayar,
        'data_produk' => $this->db->query("SELECT a.Kode_transaksi, d.Nama_barang, SUM(c.Jumlah) as Jumlah, d.Harga_barang, c.Harga_total, a.Estimasi_pembayaran as Estimasi_pembayaran, (a.Estimasi_pembayaran-e.Total_bayar) as diskon, e.Total_bayar FROM transaksi a JOIN kasir b ON a.ID_Kasir=b.ID_kasir JOIN detail_transaksi c ON a.Kode_transaksi=c.Kode_transaksi JOIN barang d ON c.Barcode=d.Barcode JOIN pembayaran e ON a.Kode_transaksi=e.Kode_transaksi JOIN pembayaran_debit f ON e.ID_pembayaran=f.ID_pembayaran WHERE a.Kode_transaksi='$id' GROUP BY c.Barcode")->result(),

      );
      $this->load->view('kasir/pembayaran/struk/pembayaran_debit/PrintStruk', $data);
    } else {
      $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
      redirect(site_url('detailStrukDebit'));
    }

      // $data['struk'] = $this->Model_Pembayaran->detailStrukDebit($_GET['Kode']);
      // $this->load->view('kasir/pembayaran/struk/pembayaran_debit/PrintStruk', $data);
  }


  public function metodePembayaran()
  {
    $this->load->view('kasir/pembayaran/invoice/MetodePembayaran');
  }

  public function diskon()
  {
    $data = $this->Model_Pembayaran->getAllDiskon();
    $this->session->set_userdata('diskon', $data);
    $this->load->view('kasir/pembayaran/diskon/diskon');
  }

  public function inputDiskon()
  {
    $this->load->view('kasir/pembayaran/diskon/inputDiskon');
  }

  public function prosesInputDiskon()
  {
    $kode = $this->input->post('kodeDiskon');
    $keterangan = $this->input->post('keterangan');
    $total = $this->input->post('total');
    $jumlah = $this->input->post('jumlah');
    $jenis = $this->input->post('jenis');
    $id_kasir = $this->session->userdata("id_kas");

    $this->Model_Pembayaran->inputDiskon($kode, $keterangan, $total, $jumlah, $jenis, $id_kasir);
    $this->session->set_flashdata('message', '
      <div class="alert alert-block alert-success"></i><a  class="close" data-dismiss="alert" aria-label="close">&times;</a></button>
      <i class="ace-icon fa fa-bullhorn green"></i> Diskon berhasil ditambahkan
      </div>');
    redirect('kasir/pembayaran/diskon');

  }


  public function editDiskon()
  {
    $data['dataDiskon'] = $this->Model_Pembayaran->getDiskon($_GET['KodeDiskon']);
    $this->load->view('kasir/pembayaran/diskon/editDiskon', $data);
  }

  public function prosesEditDiskon()
  {
    $kode = $this->input->post('kode');
    $keterangan = $this->input->post('ket');
    $total = $this->input->post('total');
    $jumlah = $this->input->post('minimal');
    $jenis = $this->input->post('jenis');
    $id_kasir = $this->session->userdata("id_kas");

    $this->Model_Pembayaran->updateDiskon($kode, $keterangan, $total, $jumlah, $jenis, $id_kasir);

    redirect('kasir/pembayaran/diskon');
  }

  public function hapusDiskon()
  {

    $kode = $_GET['KodeDiskon'];

    $this->Model_Pembayaran->hapusDiskon($kode);
    $this->session->set_flashdata('message', '
      <div class="alert alert-block alert-warning"></i><a  class="close" data-dismiss="alert" aria-label="close">&times;</a></button>
      <i class="ace-icon fa fa-bullhorn green"></i> Diskon berhasil ditambahkan
      </div>');
    redirect('kasir/pembayaran/diskon');
  }

  public function tolak()
  {
    $this->load->view('kasir/tolak');
  }


}
?>