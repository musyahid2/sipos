<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TutupKasir extends CI_Controller {
	public function __construct()
	  {
	    parent::__construct();
	    $this->load->model('Model_LoginKasir');
	    $this->load->model('Model_OprKasir');
	    $this->load->model('Model_transaksi');
	    $this->load->model('Model_ID');
	    if(is_null($this->session->userdata('id_kas'))){
      	redirect('kasir/auth/login');
    	}
  }


	public function index()
	{
		$totalTransaksitunai = $this->Model_transaksi->getTotaltransaksiTunai();
		$totalTransaksiWallet = $this->Model_transaksi->getTotaltransaksiWallet();
		$totalTransaksiDebit = $this->Model_transaksi->getTotaltransaksiDebit();
		$this->session->set_userdata('totalTransaksitunai', $totalTransaksitunai);
		$this->session->set_userdata('totalTransaksiWallet', $totalTransaksiWallet);
		$this->session->set_userdata('totalTransaksiDebit', $totalTransaksiDebit);
		$this->load->view('kasir/Input_TutupKasir');
	}

  public function proses_inputTutupKasir()
  {
    $Sal_akhir = $this->input->post('Saldo_akhir');
    $Sal_akh = preg_replace('/[^A-Za-z0-9]/', '', $Sal_akhir);
    $Saldo_akhir = (int) $Sal_akh;
    $ID_kasir = $this->session->userdata("id_kas");
    $this->Model_OprKasir->update_OprKasir($Saldo_akhir, $ID_kasir);
    print "<script type=\"text/javascript\">alert('Tutup Kasir Berhasil');
	window.location = '../../../SIPos/logout/kasir';</script>";
  }

}
