<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class auth extends CI_Controller {
	public function __construct(){
	  parent::__construct();
	  $this->load->model('Model_LoginKasir');
		
		if(!is_null($this->session->userdata('id_kas'))){
  		redirect('kasir/home');
  	}
  	if(!is_null($this->session->userdata('id_pen'))) {
  		redirect('pengelola/home');
		}
	
	}




	public function login()
	{
		$this->load->view('kasir/login');
	}

	public function proses_login()
	{
		$id_kasir = $this->input->post('id_kasir');
		$pin_kasir = $this->input->post('pin');

		$pin = md5($pin_kasir);
		$cekLogin = $this->Model_LoginKasir->cekLogin($id_kasir, $pin);
		$hasil = count($cekLogin);

		if ($hasil > 0) {
			
			$sedangLogin = $this->db->get_where('kasir', array('ID_kasir'=>$id_kasir, 'PIN'=> $pin))->row();


			$nama = $this->Model_LoginKasir->getKasir($sedangLogin->ID_kasir)[0]->Nama_kasir;
			$id_kas = $this->Model_LoginKasir->getKasir($sedangLogin->ID_kasir)[0]->ID_kasir;
			$foto = $this->Model_LoginKasir->getKasir($sedangLogin->ID_kasir)[0]->Foto;
			
			$stat = $this->Model_LoginKasir->getKasir($sedangLogin->ID_kasir)[0]->Status;

			if($stat == "Aktif"){
				$dataSession = array(
					'nama' => $nama,
					'id_kas' => $id_kas,
					'foto' => $foto,
					'Status' => 'Login'
				);

				$this->session->set_userdata($dataSession);
				redirect(base_url("kasir/home"));
			}else{
				$this->session->set_flashdata('message', '
					<div class="alert alert-block alert-danger"></i></button>
					<i class="ace-icon fa fa-bullhorn green"></i> Akun sedang tidak aktif
					</div>');
				redirect('kasir/auth/login');
			}


			
		}
		else {
			$this->session->set_flashdata('message', '
			<div class="alert alert-block alert-danger"></i></button>
			<i class="ace-icon fa fa-bullhorn green"></i> ID atau Password anda salah
			</div>');
			redirect(base_url("kasir/auth/login"));
		}
		
	}


}
