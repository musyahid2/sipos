<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class home extends CI_Controller {
	public function __construct()
	  {
	    parent::__construct();
	    $this->load->model('Model_LoginKasir');
	    // if($this->session->userdata('Status')!="Login") {
	    // 	redirect(base_url("kasir/auth/login"));
	    // }
	    if(is_null($this->session->userdata('id_kas'))){
      	redirect('kasir/auth/login');
    	}
  }


	public function index()
	{
		$this->load->view('kasir/home');
	}
}
