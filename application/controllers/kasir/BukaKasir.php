<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BukaKasir extends CI_Controller {
	public function __construct()
	  {
	    parent::__construct();
	    $this->load->model('Model_LoginKasir');
	    $this->load->model('Model_OprKasir');
	    $this->load->model('Model_ID');
	    if(is_null($this->session->userdata('id_kas'))){
      	redirect('kasir/auth/login');
    	}
    }

	public function index()
	{
		$this->load->view('kasir/Input_BukaKasir');
	}

  public function proses_inputBukaKasir()
  {
    $Sal_awal = $this->input->post('Saldo_awal');
    $Sal_awl = preg_replace('/[^A-Za-z0-9]/', '', $Sal_awal);
    $Saldo_awal = (int) $Sal_awl;
    $id_kasir = $this->session->userdata("id_kas");
    $this->Model_OprKasir->input_OprKasir($Saldo_awal, $id_kasir);
    $this->session->set_flashdata('message', '
			<div class="alert alert-block alert-success"></i><a  class="close" data-dismiss="alert" aria-label="close">&times;</a></button>
			<i class="ace-icon fa fa-bullhorn green"></i> Saldo berhasil ditambahkan
			</div>');
    redirect('kasir/home');
  }

}
