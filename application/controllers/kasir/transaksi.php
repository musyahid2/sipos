<?php 

/**
 * 
 */
class transaksi extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Model_Transaksi');
		if(is_null($this->session->userdata('id_kas'))){
      	redirect('kasir/auth/login');
    	}
	}


	public function index()
	{
		$data = $this->Model_Transaksi->getInvoice();
    	$this->session->set_userdata('Invoice', $data);
		$this->load->view('pengelola/transaksi/Invoice');
	}

	public function DetailInvoice()
	  {
	    
	    $data['invoice'] = $this->Model_Transaksi->detailInvoice($_GET['Kode']);
	    $this->load->view('pengelola/transaksi/DetailInvoice', $data);
	  }

}
 ?>