<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class stok extends CI_Controller {
	public function __construct()
	  {
	    parent::__construct();
	    $this->load->model('Model_Produk');
			$this->load->model('Model_Stok');
			if(is_null($this->session->userdata('id_pen'))) {
	    	redirect(base_url("pengelola/auth/login"));
	    }
  }

	public function index()
	{
		$data = $this->Model_Stok->getAllStok();
		$this->session->set_userdata('all_data', $data);
		$this->load->view('pengelola/stok/kartu_stok');
	}

	public function input_stok()
	{
		$data = $this->Model_Produk->getAllProdukNotInStok();
	  	$this->session->set_userdata('all_data', $data);
		$this->load->view('pengelola/stok/input_stok');
	}

	public function proses_input_stok()
	{
		$Jumlah = $this->input->post('Jumlah');
		$Barcode = $this->input->post('barcode');
		$id_pengelola = $this->session->userdata("id_pen");
		$this->Model_Stok->input_stok($Jumlah, $Barcode, $id_pengelola);
		$this->session->set_flashdata('message', '
			<div class="alert alert-block alert-success"></i><a  class="close" data-dismiss="alert" aria-label="close">&times;</a></button>
			<i class="ace-icon fa fa-bullhorn green"></i> Data berhasil ditambahkan
			</div>');
		redirect('pengelola/stok');
	}

	public function hapus_stok()
	 {
	 	 $ID_stok = $_GET['ID_stok'];
		 $this->Model_Stok->hapus_stok($ID_stok);
		 $this->session->set_flashdata('message', '
			<div class="alert alert-block alert-warning"></i><a  class="close" data-dismiss="alert" aria-label="close">&times;</a></button>
			<i class="ace-icon fa fa-bullhorn green"></i> Data berhasil dihapus
			</div>');
		 redirect('pengelola/stok');
	 }

	public function update_stok_masuk()
	{
		$data = $this->Model_Stok->getAllProduk();
		$this->session->set_userdata('all_data', $data);
		$query['dataStok'] = $this->Model_Stok->getStok($_GET['ID_stok']);
		$this->load->view('pengelola/stok/Input_stokMasuk',$query);
	}

	public function proses_updateStok()
	{
		$ID_stok = $this->input->post('ID_stok');
		$Jumlah_tersedia = $this->input->post('Jumlah');
		$Jumlah_Masuk = $this->input->post('Jumlah_Masuk');
		$Harga_beli = $this->input->post('Harga_beli');
		$id_pengelola = $this->session->userdata("id_pen");
		$stok_baru = $Jumlah_Masuk + $Jumlah_tersedia;

		$this->Model_Stok->input_stok_masuk( $Jumlah_Masuk, $Harga_beli, $ID_stok, $id_pengelola);
		$this->Model_Stok->update_stok($stok_baru, $ID_stok);
		$this->session->set_flashdata('message', '
			<div class="alert alert-block alert-success"></i><a  class="close" data-dismiss="alert" aria-label="close">&times;</a></button>
			<i class="ace-icon fa fa-bullhorn green"></i> Stok berhasil ditambahkan
			</div>');
		redirect('pengelola/stok');
	}

	public function hapus_produk()
	 {
	 	$Barcode = $_GET['barcode'];
		$this->Model_Produk->hapus_produk($Barcode);
		redirect('pengelola/produk');

	 }

	public function kartu_stok()
	{
		$data = $this->Model_Stok->getAllStok();
		$this->session->set_userdata('all_data', $data);
		$this->load->view('pengelola/stok/kartu_stok');
	}

	public function data_stokmasuk()
	{
		$data = $this->Model_Stok->getAllStokMasuk();
		$this->session->set_userdata('all_data', $data);
		$this->load->view('pengelola/stok/data_stokmasuk');
	}

	public function data_stokkeluar()
	{
		$data = $this->Model_Stok->getAllStokKeluar();
		$this->session->set_userdata('all_data', $data);
		$this->load->view('pengelola/stok/data_stokkeluar');
	}

	public function update_stok_keluar()
	{
		$data = $this->Model_Stok->getAllProduk();
		$this->session->set_userdata('all_data', $data);
		$query['dataStok'] = $this->Model_Stok->getStok($_GET['ID_stok']);
		$this->load->view('pengelola/stok/Input_stokKeluar',$query);
	}

	public function proses_updateStokkeluar()
	{
		$ID_stok = $this->input->post('ID_stok');
		$Jumlah_tersedia = $this->input->post('Jumlah');
		$Jumlah_Keluar = $this->input->post('Jumlah_Keluar');
		$Keterangan = $this->input->post('Keterangan');
		$id_pengelola = $this->session->userdata("id_pen");
		$stok_baru = $Jumlah_tersedia - $Jumlah_Keluar;

		$this->Model_Stok->input_stok_keluar( $Jumlah_Keluar, $Keterangan, $ID_stok, $id_pengelola);
		$this->Model_Stok->update_stok($stok_baru, $ID_stok);
		redirect('pengelola/stok');
	}

	public function data_stokOpname()
	{
		$data = $this->Model_Stok->getAllStokOpname();
		$this->session->set_userdata('all_data', $data);
		$this->load->view('pengelola/stok/data_stokOpname');
	}
	public function input_stokOpname()
	{
		$this->load->view('pengelola/stok/Input_stokOpname');
	}

	public function detail_stokOpname()
	{
		// $detail['opname'] = $this->Model_Stok->getDetailOpname($_GET['ID']);
		$data['opname'] = $this->Model_Stok->getDetailStokOpname($_GET['ID']);
		// $this->session->set_userdata('all_data', $data);
		$this->load->view('pengelola/stok/Detail_stokOpname', $data);
	}

	public function Print()
	{
		// $detail['print'] = $this->Model_Stok->getDetailOpname($_GET['ID']);
		$data['opname'] = $this->Model_Stok->getDetailStokOpname($_GET['ID']);
		// $this->session->set_userdata('all_data', $data);
		$this->load->view('pengelola/stok/PrintStok', $data);
	}

	// public function proses_inputdata_stokOpname()
	// {
	// 	$Keterangan = $this->input->post('Keterangan');
	// 	$id_pengelola = $this->session->userdata("id_pen");
	// 	$this->Model_Stok->input_stokopname($Keterangan, $id_pengelola);
	// 	$batas = $this->input->post('batas');
	// 	// print_r($_POST);
	// 	// echo count($_POST);
	// 	// for ($i=1; $i < $batas; $i++) {
	// 	// 	$ID_stok = $this->input->post('ID_stok'.$i);
	// 	// 	$Stok_tersedia = $this->input->post('Jumlah_tersedia'.$i);
	// 	// 	$last_key = $this->Model_Stok->last_record();
	// 	// 	$ID_stokopname= $this->Model_Stok->get_obj_index($last_key, 'ID_stokopname');
	// 	// 	$Stok_Aktual = $this->input->post('Jumlah_Aktual'.$i);
	// 	// 	$Harga_lama = $this->input->post('Harga_lama'.$i);
	// 	// 	$Harga_baru = $this->input->post('Harga_Baru'.$i);
	// 	// 	if ($Stok_Aktual!='') {
	// 	// 		if ($Stok_Aktual > $Stok_tersedia) {
	// 	// 			$Selisih = $Stok_Aktual - $Stok_tersedia;
	// 	// 		}else {
	// 	// 			$Selisih = $Stok_tersedia - $Stok_Aktual;
	// 	// 		}
	// 	// 	}else {
	// 	// 		$Stok_Aktual=$Stok_tersedia;
	// 	// 		if ($Stok_Aktual > $Stok_tersedia) {
	// 	// 			$Selisih = $Stok_Aktual - $Stok_tersedia;
	// 	// 		}else {
	// 	// 			$Selisih = $Stok_tersedia - $Stok_Aktual;
	// 	// 		}
	// 	// 	}
	// 	// 	if ($Harga_baru=='') {
	// 	// 		$Harga_baru = $Harga_lama;
	// 	// 	}else {
	// 	// 		$Harga_baru = $Harga_baru;
	// 	// 	}
	// 	// 	// echo "stok aktul = "."$i"."$Stok_Aktual"."<br>";
	// 	// 	// echo "ID stok = "."$i"."$ID_stok"."<br>";	
	// 	// 	$this->output->enable_profiler(TRUE);
	// 	// 	$this->Model_Stok->update_stok($Stok_Aktual, $ID_stok);
	// 	// 	$this->Model_Produk->update_harga($Harga_baru, $ID_stok);
	// 	// 	$this->Model_Stok->input_stokopname_relasi($ID_stok, $ID_stokopname, $Stok_tersedia, $Stok_Aktual, $Selisih, $Harga_lama, $Harga_baru);
			
	// 	// }
	// 	// redirect('pengelola/stok/data_stokOpname');
	// }

	public function proses_inputdata_stokOpname()
		{
			$Keterangan = $this->input->post('Keterangan');
			$id_pengelola = $this->session->userdata("id_pen");
			$this->Model_Stok->input_stokopname($Keterangan, $id_pengelola);
			$batas = $this->input->post('batas');
			if (count($_POST)==7) {
				$ID_stok = $this->input->post('ID_stok');
				$Stok_tersedia = $this->input->post('Jumlah_tersedia');
				$last_key = $this->Model_Stok->last_record();
				$ID_stokopname= $this->Model_Stok->get_obj_index($last_key, 'ID_stokopname');
				$Stok_Aktual = $this->input->post('Jumlah_Aktual');
				$Harga_lama = $this->input->post('Harga_lama');
				$Harga_baru = $this->input->post('Harga_Baru');
				if ($Stok_Aktual!='') {
					if ($Stok_Aktual > $Stok_tersedia) {
						$Selisih = $Stok_Aktual - $Stok_tersedia;
					}else {
						$Selisih = $Stok_tersedia - $Stok_Aktual;
					}
				}else {
					$Stok_Aktual=$Stok_tersedia;
					if ($Stok_Aktual > $Stok_tersedia) {
						$Selisih = $Stok_Aktual - $Stok_tersedia;
					}else {
						$Selisih = $Stok_tersedia - $Stok_Aktual;
					}
				}
				if ($Harga_baru=='') {
					$Harga_baru = $Harga_lama;
				}else {
					$Harga_baru = $Harga_baru;
				}
				$this->output->enable_profiler(TRUE);
				$this->Model_Stok->update_stok($Stok_Aktual, $ID_stok);
				$this->Model_Produk->update_harga($Harga_baru, $ID_stok);
				$this->Model_Stok->input_stokopname_relasi($ID_stok, $ID_stokopname, $Stok_tersedia, $Stok_Aktual, $Selisih, $Harga_lama, $Harga_baru);
			}else{
				$row = (count($_POST)-2)/5;
				// echo "$row";
				for ($i=1; $i <= $row; $i++) {
				$ID_stok = $this->input->post('ID_stok'.$i);
				$Stok_tersedia = $this->input->post('Jumlah_tersedia'.$i);
				$last_key = $this->Model_Stok->last_record();
				$ID_stokopname= $this->Model_Stok->get_obj_index($last_key, 'ID_stokopname');
				$Stok_Aktual = $this->input->post('Jumlah_Aktual'.$i);
				$Harga_lama = $this->input->post('Harga_lama'.$i);
				$Harga_baru = $this->input->post('Harga_Baru'.$i);
				if ($Stok_Aktual!='') {
					if ($Stok_Aktual > $Stok_tersedia) {
						$Selisih = $Stok_Aktual - $Stok_tersedia;
					}else {
						$Selisih = $Stok_tersedia - $Stok_Aktual;
					}
				}else {
					$Stok_Aktual=$Stok_tersedia;
					if ($Stok_Aktual > $Stok_tersedia) {
						$Selisih = $Stok_Aktual - $Stok_tersedia;
					}else {
						$Selisih = $Stok_tersedia - $Stok_Aktual;
					}
				}
				if ($Harga_baru=='') {
					$Harga_baru = $Harga_lama;
				}else {
					$Harga_baru = $Harga_baru;
				}
				$this->output->enable_profiler(TRUE);
				$this->Model_Stok->update_stok($Stok_Aktual, $ID_stok);
				$this->Model_Produk->update_harga($Harga_baru, $ID_stok);
				$this->Model_Stok->input_stokopname_relasi($ID_stok, $ID_stokopname, $Stok_tersedia, $Stok_Aktual, $Selisih, $Harga_lama, $Harga_baru);
				}
			}
		redirect('pengelola/stok/data_stokOpname');
	}

	public function stok_opnameInput()
	{
		$Keterangan= array('Keterangan' =>$this->input->get('Keterangan'));
		$data = $this->Model_Stok->getAllStok();
		$this->session->set_userdata('all_data', $data);
		$this->load->view('pengelola/stok/stok_opnameInput', $Keterangan);
	}
}
