<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class pengelola extends CI_Controller {
	public function __construct()
	  {
	    parent::__construct();
	    $this->load->model('Model_Pengelola');
	    if(is_null($this->session->userdata('id_pen'))) {
	    	redirect(base_url("pengelola/auth/login"));
	    }
	  }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data = $this->Model_Pengelola->getAllPengelola();
		$this->session->set_userdata('all_data', $data);
		$this->load->view('pengelola/pengelola/data_pengelola');
	}

	public function input_pengelola()
	{
		$this->load->view('pengelola/pengelola/input_pengelola');
	}

	public function proses_inputPengelola()
	{
		$pengelola = $this->Model_Pengelola;
        $validation = $this->form_validation;
		$validation->set_rules('id_pengelola', 'ID Pengelola', 'required|trim|is_unique[pengelola.ID_pengelola]');
		$validation->set_rules('email', 'Email', 'required|valid_email|is_unique[pengelola.Email]');
		$validation->set_rules('password', 'Password', 'required|min_length[5]');
		$validation->set_rules('password_confirm', 'Konfirmasi Password', 'required|matches[password]');
		$validation->set_rules('nama_pengelola', 'Nama Pengelola', 'required');
		$validation->set_rules('nomor_telp', 'No. HP', 'required|trim|numeric');
		$validation->set_message('is_unique', '%s telah terpakai, silahkan ganti dengan yang lain');
		$validation->set_message('matches', 'Password dan konfirmasi password harus sama');
		$validation->set_message('required', '{field} mohon diisi');
		$validation->set_message('numeric', 'No. HP harus angka');
		$validation->set_message('min_length', 'Password harus 5 karakter');
		$validation->set_message('valid_email', 'Format Email Salah');
		
		if ($validation->run()==FALSE) {
        	$this->load->view('pengelola/pengelola/input_pengelola');  
        }else {
        	$pengelola->save();
            $this->session->set_flashdata('success', 'Berhasil disimpan');
            		$this->session->set_flashdata('message', '
			<div class="alert alert-block alert-success"></i><a  class="close" data-dismiss="alert" aria-label="close">&times;</a></button>
			<i class="ace-icon fa fa-bullhorn green"></i> Data berhasil ditambahkan
			</div>');
           	redirect(site_url('pengelola/pengelola'));
        }
		 
	}

	public function edit_pengelola()
	{
		$query['dataPengelola'] = $this->Model_Pengelola->getPengelola($_GET['ID_pengelola']);
		$this->load->view('pengelola/pengelola/edit_pengelola', $query);
	}

	public function proses_editPengelola()
	{
		$pengelola = $this->Model_Pengelola;
        $validation = $this->form_validation;
		$validation->set_rules('email', 'Email', 'required|valid_email');
		$validation->set_rules('nama_pengelola', 'Nama Pengelola', 'required');
		$validation->set_rules('nomor_telp', 'No. HP', 'required|trim|numeric');
		$validation->set_message('required', '{field} mohon diisi');
		$validation->set_message('numeric', 'No. HP harus angka');
		$validation->set_message('valid_email', 'Format Email Salah');

		if ($validation->run()==FALSE) {
        	$this->load->view('pengelola/pengelola/edit_pengelola');  
        }else {
        	$pengelola->update();
            $this->session->set_flashdata('success', 'Berhasil disimpan');
            		$this->session->set_flashdata('message', '
			<div class="alert alert-block alert-success"></i><a  class="close" data-dismiss="alert" aria-label="close">&times;</a></button>
			<i class="ace-icon fa fa-bullhorn green"></i> Data berhasil diubah
			</div>');
            redirect(site_url('pengelola/pengelola'));
        }
	}

	public function hapus_pengelola()
	{
		$id_pengelola = $_GET['ID_pengelola'];

		$data = $this->Model_Pengelola->hapus_pengelola($id_pengelola);
		$this->session->set_flashdata('message', '
			<div class="alert alert-block alert-success"></i><a  class="close" data-dismiss="alert" aria-label="close">&times;</a></button>
			<i class="ace-icon fa fa-bullhorn green"></i> Data berhasil dihapus
			</div>');
		redirect('pengelola/pengelola');
	}

	public function detail_pengelola()
	{
		$query['dataPengelola'] = $this->Model_Pengelola->getDetailPengelola($_GET['ID_pengelola']);
		$this->load->view('pengelola/pengelola/detail_pengelola', $query);
	}

	public function profilePengelola()
	{
		$query['dataPengelola'] = $this->Model_Pengelola->getProfilepengelola();
		$this->load->view('pengelola/pengelola/detail_pengelola', $query);
	}
	
}
