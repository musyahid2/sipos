<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class home extends CI_Controller {
	public function __construct()
	  {
	    parent::__construct();
	    $this->load->model('Model_Login');
	    $this->load->model('Model_Produk');
	    $this->load->model('Model_Karyawan');
	    $this->load->model('Model_transaksi');
	    $this->load->model('chart_model');
	    
	    if(is_null($this->session->userdata('id_pen'))) {
	    	redirect(base_url("pengelola/auth/login"));
	    }
  }

	public function index()
	{
		$dataProduk = $this->Model_Produk->getJumlahproduk();
		$produkTerlaris = $this->Model_Produk->getBarangterlaris();
		$dataKaryawan = $this->Model_Karyawan->getJumlahkaryawan();
		$dataTransaksi = $this->Model_transaksi->getJumlahtransaksi();
		$totalTransaksi = $this->Model_transaksi->getTotaltransaksi();
		$kategoriTerlaris = $this->Model_Produk->getKategoriterlaris();
		$this->session->set_userdata('jumlahProduk', $dataProduk);
		$this->session->set_userdata('produkTerlaris', $produkTerlaris);
		$this->session->set_userdata('jumlahKaryawan', $dataKaryawan);
		$this->session->set_userdata('dataTransaksi', $dataTransaksi);
		$this->session->set_userdata('totalTransaksi', $totalTransaksi);
		$this->session->set_userdata('kategoriTerlaris', $kategoriTerlaris);
		$data = $this->chart_model->get_data()->result();
      	$x['data'] = json_encode($data);
		$this->load->view('pengelola/home', $x);
	}
}
