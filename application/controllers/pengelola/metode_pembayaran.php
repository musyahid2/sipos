<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class metode_pembayaran extends CI_Controller {
	public function __construct()
	  {
	    parent::__construct();
	    $this->load->model('Model_metodePembayaran');
	    if(is_null($this->session->userdata('id_pen'))) {
	    	redirect(base_url("pengelola/auth/login"));
	    }
	  }
	
	 
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function data_wallet()
	{
		$data['data_wallet'] = $this->Model_metodePembayaran->getAllWallet();
		$this->load->view('pengelola/metode_pembayaran/wallet/data_wallet', $data);
	}

	public function data_debit()
	{
		$data['data_wallet'] = $this->Model_metodePembayaran->getAllDebit();
		$this->load->view('pengelola/metode_pembayaran/debit/data_debit', $data);
	}

	public function input_debit()
	{
		$this->load->view('pengelola/metode_pembayaran/debit/input_debit');
	}

	public function input_wallet()
	{
		$this->load->view('pengelola/metode_pembayaran/wallet/input_wallet');
	}

	public function edit_wallet()
	{
		$query['dataWallet'] = $this->Model_metodePembayaran->getWallet($_GET['Id_metodePembayaran']);
		$this->load->view('pengelola/metode_pembayaran/wallet/edit_wallet', $query);
	}

	public function edit_debit()
	{
		$query['dataDebit'] = $this->Model_metodePembayaran->getDebit($_GET['Id_metodePembayaran']);
		$this->load->view('pengelola/metode_pembayaran/debit/edit_debit', $query);
	}

	public function proses_inputWallet()
	{
		$metode_pembayaran = $this->Model_metodePembayaran;
        $validation = $this->form_validation;
		$this->form_validation->set_rules('id_metodePembayaran', 'ID Metode Pembayaran', 'required|trim|is_unique[metode_pembayaran.id_metodePembayaran]');
		$validation->set_rules('nama_metodePembayaran', 'Nama Metode Pembayaran', 'required');
		$validation->set_message('required', '{field} mohon diisi');
		$validation->set_message('is_unique', '%s telah terpakai, silahkan ganti dengan yang lain');

		if ($validation->run()==FALSE) {
        	$this->load->view('pengelola/metode_pembayaran/input_wallet');  
        }else {
        	$metode_pembayaran->saveWallet();
            $this->session->set_flashdata('success', 'Berhasil disimpan');
            		$this->session->set_flashdata('message', '
			<div class="alert alert-block alert-success"></i><a  class="close" data-dismiss="alert" aria-label="close">&times;</a></button>
			<i class="ace-icon fa fa-bullhorn green"></i> Metode pembayaran berhasil ditambahkan
			</div>');
           	redirect(site_url('pengelola/metode_pembayaran/data_wallet'));
        }

	}

	public function proses_inputDebit()
	{
		$metode_pembayaran = $this->Model_metodePembayaran;
        $validation = $this->form_validation;
		$this->form_validation->set_rules('id_metodePembayaran', 'ID Metode Pembayaran', 'required|trim|is_unique[metode_pembayaran.id_metodePembayaran]');
		$validation->set_rules('nama_metodePembayaran', 'Nama Metode Pembayaran', 'required');
		$validation->set_message('required', '{field} mohon diisi');
		$validation->set_message('is_unique', '%s telah terpakai, silahkan ganti dengan yang lain');

		if ($validation->run()==FALSE) {
        	$this->load->view('pengelola/metode_pembayaran/debit/input_debit');  
        }else {
        	$metode_pembayaran->saveDebit();
            $this->session->set_flashdata('success', 'Berhasil disimpan');
            		$this->session->set_flashdata('message', '
			<div class="alert alert-block alert-success"></i><a  class="close" data-dismiss="alert" aria-label="close">&times;</a></button>
			<i class="ace-icon fa fa-bullhorn green"></i> Metode pembayaran berhasil ditambahkan
			</div>');
           	redirect(site_url('pengelola/metode_pembayaran/data_debit'));
        }
	}

	public function proses_editWallet()
	{
		$metode_pembayaran = $this->Model_metodePembayaran;
        $validation = $this->form_validation;
		$this->form_validation->set_rules('id_metodePembayaran', 'ID Metode Pembayaran', 'required');
		$validation->set_rules('nama_metodePembayaran', 'Nama Metode Pembayaran', 'required');
		$validation->set_message('required', '{field} mohon diisi');

		if ($validation->run()==FALSE) {
        	$this->load->view('pengelola/metode_pembayaran/wallet/edit_wallet'); 
        }else {
        	$metode_pembayaran->updatemetodePembayaran();
            $this->session->set_flashdata('success', 'Berhasil disimpan');
            		$this->session->set_flashdata('message', '
			<div class="alert alert-block alert-success"></i><a  class="close" data-dismiss="alert" aria-label="close">&times;</a></button>
			<i class="ace-icon fa fa-bullhorn green"></i> Data berhasil diubah
			</div>');
           	redirect(site_url('pengelola/metode_pembayaran/data_wallet'));
        }
	}

		public function proses_editDebit()
	{
		$metode_pembayaran = $this->Model_metodePembayaran;
        $validation = $this->form_validation;
		$this->form_validation->set_rules('id_metodePembayaran', 'ID Metode Pembayaran', 'required');
		$validation->set_rules('nama_metodePembayaran', 'Nama Metode Pembayaran', 'required');
		$validation->set_message('required', '{field} mohon diisi');

		if ($validation->run()==FALSE) {
        	$this->load->view('pengelola/metode_pembayaran/debit/edit_debit'); 
        }else {
        	$metode_pembayaran->updatemetodePembayaran();
            $this->session->set_flashdata('success', 'Berhasil disimpan');
            		$this->session->set_flashdata('message', '
			<div class="alert alert-block alert-success"></i><a  class="close" data-dismiss="alert" aria-label="close">&times;</a></button>
			<i class="ace-icon fa fa-bullhorn green"></i> Data berhasil diubah
			</div>');
           	redirect(site_url('pengelola/metode_pembayaran/data_debit'));
        }
	}

	public function hapus_metodePembayaran()
	{
		$Id_metodePembayaran = $_GET['Id_metodePembayaran'];

		$data = $this->Model_metodePembayaran->hapus_wallet($Id_metodePembayaran);
		$this->session->set_flashdata('message', '
			<div class="alert alert-block alert-warning"></i><a  class="close" data-dismiss="alert" aria-label="close">&times;</a></button>
			<i class="ace-icon fa fa-bullhorn green"></i> Data berhasil dihapus
			</div>');
		redirect(site_url('pengelola/metode_pembayaran/data_wallet'));
	}

	

}