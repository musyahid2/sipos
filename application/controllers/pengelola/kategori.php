<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Controller {
	public function __construct()
	  {
	    parent::__construct();
	    $this->load->model('Model_Kategori');
	    if(is_null($this->session->userdata('id_pen'))) {
	    	redirect(base_url("pengelola/auth/login"));
	    }
	  }
	
	 
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function index()
	{
		$data = $this->Model_Kategori->getAllKategori();
		$this->session->set_userdata('all_data', $data);
		$this->load->view('pengelola/kategori/Daftar_Kategori');
	}

	public function input_kategori()
	{
		$this->load->view('pengelola/kategori/input_kategori');
	}

	public function proses_inputKategori()
	{
		$Kode_Kategori = $this->input->post('kode_kategori');
		$Nama_kategori = $this->input->post('nama_kategori');
		$Id_pengelola = $this->session->userdata('id_pen');

		$this->Model_Kategori->input_kategori($Kode_Kategori, $Nama_kategori, $Id_pengelola);

		redirect('pengelola/kategori');
	}

	public function edit_kategori()
	{
		$query['dataKategori'] =  $this->Model_Kategori->getKategori($_GET['Kode_Kategori']);
		$this->load->view('pengelola/kategori/edit_kategori', $query);
	}	

	public function proses_editKategori()
	{
		$kode 		= $this->input->post('kode_kategori');
		$namaKat	 = $this->input->post('nama_kategori');
		$Id_pengelola = $this->session->userdata('id_pen');


		$this->Model_Kategori->update_kategori($kode, $namaKat, $Id_pengelola);

		redirect('pengelola/kategori');
	}
	public function hapus_kategori()
	{
		$Kode_Kategori = $_GET['Kode_Kategori'];

		$this->Model_Kategori->hapus_kategori($Kode_Kategori);
		$this->session->set_flashdata('message', '
			<div class="alert alert-block alert-success"></i><a  class="close" data-dismiss="alert" aria-label="close">&times;</a></button>
			<i class="ace-icon fa fa-bullhorn green"></i> Kategori berhasil dihapus
			</div>');
		redirect('pengelola/kategori');
	}
}