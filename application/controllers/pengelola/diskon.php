<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Diskon extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Model_Diskon');
		$this->load->library('form_validation');
		if(is_null($this->session->userdata('id_pen'))) {
	    	redirect(base_url("pengelola/auth/login"));
	    }
	}

	public function index()
	{
		$this->load->view('pengelola/diskon/input_diskon');
	}

	public function all_diskon_persen()
	{
		$this->load->view('pengelola/diskon/all_diskon_persen');
	}

	public function input_all_diskon_persen()
	{
		$this->form_validation->set_rules('kode_diskon', 'Kode Diskon', 'required|trim|is_unique[diskon.Kode_Diskon]');
		$this->form_validation->set_rules('nama_diskon', 'Nama Diskon', 'required');
		$this->form_validation->set_rules('persentase_diskon', 'Persentase Diskon', 'required|trim|numeric');
		$this->form_validation->set_message('is_unique', '%s telah terpakai, silahkan ganti dengan yang lain');
		$this->form_validation->set_message('required', '{field} mohon diisi');
		$this->form_validation->set_message('numeric', 'diisi dalam persentase');

		if ($this->form_validation->run()==TRUE) {
			
		
			$kode_diskon = $this->input->post('kode_diskon');
			$nama_diskon = $this->input->post('nama_diskon');
			$persentase_diskon = $this->input->post('persentase_diskon');
			$id_pengelola = $this->session->userdata("id_pen");
			$status = 'Pending';

			$this->Model_Diskon->input_all_diskon_persen($kode_diskon, $nama_diskon, $persentase_diskon, $id_pengelola, $status);

			$this->session->set_flashdata('success', 'Berhasil disimpan');
            		$this->session->set_flashdata('message', '
			<div class="alert alert-block alert-success"></i><a  class="close" data-dismiss="alert" aria-label="close">&times;</a></button>
			<i class="ace-icon fa fa-bullhorn green"></i> Data berhasil ditambahkan
			</div>');

            redirect('pengelola/diskon/daftar_diskon_pending', 'refresh');
		}
		else
		{
			$this->load->view('pengelola/diskon/all_diskon_persen');
		}
	}

	public function all_diskon_rupiah()
	{
		$this->load->view('pengelola/diskon/all_diskon_rupiah');
	}

	public function input_all_diskon_rupiah()
	{
		$this->form_validation->set_rules('kode_diskon', 'Kode Diskon', 'required|trim|is_unique[diskon.Kode_Diskon]');
		$this->form_validation->set_rules('nama_diskon', 'Nama Diskon', 'required');
		$this->form_validation->set_rules('potongan_harga', 'Potongan Harga', 'required|trim|numeric');
		$this->form_validation->set_message('is_unique', '%s telah terpakai, silahkan ganti dengan yang lain');
		$this->form_validation->set_message('required', '{field} mohon diisi');

		if ($this->form_validation->run()==TRUE) {
			
		
			$kode_diskon = $this->input->post('kode_diskon');
			$nama_diskon = $this->input->post('nama_diskon');
			$potongan = $this->input->post('potongan_harga');
			$pot_harga = preg_replace('/[^A-Za-z0-9]/', '', $potongan);
    		$potongan_harga = (int) $pot_harga;
			$id_pengelola = $this->session->userdata("id_pen");
			$status = 'Pending';

			$this->Model_Diskon->input_all_diskon_rupiah($kode_diskon, $nama_diskon, $potongan_harga, $id_pengelola, $status);

			$this->session->set_flashdata('success', 'Berhasil disimpan');
            		$this->session->set_flashdata('message', '
			<div class="alert alert-block alert-success"></i><a  class="close" data-dismiss="alert" aria-label="close">&times;</a></button>
			<i class="ace-icon fa fa-bullhorn green"></i> Data berhasil ditambahkan
			</div>');

            redirect('pengelola/diskon/daftar_diskon_pending', 'refresh');
		}
		else
		{
			$this->load->view('pengelola/diskon/all_diskon_rupiah');
		}
	}

	public function all_persen_max()
	{
		$this->load->view('pengelola/diskon/all_persen_max');
	}

	public function input_all_persen_max()
	{
		$this->form_validation->set_rules('kode_diskon', 'Kode Diskon', 'required|trim|is_unique[diskon.Kode_Diskon]');
		$this->form_validation->set_rules('nama_diskon', 'Nama Diskon', 'required');
		$this->form_validation->set_rules('persentase_diskon', 'Persentase Diskon', 'required|trim|numeric');
		$this->form_validation->set_rules('maksimal_diskon', 'Maksimal Diskon', 'required|trim|numeric');

		$this->form_validation->set_message('is_unique', '%s telah terpakai, silahkan ganti dengan yang lain');
		$this->form_validation->set_message('required', '{field} mohon diisi');

		if ($this->form_validation->run()==TRUE) {
			
		
			$kode_diskon = $this->input->post('kode_diskon');
			$nama_diskon = $this->input->post('nama_diskon');
			$persentase_diskon = $this->input->post('persentase_diskon');
			$makdiskon = $this->input->post('maksimal_diskon');
			$mak_diskon = preg_replace('/[^A-Za-z0-9]/', '', $makdiskon);
    		$maksimal_diskon = (int) $mak_diskon;
			$id_pengelola = $this->session->userdata("id_pen");
			$status = 'Pending';

			$this->Model_Diskon->input_all_persen_max($kode_diskon, $nama_diskon, $persentase_diskon, $maksimal_diskon, $id_pengelola, $status);

			$this->session->set_flashdata('success', 'Berhasil disimpan');
            		$this->session->set_flashdata('message', '
			<div class="alert alert-block alert-success"></i><a  class="close" data-dismiss="alert" aria-label="close">&times;</a></button>
			<i class="ace-icon fa fa-bullhorn green"></i> Data berhasil ditambahkan
			</div>');

            redirect('pengelola/diskon/daftar_diskon_pending', 'refresh');
		}
		else
		{
			$this->load->view('pengelola/diskon/all_persen_max');
		}
	}

	public function min_trans_persen()
	{
		$this->load->view('pengelola/diskon/min_belanja_persen');
	}

	public function input_min_trans_persen()
	{
		$this->form_validation->set_rules('kode_diskon', 'Kode Diskon', 'required|trim|is_unique[diskon.Kode_Diskon]');
		$this->form_validation->set_rules('nama_diskon', 'Nama Diskon', 'required');
		$this->form_validation->set_rules('persentase_diskon', 'Persentase Diskon', 'required|trim|numeric');
		$this->form_validation->set_rules('minimal_transaksi', 'Minimal Transaksi', 'required|trim|numeric');

		$this->form_validation->set_message('is_unique', '%s telah terpakai, silahkan ganti dengan yang lain');
		$this->form_validation->set_message('required', '{field} mohon diisi');

		if ($this->form_validation->run()==TRUE) {
			
		
			$kode_diskon = $this->input->post('kode_diskon');
			$nama_diskon = $this->input->post('nama_diskon');
			$persentase_diskon = $this->input->post('persentase_diskon');
			$mintrans = $this->input->post('minimal_transaksi');
			$min_trans = preg_replace('/[^A-Za-z0-9]/', '', $mintrans);
    		$minimal_transaksi = (int) $min_trans;
			$id_pengelola = $this->session->userdata("id_pen");
			$status = 'Pending';

			$this->Model_Diskon->input_min_trans_persen($kode_diskon, $nama_diskon, $persentase_diskon, $minimal_transaksi, $id_pengelola, $status);

			$this->session->set_flashdata('success', 'Berhasil disimpan');
            		$this->session->set_flashdata('message', '
			<div class="alert alert-block alert-success"></i><a  class="close" data-dismiss="alert" aria-label="close">&times;</a></button>
			<i class="ace-icon fa fa-bullhorn green"></i> Data berhasil ditambahkan
			</div>');

            $this->load->view('pengelola/diskon/daftar_diskon_pending', 'refresh');
		}
		else
		{
			$this->load->view('pengelola/diskon/min_belanja_persen');
		}
	}

	public function min_trans_rupiah()
	{
		$this->load->view('pengelola/diskon/min_belanja_rupiah');
	}

	public function input_min_trans_rupiah()
	{
		$this->form_validation->set_rules('kode_diskon', 'Kode Diskon', 'required|trim|is_unique[diskon.Kode_Diskon]');
		$this->form_validation->set_rules('nama_diskon', 'Nama Diskon', 'required');
		$this->form_validation->set_rules('potongan_harga', 'Potongan Harga', 'required|trim|numeric');
		$this->form_validation->set_rules('minimal_transaksi', 'Minimal Transaksi', 'required|trim|numeric');

		$this->form_validation->set_message('is_unique', '%s telah terpakai, silahkan ganti dengan yang lain');
		$this->form_validation->set_message('required', '{field} mohon diisi');

		if ($this->form_validation->run()==TRUE) {
			
		
			$kode_diskon = $this->input->post('kode_diskon');
			$nama_diskon = $this->input->post('nama_diskon');
			$pot_harga = $this->input->post('potongan_harga');
			$potong_harga = preg_replace('/[^A-Za-z0-9]/', '', $pot_harga);
    		$potongan_harga = (int) $potong_harga;
			$min_transaksi = $this->input->post('minimal_transaksi');
			$min_trx = preg_replace('/[^A-Za-z0-9]/', '', $min_transaksi);
    		$minimal_transaksi = (int) $min_trx;
			$id_pengelola = $this->session->userdata("id_pen");
			$status = 'Pending';

			$this->Model_Diskon->input_min_trans_rupiah($kode_diskon, $nama_diskon, $potongan_harga, $minimal_transaksi, $id_pengelola, $status);

			$this->session->set_flashdata('success', 'Berhasil disimpan');
            		$this->session->set_flashdata('message', '
			<div class="alert alert-block alert-success"></i><a  class="close" data-dismiss="alert" aria-label="close">&times;</a></button>
			<i class="ace-icon fa fa-bullhorn green"></i> Data berhasil ditambahkan
			</div>');

            $this->load->view('pengelola/diskon/daftar_diskon_pending', 'refresh');
		}
		else
		{
			$this->load->view('pengelola/diskon/min_belanja_persen');
		}
	}

	public function min_trans_persen_max_diskon()
	{
		$this->load->view('pengelola/diskon/min_belanja_persen_max_diskon');
	}

	public function input_min_trans_persen_max_diskon()
	{
		$this->form_validation->set_rules('kode_diskon', 'Kode Diskon', 'required|trim|is_unique[diskon.Kode_Diskon]');
		$this->form_validation->set_rules('nama_diskon', 'Nama Diskon', 'required');
		$this->form_validation->set_rules('persentase_diskon', 'Persentase Diskon', 'required|trim|numeric');
		$this->form_validation->set_rules('maksimal_diskon', 'Potongan Harga', 'required|trim|numeric');
		$this->form_validation->set_rules('minimal_transaksi', 'Minimal Transaksi', 'required|trim|numeric');

		$this->form_validation->set_message('is_unique', '%s telah terpakai, silahkan ganti dengan yang lain');
		$this->form_validation->set_message('required', '{field} mohon diisi');

		if ($this->form_validation->run()==TRUE) {
			
		
			$kode_diskon = $this->input->post('kode_diskon');
			$nama_diskon = $this->input->post('nama_diskon');
			$persentase_diskon = $this->input->post('persentase_diskon');
			$maks_diskon = $this->input->post('maksimal_diskon');
			$maks_diskn = preg_replace('/[^A-Za-z0-9]/', '', $maks_diskon);
    		$maksimal_diskon = (int) $maks_diskn;	
			$min_transaksi = $this->input->post('minimal_transaksi');
			$min_trx = preg_replace('/[^A-Za-z0-9]/', '', $min_transaksi);
    		$minimal_transaksi = (int) $min_trx;
			$id_pengelola = $this->session->userdata("id_pen");
			$status = 'Pending';

			$this->Model_Diskon->input_min_trans_persen_max_diskon($kode_diskon, $nama_diskon, $persentase_diskon, $maksimal_diskon, $minimal_transaksi, $id_pengelola, $status);

			$this->session->set_flashdata('success', 'Berhasil disimpan');
            		$this->session->set_flashdata('message', '
			<div class="alert alert-block alert-success"></i><a  class="close" data-dismiss="alert" aria-label="close">&times;</a></button>
			<i class="ace-icon fa fa-bullhorn green"></i> Data berhasil ditambahkan
			</div>');

            $this->load->view('pengelola/diskon/daftar_diskon_pending', 'refresh');
		}
		else
		{
			$this->load->view('pengelola/diskon/min_belanja_persen');
		}
	}

	public function daftar_diskon_pending()
	{
		$data = $this->Model_Diskon->daftar_diskon_pending();
		$this->session->set_userdata('diskon_pending', $data);
		$this->load->view('pengelola/diskon/daftar_diskon_pending');
	}

	public function approve_diskon()
	{
		$this->Model_Diskon->approve_diskon($_GET['Kode_Diskon']);
		redirect('pengelola/diskon/daftar_diskon_aktif');
	}

	public function hapus_diskon_pending()
	{
		$this->Model_Diskon->hapus_diskon_pending($_GET['Kode_Diskon']);
		redirect('pengelola/diskon/daftar_diskon_pending');
	}

	public function daftar_diskon_aktif()
	{
		$data = $this->Model_Diskon->daftar_diskon_aktif();
		$this->session->set_userdata('diskon_aktif', $data);
		$this->load->view('pengelola/diskon/daftar_diskon_aktif');
	}

	public function diskon_selesai()
	{
		$this->Model_Diskon->diskon_selesai($_GET['Kode_Diskon']);
		redirect('pengelola/diskon/daftar_diskon_aktif');
	}

	public function history_diskon()
	{
		$data = $this->Model_Diskon->history_diskon();
		$this->session->set_userdata('history', $data);
		$this->load->view('pengelola/diskon/history_diskon');
	}


}

/* End of file diskon.php */
/* Location: ./application/controllers/pengelola/diskon.php */