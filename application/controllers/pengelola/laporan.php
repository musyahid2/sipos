<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {
	public function __construct()
	  {
	    parent::__construct();
	    $this->load->model('Model_laporan');
	    if(is_null($this->session->userdata('id_pen'))) {
	    	redirect(base_url("pengelola/auth/login"));
	    }
	  }
	
	 
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function transaksiPenjualan()
	{
		if ( count($this->input->post()) > 0 ) {
			$tgl = $this->input->post("tanggal");
			$data['dataTransaksipenjualan'] = $this->Model_laporan->getRentangwaktutransaksi($tgl);
			$data['grandTotal'] = $this->Model_laporan->getGrandtotaltransaksibySearch($tgl);
			$this->load->view('pengelola/laporan/transaksi_penjualan', $data);
		}else {
			$data['dataTransaksipenjualan'] = $this->Model_laporan->getTransaksipenjualan();
			$data['grandTotal'] = $this->Model_laporan->getGrandtotaltransaksi();
			$this->load->view('pengelola/laporan/transaksi_penjualan', $data);
		}
	}


	public function penjualanProduk()
	{
		if ( count($this->input->post()) > 0 ) {
			$tgl = $this->input->post("tanggal");
			$data['dataPenjualan'] = $this->Model_laporan->getRentangwaktu($tgl);
			$data['grandTotal'] = $this->Model_laporan->getGrandtotalpenjualanbySearch($tgl);
			$this->load->view('pengelola/laporan/penjualan_produk', $data);
		} else {
			$data['dataPenjualan'] = $this->Model_laporan->getDatapenjualan();
			$data['grandTotal'] = $this->Model_laporan->getGrandtotalpenjualan();
			$this->load->view('pengelola/laporan/penjualan_produk', $data);
		}
	}

	public function labaProduk()
	{
		if ( count($this->input->post()) > 0 ) {
			$tgl = $this->input->post("tanggal");
			$data['labaProduk'] = $this->Model_laporan->getRentangwaktulaba($tgl);
			$data['grandTotal'] = $this->Model_laporan->getGrandtotallababySearch($tgl);
			$this->load->view('pengelola/laporan/laba_Produk', $data);
		} else {
			$data['labaProduk'] = $this->Model_laporan->labaProduk();
			$data['grandTotal'] = $this->Model_laporan->getGrandtotalLaba();
			$this->load->view('pengelola/laporan/laba_Produk', $data);
		}
	}

	public function penjualanKategori()
	{
		if (count($this->input->post()) > 0) {
			$tgl = $this->input->post("tanggal");
			$data['penjualanKategori'] = $this->Model_laporan->getRentangwaktupenjualanKategori($tgl);
			$data['grandTotal'] = $this->Model_laporan->getGrandtotalpenjualanKategoribySearch($tgl);
			$this->load->view('pengelola/laporan/penjualan_kategori', $data);
		} else {
			$data['penjualanKategori'] = $this->Model_laporan->getpenjualanKategori();
			$data['grandTotal'] = $this->Model_laporan->getGrandtotalPenjualankategori();
			$this->load->view('pengelola/laporan/penjualan_kategori', $data);
		}
	}
	
	public function historykasir()
	{
		$data['historykasir'] = $this->Model_laporan->getHistoryKasir();
		$this->load->view('pengelola/laporan/history_kasir', $data);
	}
}