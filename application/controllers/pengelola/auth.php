<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class auth extends CI_Controller {
	public function __construct()
	  {
	    parent::__construct();
	    $this->load->model('Model_Login');
	    $this->load->model('Model_Pengelola');


	    if(!is_null($this->session->userdata('id_kas'))){
  			redirect('kasir/home');
  		}
  		if(!is_null($this->session->userdata('id_pen'))) {
  			redirect('pengelola/home');
			}


	  }




	public function login()
	{
		$this->load->view('pengelola/login');
	}

	public function proses_login()
	{
		$id_pengelola = $this->input->post('id_pengelola');
		$password = $this->input->post('password');

		$pass = md5($password);
		$cekLogin = $this->Model_Login->cekLogin($id_pengelola, $pass);
		$hasil = count($cekLogin);

		if ($hasil > 0) {
			
			$sedangLogin = $this->db->get_where('pengelola', array('ID_pengelola'=>$id_pengelola, 'Password'=> $pass))->row();
			$nama = $this->Model_Login->getPengelola($sedangLogin->ID_pengelola)[0]->Nama_pengelola;
			$id_pen = $this->Model_Login->getPengelola($sedangLogin->ID_pengelola)[0]->ID_pengelola;
			$foto = $this->Model_Login->getPengelola($sedangLogin->ID_pengelola)[0]->Foto;
	
			$dataSession = array(
				'nama' => $nama,
				'id_pen' => $id_pen,
				'foto' => $foto,
				'Status' => 'Login'
			);
		

			$this->session->set_userdata($dataSession);
			redirect(base_url("pengelola/home"));
		}
		else {
			$this->session->set_flashdata('message', '
			<div class="alert alert-block alert-danger"></i></button>
			<i class="ace-icon fa fa-bullhorn green"></i> ID atau Password anda salah
			</div>');
			redirect(base_url("pengelola/auth/login"));
		}
	}

}
