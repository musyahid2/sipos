<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class karyawan extends CI_Controller {
	public function __construct()
	  {
	    parent::__construct();
	    $this->load->model('Model_Karyawan');
	    if(is_null($this->session->userdata('id_pen'))) {
	    	redirect(base_url("pengelola/auth/login"));
	    }
	  }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data = $this->Model_Karyawan->getAllKasir();
		$this->session->set_userdata('all_data', $data);
		$this->load->view('pengelola/karyawan/data_kasir');
	}




	public function input_kasir()
	{
		$this->load->view('pengelola/karyawan/input_kasir');
	}

	public function proses_inputKasir()
	{
		$karyawan = $this->Model_Karyawan;
        $validation = $this->form_validation;
		$this->form_validation->set_rules('id_kasir', 'ID Kasir', 'required|trim|min_length[5]|is_unique[kasir.ID_kasir]');
		$validation->set_rules('pin', 'PIN', 'required|min_length[5]');
		$validation->set_rules('pin_confirm', 'Konfirmasi PIN', 'required|matches[pin]');
		$validation->set_rules('nama_kasir', 'Nama Kasir', 'required');
		$validation->set_rules('Jenis_Kelamin', 'Jenis Kelamim', 'required');
		$validation->set_rules('no_hp', 'No. HP', 'required|trim|numeric');
		$validation->set_rules('alamat', 'Alamat', 'required');
		$validation->set_rules('Status', 'Status', 'required');
		$validation->set_message('required', '{field} mohon diisi');
		$validation->set_message('matches', 'PIN dan PIN password harus sama');
		$validation->set_message('numeric', 'No. HP harus angka');
		$validation->set_message('min_length', '{field} harus lebih dari 5 karakter');
		$validation->set_message('is_unique', '%s telah terpakai, silahkan ganti dengan yang lain');

		if ($validation->run()==FALSE) {
        	$this->load->view('pengelola/karyawan/input_kasir');  
        }else {
        	$karyawan->save();
            $this->session->set_flashdata('success', 'Berhasil disimpan');
            		$this->session->set_flashdata('message', '
			<div class="alert alert-block alert-success"></i><a  class="close" data-dismiss="alert" aria-label="close">&times;</a></button>
			<i class="ace-icon fa fa-bullhorn green"></i> Data berhasil ditambahkan
			</div>');
           	redirect(site_url('pengelola/karyawan'));
        }
	

	}

	public function edit_kasir()
	{
		$query['dataKasir'] = $this->Model_Karyawan->getKasir($_GET['ID_kasir']);
		$this->load->view('pengelola/karyawan/edit_kasir', $query);
	}

	public function proses_editKasir()
	{
		$karyawan = $this->Model_Karyawan;
       	$validation = $this->form_validation;
		$validation->set_rules('nama_kasir', 'Nama Kasir', 'required');
		$validation->set_rules('Jenis_Kelamin', 'Jenis Kelamim', 'required');
		$validation->set_rules('no_hp', 'No. HP', 'required|trim|numeric');
		$validation->set_rules('alamat', 'Alamat', 'required');
		$validation->set_rules('Status', 'Status', 'required');
		$validation->set_message('required', '{field} mohon diisi');
		$validation->set_message('numeric', 'No. HP harus angka');

		if ($validation->run()==FALSE) {
        	$this->load->view('pengelola/karyawan/edit_kasir');  
        }else {
        	$karyawan->update();
            $this->session->set_flashdata('success', 'Berhasil disimpan');
            		$this->session->set_flashdata('message', '
			<div class="alert alert-block alert-success"></i><a  class="close" data-dismiss="alert" aria-label="close">&times;</a></button>
			<i class="ace-icon fa fa-bullhorn green"></i> Data berhasil diubah
			</div>');
           	redirect(site_url('pengelola/karyawan'));
        }
	}

	public function detail_kasir()
	{
		$query['dataKasir'] = $this->Model_Karyawan->getDetailKasir($_GET['ID_kasir']);
		$gambar['img'] = $this->Model_Karyawan->getimg($_GET['ID_kasir']);
		$data = array_merge($query, $gambar);
		$this->load->view('pengelola/karyawan/detail_kasir', $data);
	}

	public function hapus_kasir()
	{
		$id_kasir = $_GET['ID_kasir'];

		$data = $this->Model_Karyawan->hapus_kasir($id_kasir);
		$this->session->set_flashdata('message', '
			<div class="alert alert-block alert-warning"></i><a  class="close" data-dismiss="alert" aria-label="close">&times;</a></button>
			<i class="ace-icon fa fa-bullhorn green"></i> Data berhasil dihapus
			</div>');
		redirect('pengelola/karyawan');
	}
	// public function proses_inputKasir()
	// {
	// 	$config = array(
	// 		'upload_path' => './assets/images/kasir',
	// 		'allowed_types' => 'jpeg|jpg|png',
	// 		'max_size' => '048',
	// 		'max_width' => '4000',
	// 		'max_height' => '4000'
 // 		);
	// 	$this->load->library('upload', $config);

	// 	if (!$this->upload->do_upload('filefoto')) {
	// 		$this->session->set_flashdata('message', "<div style='color:#ff0000;'>" . $this->upload->display_errors() . "</div>");
	// 		redirect(site_url('pengelola/karyawan/input_kasir'));
	// 	} else {
	// 		$file = $this->upload->data();
	// 		$dataKaryawan = array(
	// 				'ID_kasir' => $this->input->post('id_kasir'),
	// 				'PIN' => md5($this->input->post('pin')),
	// 				'Foto' => $file['file_name'],
	// 				'Nama_kasir' => $this->input->post('nama_kasir'),
	// 				'Jenis_kelamin' => $this->input->post('Jenis_Kelamin'),
	// 				'Nomor_telp' => $this->input->post('no_hp'),
	// 				'Tanggal_masuk' => mdate('%Y-%m-%d %H:%i:%s', now()),
	// 				'Alamat' => $this->input->post('alamat'),
	// 				'Status' => $this->input->post('Status'),
	// 				'id_pengelola' => $this->session->userdata("id_pen")
	// 		);

	// 		$this->Model_Karyawan->input_kasir($dataKaryawan);
	// 	}
	// 	$this->session->set_flashdata('message', '
	// 		<div class="alert alert-block alert-success"></i><a  class="close" data-dismiss="alert" aria-label="close">&times;</a></button>
	// 		<i class="ace-icon fa fa-bullhorn green"></i> Data berhasil ditambahkan
	// 		</div>');
	// 	redirect(site_url('pengelola/karyawan'));


	// }
}
