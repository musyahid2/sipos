<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class produk extends CI_Controller {
	public function __construct()
	  {
	    parent::__construct();
	    $this->load->model('Model_Karyawan');
	    $this->load->model('Model_Produk');
		$this->load->model('Model_Kategori');
		$this->load->library('form_validation');
		if(is_null($this->session->userdata('id_pen'))) {
	    	redirect(base_url("pengelola/auth/login"));
	    }
	  }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function index()
	{
		$data = $this->Model_Produk->getAllProduk();
		$this->session->set_userdata('all_data', $data);
		$this->load->view('pengelola/produk/Daftar_Produk');
	}

	public function input_produk()
 	{
 		$data = $this->Model_Produk->getAllKategori();
	    $this->session->set_userdata('all_data', $data);
	    $this->load->view('pengelola/produk/Input_Produk');
 	}


	public function prosesinputProduk()
 	{
 
	   $barang = $this->Model_Produk;
       $validation = $this->form_validation;
       $validation->set_rules('barcode', 'BARCODE', 'required|trim|is_unique[barang.Barcode]');
	   $validation->set_rules('sku', 'SKU', 'required');
		$validation->set_rules('nama_barang', 'Nama Barang', 'required');
		$validation->set_rules('harga', 'Harga', 'required|numeric');
		$validation->set_rules('satuan', 'Satuan Barang', 'required');
		$validation->set_rules('kategori', 'Kategori', 'required');
		$validation->set_message('required', '{field} mohon diisi');
		$validation->set_message('is_unique', '%s telah terpakai, silahkan ganti dengan yang lain');
        if ($validation->run()==FALSE) {
        	$this->load->view('pengelola/produk/Input_Produk');  
        }else {
        	$barang->save();
            $this->session->set_flashdata('success', 'Berhasil disimpan');
            		$this->session->set_flashdata('message', '
			<div class="alert alert-block alert-success"></i><a  class="close" data-dismiss="alert" aria-label="close">&times;</a></button>
			<i class="ace-icon fa fa-bullhorn green"></i> Data berhasil ditambahkan
			</div>');
           	redirect(site_url('pengelola/produk'));
        }
 	}


	public function update_produk()
	{
		$data = $this->Model_Kategori->getAllKategori();
		$this->session->set_userdata('all_data', $data);
		$query['dataProduk'] = $this->Model_Produk->getProduk($_GET['BARCODE']);
		$this->load->view('pengelola/produk/Update_Produk',$query);
	}

	public function proses_updateBarang()
	{
		$barang = $this->Model_Produk;
       $validation = $this->form_validation;
	   $validation->set_rules('sku', 'SKU', 'required');
		$validation->set_rules('nama_barang', 'Nama Barang', 'required');
		$validation->set_rules('harga', 'Harga', 'required|numeric');
		$validation->set_rules('satuan', 'Satuan Barang', 'required');
		$validation->set_rules('kategori', 'Kategori', 'required|trim|numeric');
		$validation->set_message('required', '{field} mohon diisi');
        if ($validation->run()==FALSE) {
        	$this->load->view('pengelola/produk/Update_Produk');  
        }else {
        	$barang->update();
            $this->session->set_flashdata('success', 'Berhasil disimpan');
            		$this->session->set_flashdata('message', '
			<div class="alert alert-block alert-success"></i><a  class="close" data-dismiss="alert" aria-label="close">&times;</a></button>
			<i class="ace-icon fa fa-bullhorn green"></i> Data berhasil diubah
			</div>');
           	redirect(site_url('pengelola/produk'));
        }
	}

	public function hapus_produk()
	 {
		 	 $Barcode = $_GET['barcode'];
			 $this->Model_Produk->hapus_produk($Barcode);
			 $this->session->set_flashdata('message', '
			<div class="alert alert-block alert-warning"></i><a  class="close" data-dismiss="alert" aria-label="close">&times;</a></button>
			<i class="ace-icon fa fa-bullhorn green"></i> Produk berhasil dihapus
			</div>');
			 redirect('pengelola/produk');

	 }

	 public function detail_produk()
	{
		$query['dataProduk'] = $this->Model_Produk->getDetailProduk($_GET['Barcode']);
		$gambar['img'] = $this->Model_Produk->getimg($_GET['Barcode']);
		$data = array_merge($query, $gambar);
		$this->load->view('pengelola/produk/detail_produk', $data);
	}

}

