<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class produk extends CI_Controller {
	public function __construct()
	  {
	    parent::__construct();
	    $this->load->model('Model_Karyawan');
	    $this->load->model('Model_Produk');
			$this->load->model('Model_Kategori');
	  }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function index()
	{
		$data = $this->Model_Produk->getAllProduk();
		$this->session->set_userdata('all_data', $data);
		$this->load->view('pengelola/produk/Daftar_Produk');
	}


	public function input_produk()
 	{
	  $data = $this->Model_Produk->getAllKategori();
	  $this->session->set_userdata('all_data', $data);
	  $this->load->view('pengelola/produk/Input_Produk');
 	}

	//Barang
	public function proses_inputBarang()
	{
		$config = array(
			'upload_path' => './assets/images/produk',
			'allowed_types' => 'jpeg|jpg|png',
			'max_size' => '2048',
			'max_width' => '4000',
			'max_height' => '4000'
 		);
		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('filefoto')) {
			$this->session->set_flashdata('message', "<div style='color:#ff0000;'>" . $this->upload->display_errors() . "</div>");
			redirect(site_url('pengelola/produk'));
		} else {
			$file = $this->upload->data();
			$dataProduk = array(
					'Barcode' => $this->input->post('barcode'),
					'SKU' => $this->input->post('sku'),
					'Nama_barang' => $this->input->post('nama_barang'),
					'Gambar_barang' => $file['file_name'],
					// $hargaBarang => $this->input->post('harga'),
					$cutHarga => preg_replace('/[^a-zA-Z0-9-_\.]/','', $this->input->post('harga')),
					// $hargaInt => (int) $cutHarga,
					'Harga_barang' => (int) $cutHarga,
					'Satuan_barang' => $this->input->post('satuan'),
					'ID_pengelola' => $this->session->userdata("id_pen"),
					'Kode_kategori' => $this->input->post('kategori')
			);

			$this->Model_Produk->input_barang($dataProduk);
		}	
		$this->session->set_flashdata('message', '
			<div class="alert alert-block alert-success"></i><a  class="close" data-dismiss="alert" aria-label="close">&times;</a></button>
			<i class="ace-icon fa fa-bullhorn green"></i> Produk berhasil ditambahkan
			</div>');
		redirect(site_url('pengelola/produk'));
	}

	public function update_produk()
	{
		$data = $this->Model_Kategori->getAllKategori();
		$this->session->set_userdata('all_data', $data);
		$query['dataProduk'] = $this->Model_Produk->getProduk($_GET['BARCODE']);
		$this->load->view('pengelola/produk/Update_Produk',$query);
	}

	public function proses_updateBarang()
	{
		$Barcode = $this->input->post('barcode');
		$SKU = $this->input->post('sku');
		$Nama_barang = $this->input->post('nama_barang');
		$Harga = $this->input->post('harga');
		$Satuan = $this->input->post('satuan');
		$Kategori = $this->input->post('kategori');
		$id_pengelola = $this->session->userdata("id_pen");

		$this->Model_Produk->edit_barang($Barcode, $SKU, $Nama_barang, $Harga, $Satuan, $Kategori, $id_pengelola);
		$this->session->set_flashdata('message', '
			<div class="alert alert-block alert-success"></i><a  class="close" data-dismiss="alert" aria-label="close">&times;</a></button>
			<i class="ace-icon fa fa-bullhorn green"></i> Produk berhasil diubah
			</div>');
		redirect('pengelola/produk');
	}

	public function hapus_produk()
	 {
		 	 $Barcode = $_GET['barcode'];
			 $this->Model_Produk->hapus_produk($Barcode);
			 $this->session->set_flashdata('message', '
			<div class="alert alert-block alert-success"></i><a  class="close" data-dismiss="alert" aria-label="close">&times;</a></button>
			<i class="ace-icon fa fa-bullhorn green"></i> Produk berhasil dihapus
			</div>');
			 redirect('pengelola/produk');

	 }

	 public function detail_produk()
	{
		$query['dataProduk'] = $this->Model_Produk->getDetailProduk($_GET['Barcode']);
		$gambar['img'] = $this->Model_Produk->getimg($_GET['Barcode']);
		$data = array_merge($query, $gambar);
		$this->load->view('pengelola/produk/detail_produk', $data);
	}

}

