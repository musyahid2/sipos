<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class logout extends CI_Controller {

	public function kasir(){
		$this->session->sess_destroy();
		redirect(base_url('kasir/auth/login'));
	
	}
	public function pengelola(){
		$this->session->sess_destroy();
		redirect(base_url('pengelola/auth/login'));
	}

}

/* End of file controllername.php */
/* Location: ./application/controllers/controllername.php */