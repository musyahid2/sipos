-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 22 Okt 2019 pada 15.01
-- Versi server: 10.4.6-MariaDB
-- Versi PHP: 7.2.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sipos_dummy`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `account`
--

CREATE TABLE `account` (
  `id` int(11) NOT NULL,
  `year` year(4) DEFAULT NULL,
  `purchase` int(11) DEFAULT NULL,
  `sale` int(11) DEFAULT NULL,
  `profit` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `account`
--

INSERT INTO `account` (`id`, `year`, `purchase`, `sale`, `profit`) VALUES
(1, 2013, 2000, 3000, 1000),
(2, 2014, 4500, 5000, 500),
(3, 2015, 3000, 4500, 1500),
(4, 2016, 2000, 3000, 1000),
(5, 2017, 2000, 4000, 2000),
(6, 2018, 2200, 3000, 800),
(7, 2019, 5000, 7000, 2000),
(8, 2020, 9000, 1000, 1000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang`
--

CREATE TABLE `barang` (
  `Barcode` varchar(25) NOT NULL,
  `SKU` varchar(20) NOT NULL,
  `Nama_barang` varchar(50) NOT NULL,
  `Gambar_barang` text NOT NULL,
  `Harga_barang` int(8) NOT NULL,
  `Satuan_barang` varchar(25) NOT NULL,
  `ID_pengelola` varchar(5) NOT NULL,
  `Kode_kategori` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `barang`
--

INSERT INTO `barang` (`Barcode`, `SKU`, `Nama_barang`, `Gambar_barang`, `Harga_barang`, `Satuan_barang`, `ID_pengelola`, `Kode_kategori`) VALUES
('001', '001', 'Aneka Gorengan ', 'default.jpg', 2500, 'PCS', 'PEN01', '3'),
('002', '002', 'Cibay', 'default.jpg', 3500, 'PCS', 'PEN01', '3'),
('003', '003', 'Sosis Lumpia', 'default.jpg', 2500, 'PCS', 'PEN01', '3'),
('005', '005', 'Pisang Crispy', 'default.jpg', 3500, 'PCS', 'PEN01', '3'),
('006', '006', 'Pisang Aroma', 'default.jpg', 2500, 'PCS', 'PEN01', '3'),
('007', '007', 'Cilok Mr Omon', 'default.jpg', 6000, 'Porsi', 'PEN01', '3'),
('008', '008', 'Nasi-Mie Goreng ', 'default.jpg', 16000, 'Porsi', 'PEN01', '3'),
('009', '009', 'Nasi Lava', 'default.jpg', 8000, 'Porsi', 'PEN01', '3'),
('010', '010', 'Onigiri Sunda', 'default.jpg', 7000, 'Porsi', 'PEN01', '3'),
('011', '011', 'Dimsum ', 'default.jpg', 6000, 'Porsi', 'PEN01', '3'),
('012', '012', 'Roti Sandwich Syifa', 'default.jpg', 3000, 'PCS', 'PEN01', '3'),
('014', '014', 'Nasi Kuning', 'default.jpg', 9000, 'PCS', 'PEN01', '3'),
('015', '015', 'Es Lilin Junior', 'default.jpg', 5500, 'PCS', 'PEN01', '6'),
('016', '016', 'Sushi ', 'default.jpg', 6000, 'Porsi', 'PEN01', '3'),
('017', '017', 'Donat', 'default.jpg', 3500, 'PCS', 'PEN01', '3'),
('018', '018', 'Kue Pia ', 'default.jpg', 2000, 'PCS', 'PEN01', '3'),
('019', '019', 'Roti Rasa', 'default.jpg', 6500, 'PCS', 'PEN01', '3'),
('020', '020', 'Sosis Solo', 'default.jpg', 2500, 'PCS', 'PEN01', '3'),
('021', '021', 'Minuman Thai Tea', 'default.jpg', 10000, 'Botol', 'PEN01', '1'),
('022', '022', 'Oreo Cheese Cake', 'default.jpg', 17500, 'PCS', 'PEN01', '6'),
('023', '023', 'Pie Brownies', 'default.jpg', 5000, 'PCS', 'PEN01', '3'),
('024', '024', 'Makaroni Cheese', 'default.jpg', 5000, 'Porsi', 'PEN01', '3'),
('025', '025', 'Kentang Roll', 'default.jpg', 5000, 'PCS', 'PEN01', '3'),
('089686590036', '0', 'Chiki Balls Keju 10G', '089686590036.jpg', 2000, 'Gram', 'PEN01', '5'),
('089686596427', '0', 'Lays Rumput Laut 14G', '089686596427.png', 2000, 'Gram', 'PEN01', '5'),
('089686598018', '0', 'Chitato Sapi Panggang 15G', '089686598018.jpg', 2500, 'Gram', 'PEN01', '5'),
('111', '111', 'Paseo Facial Tissue', 'default.jpg', 3000, 'PCS', 'PEN01', '9'),
('112', '112', 'Choice Tissue', 'default.jpg', 1500, 'PCS', 'PEN01', '9'),
('113', '113', 'Hansaplast', 'default.jpg', 2500, 'PCS', 'PEN01', '9'),
('114', '114', 'Kenko Sharpener', 'default.jpg', 3500, 'PCS', 'PEN01', '7'),
('115', '115', 'Pronto Mark N Notes', 'default.jpg', 15000, 'PCS', 'PEN01', '7'),
('116', '116', 'Choki Choki Cashew', '116.jpg', 1000, 'Gram', 'PEN01', '4'),
('117', '117', 'Rugby Umbrella Chocolate', 'default.jpg', 2000, 'Gram', 'PEN01', '4'),
('118', '118', 'Emble Patch ', 'default.jpg', 10000, 'PCS', 'PEN01', '8'),
('119', '119', 'Gelang Telkom', 'default.jpg', 15000, 'PCS', 'PEN01', '8'),
('120', '120', 'Sandal Wanita ', 'default.jpg', 25000, 'PCS', 'PEN01', '8'),
('121', '121', 'Sandal Wudhu', 'default.jpg', 18000, 'PCS', 'PEN01', '8'),
('122', '122', 'Stiker Tel-U 1', 'default.jpg', 5000, 'PCS', 'PEN01', '8'),
('123', '123', 'Stiker Tel-U 2', 'default.jpg', 9000, 'PCS', 'PEN01', '8'),
('124', '124', 'Stiker Tel -U 3', 'default.jpg', 5000, 'PCS', 'PEN01', '8'),
('125', '125', 'Stiker Tel-U 4', 'default.jpg', 5000, 'PCS', 'PEN01', '8'),
('126', '126', 'Stiker Tel-U 5', 'default.jpg', 11000, 'PCS', 'PEN01', '8'),
('127', '127', 'Stiker Tel-U 6', 'default.jpg', 9000, 'PCS', 'PEN01', '8'),
('128', '128', 'Gantungan Tel-U 1', 'default.jpg', 6000, 'PCS', 'PEN01', '8'),
('129', '129', 'Gantungan Tel-U 2', 'default.jpg', 9500, 'PCS', 'PEN01', '8'),
('130', '130', 'Gantungan Tel-U 3', 'default.jpg', 5000, 'PCS', 'PEN01', '8'),
('131', '131', 'Gantungan Tel-U 4', 'default.jpg', 5000, 'PCS', 'PEN01', '8'),
('132', '132', 'Gantungan Tel-U 5', 'default.jpg', 9000, 'PCS', 'PEN01', '8'),
('133', '133', 'Pin Tel-U', 'default.jpg', 5000, 'PCS', 'PEN01', '8'),
('134', '134', 'Pulpen Tel-U', 'default.jpg', 7000, 'PCS', 'PEN01', '8'),
('135', '135', 'Payung Tel-U', 'default.jpg', 45000, 'PCS', 'PEN01', '8'),
('136', '136', 'Card Holder Tel-U', 'default.jpg', 25000, 'PCS', 'PEN01', '8'),
('137', '137', 'Tas Spun Tel-U', 'default.jpg', 7000, 'PCS', 'PEN01', '8'),
('138', '138', 'Kaos Tel-U', 'default.jpg', 50000, 'PCS', 'PEN01', '8'),
('139', '139', 'Mug Tel-U', 'default.jpg', 18000, 'PCS', 'PEN01', '8'),
('200', '200', 'Bihun Krenyes ', 'default.jpg', 10000, 'PCS', 'PEN01', '10'),
('201', '201', 'Pikda Katazy', 'default.jpg', 6500, 'PCS', 'PEN01', '10'),
('202', '202', 'Pikdas Ma\'acah', 'default.jpg', 5000, 'PCS', 'PEN01', '10'),
('203', '203', 'Telur Gabus Manis&Asin', 'default.jpg', 15000, 'PCS', 'PEN01', '10'),
('204', '204', 'Sistik Bawang ', 'default.jpg', 6000, 'PCS', 'PEN01', '10'),
('205', '205', 'Wara Wiri Seblak ', 'default.jpg', 10000, 'PCS', 'PEN01', '10'),
('206', '206', 'Keripik Cokelat', 'default.jpg', 10000, 'PCS', 'PEN01', '10'),
('207', '207', 'Makaroni-Basreng Rasa', 'default.jpg', 6000, 'PCS', 'PEN01', '10'),
('208', '208', 'Rayan Makaroni', 'default.jpg', 5000, 'PCS', 'PEN01', '10'),
('209', '209', 'Rayan Keripik Kaca', 'default.jpg', 5000, 'PCS', 'PEN01', '10'),
('210', '210', 'Rayan Basreng', 'default.jpg', 6000, 'PCS', 'PEN01', '10'),
('211', '211', 'Rayan Solondok', 'default.jpg', 7000, 'PCS', 'PEN01', '10'),
('212', '212', 'Rayan Keripik Pedas', 'default.jpg', 6000, 'PCS', 'PEN01', '10'),
('213', '213', 'Rayan Mie Lidi', 'default.jpg', 6000, 'PCS', 'PEN01', '10'),
('214', '214', 'Bandrek Cup', 'default.jpg', 7000, 'PCS', 'PEN01', '1'),
('215', '215', 'Bandrek Sachet', 'default.jpg', 5000, 'PCS', 'PEN01', '1'),
('216', '216', 'Rayan Kerupuk Bunga', 'default.jpg', 5000, 'PCS', 'PEN01', '10'),
('217', '217', 'Cimol Kering', 'default.jpg', 1500, 'PCS', 'PEN01', '10'),
('218', '218', 'Selpi ( Sale Pisang )', 'default.jpg', 15000, 'PCS', 'PEN01', '10'),
('219', '219', 'Seblak Kering Sheva', 'default.jpg', 6000, 'PCS', 'PEN01', '10'),
('220', '220', 'Pangsit Pedas', 'default.jpg', 7500, 'PCS', 'PEN01', '10'),
('4006381333627', '0', 'Stabilo Boss Kuning', 'default.jpg', 10000, 'PCS', 'PEN01', '7'),
('4006381333641', '0', 'Stabilo Boss Hijau', 'default.jpg', 10000, 'PCS', 'PEN01', '7'),
('4006381333672', '0', 'Stabilo Boss Orange', 'default.jpg', 10000, 'PCS', 'PEN01', '7'),
('4006381333689', '0', 'Stabilo Boss Pink', 'default.jpg', 10000, 'PCS', 'PEN01', '7'),
('4893049120046', '0', 'Ritz Sandwich Cheese', 'default.jpg', 2500, 'Gram', 'PEN01', '2'),
('6914973600362', '0', 'Snickers 51G', 'default.jpg', 10000, 'Gram', 'PEN01', '4'),
('6934335009152', '0', 'Toffe Candy', 'default.jpg', 2000, 'Gram', 'PEN01', '4'),
('7622210515278', '0', 'Oreo Mini Chocolate', '7622210515278.png', 2500, 'Gram', 'PEN01', '2'),
('7622210515285', '0', 'Oreo Mini Original', '7622210515285.jpg', 2500, 'Gram', 'PEN01', '2'),
('7622210783721', '0', 'Belvita Milk Sereal', 'default.jpg', 2000, 'Gram', 'PEN01', '2'),
('7622210946546', '0', 'Biskuat Sandwich', 'default.jpg', 2000, 'Gram', 'PEN01', '2'),
('7622300136048', '0', 'Oreo Creme Strawberry', 'default.jpg', 2000, 'Gram', 'PEN01', '2'),
('7622300335809', '0', 'Oreo Softcake', '7622300335809.png', 2000, 'Gram', 'PEN01', '2'),
('7622300405588', '0', 'Kejucake', 'default.jpg', 2000, 'Gram', 'PEN01', '2'),
('7622300442477', '0', 'Oreo creme Ice Cream', '7622300442477.jpg', 2000, 'Gram', 'PEN01', '2'),
('8850389108048', '0', 'Mogu Mogu Mangga', 'default.jpg', 9000, 'Botol', 'PEN01', '1'),
('8850389108055', '0', 'Mogu Mogu Stroberi', 'default.jpg', 9000, 'Botol', 'PEN01', '1'),
('8850389108062', '0', 'Mogu Mogu Leci', 'default.jpg', 9000, 'Botol', 'PEN01', '1'),
('8850389108277', '0', 'Mogu Mogu Kelapa', 'default.jpg', 9000, 'Botol', 'PEN01', '1'),
('8850389108314', '0', 'Mogu Mogu Anggur', 'default.jpg', 9000, 'Botol', 'PEN01', '1'),
('8851019210117', '0', 'Glico Pocky Cokelat', 'default.jpg', 8500, 'Gram', 'PEN01', '2'),
('8851019210223', '0', 'Glico Pocky Matcha', 'default.jpg', 8500, 'Gram', 'PEN01', '2'),
('8886001038011', '0', 'Beng Beng Regular ', '8886001038011.jpg', 2000, 'Gram', 'PEN01', '4'),
('8886008101053', '0', 'Aqua Air Mineral 600ML', '8886008101053.jpg', 4000, 'Botol', 'PEN01', '1'),
('8886008101336', '0', 'Aqua Air Mineral 330ML', '8886008101336.jpg', 2500, 'Botol', 'PEN01', '1'),
('8886013700104', '0', 'Mie Gemez Enak', 'default.jpg', 2000, 'Gram', 'PEN01', '5'),
('8888166337992', '0', 'Nissin Mini Stick', '8888166337992.jpg', 2000, 'Gram', 'PEN01', '2'),
('8888166989634', '0', 'Serena Monde Snack', 'default.jpg', 6000, 'Gram', 'PEN01', '5'),
('8990333162044', '0', 'Lotte Xylitol Jeruk Nipis', 'default.jpg', 4500, 'Gram', 'PEN01', '4'),
('8990333162051', '0', 'Lotte Xylitol Fresh Mint', 'default.jpg', 4500, 'Gram', 'PEN01', '4'),
('8990800021850', '0', 'Mentos Roll Fruit Kecil', 'default.jpg', 3000, 'Gram', 'PEN01', '4'),
('8990800100012', '0', 'Mentos Mint', '8990800100012.jpg', 4500, 'Gram', 'PEN01', '4'),
('8990800100050', '0', 'Mentos Fruit', '8990800100050.jpg', 4500, 'Gram', 'PEN01', '4'),
('8991001501011', '0', 'Delfi Chic Choc', 'default.jpg', 9000, 'Gram', 'PEN01', '4'),
('8991001780133', '0', 'Twister Black  45G', 'default.jpg', 6500, 'Gram', 'PEN01', '2'),
('8991002121003', '0', 'Good Day Tiramisu 250ML', 'default.jpg', 8000, 'PCS', 'PEN01', '1'),
('8991002121034', '0', 'Good Day Mocca Latte 200ML', 'default.jpg', 6000, 'Lainnya', 'PEN01', '1'),
('8991002121058', '0', 'Good Day White Vanila 200ML', 'default.jpg', 6000, 'Lainnya', 'PEN01', '1'),
('8991002121065', '0', 'Good Day Avocado 250Ml', 'default.jpg', 8000, 'Botol', 'PEN01', '1'),
('8991002121089', '0', 'Good Day Cappucino 250ML', 'default.jpg', 8000, 'Botol', 'PEN01', '1'),
('8991102387262', '0', 'Tango Wafer Vanila 47G', '8991102387262.png', 3000, 'Gram', 'PEN01', '2'),
('8991102387286', '0', 'Tango Wafer Cokelat 47G', '8991102387286.png', 3000, 'Gram', 'PEN01', '2'),
('8991102900607', '0', 'Kiranti Orange', 'default.jpg', 7500, 'Botol', 'PEN01', '1'),
('8991115010102', '0', 'Big Babol Tutti Frutti', '8991115010102.jpg', 3000, 'Gram', 'PEN01', '4'),
('8991115011109', '0', 'Big Babol Blueberry', '8991115011109.jpg', 3000, 'Gram', 'PEN01', '4'),
('8991115012106', '0', 'Big Babol Stroberi Krim', '8991115012106.jpg', 3000, 'Gram', 'PEN01', '4'),
('8992388101085', '0', 'Nu Greentea Original 330', '8992388101085.jpg', 5000, 'Botol', 'PEN01', '1'),
('8992388101092', '0', 'Nu Greentea Honey 330', '8992388101092.jpg', 5000, 'Botol', 'PEN01', '1'),
('8992388133017', '0', 'Nu Milktea ', '8992388133017.jpg', 6500, 'Botol', 'PEN01', '1'),
('8992388133345', '0', 'Nu Teh Tarik', 'default.jpg', 6500, 'PCS', 'PEN01', '1'),
('8992388133604', '0', 'Nu Oceana Lemonade 330', 'default.jpg', 6000, 'Botol', 'PEN01', '1'),
('8992696404441', '0', 'Bear Brand Milk', 'default.jpg', 9500, 'Lainnya', 'PEN01', '1'),
('8992696419742', '0', 'Nescafe Coffe Cream 200ML', 'default.jpg', 6000, 'Lainnya', 'PEN01', '1'),
('8992696422650', '0', 'Nescafe French Vanila 200ML', 'default.jpg', 6000, 'Lainnya', 'PEN01', '1'),
('8992696425316', '0', 'Nescafe Black 200ML', 'default.jpg', 6000, 'Lainnya', 'PEN01', '1'),
('8992696428812', '0', 'Nescafe White Coffe 200ML', 'default.jpg', 6000, 'Lainnya', 'PEN01', '1'),
('8992696430266', '0', 'Milo Activ-Go 190ML', 'default.jpg', 6000, 'Lainnya', 'PEN01', '1'),
('8992696520141', '0', 'Polo Peppermint', '8992696520141.jpg', 3000, 'Gram', 'PEN01', '4'),
('8992716109172', '0', 'Biskuat Bolu Coklat', 'default.jpg', 2000, 'Gram', 'PEN01', '2'),
('8992716109462', '0', 'Biskuat Bolu Pandan', 'default.jpg', 2000, 'Gram', 'PEN01', '2'),
('8992727004169', '0', 'Laurier Maxi Wing', 'default.jpg', 1500, 'PCS', 'PEN01', '9'),
('8992727004480', '0', 'Laurier Maxi Non Wing', 'default.jpg', 1000, 'PCS', 'PEN01', '9'),
('8992741904315', '0', 'Yupi Gummy Farnk', 'default.jpg', 5000, 'Gram', 'PEN01', '4'),
('8992741905183', '0', 'Yupi O\'ringo', 'default.jpg', 5000, 'Gram', 'PEN01', '4'),
('8992741905602', '0', 'Yupi Burger', 'default.jpg', 5000, 'Gram', 'PEN01', '4'),
('8992741950206', '0', 'Yupi Apple Rings', 'default.jpg', 5000, 'Gram', 'PEN01', '4'),
('8992752011033', '0', 'Vit Air Mineral 600ML', '8992752011033.png', 3000, 'Botol', 'PEN01', '1'),
('8992760121014', '0', 'Oreo Creme Original', '8992760121014.jpg', 2000, 'Gram', 'PEN01', '2'),
('8992760121083', '0', 'Oreo Cream Peanut Butter', 'default.jpg', 2000, 'Gram', 'PEN01', '2'),
('8992760223022', '0', 'Oreo  Creme Chocolate', '8992760223022.jpg', 2000, 'Gram', 'PEN01', '2'),
('8992761002015', '0', 'Coca Cola 390ML', '8992761002015.jpg', 5000, 'Botol', 'PEN01', '1'),
('8992761002022', '0', 'Sprite 390ML', '8992761002022.png', 5000, 'Botol', 'PEN01', '1'),
('8992761002039', '0', 'Fanta Stroberi 390ML', '8992761002039.jpg', 5000, 'Botol', 'PEN01', '1'),
('8992761002046', '0', 'Fanta Orange 390ML', '8992761002046.jpg', 5000, 'Botol', 'PEN01', '1'),
('8992761002053', '0', 'Fanta Gape 390ML', '8992761002053.jpg', 5000, 'Botol', 'PEN01', '1'),
('8992761122317', '0', 'Frestea Greentea 500ML', 'default.jpg', 7000, 'Botol', 'PEN01', '1'),
('8992761122324', '0', 'Frestea Original 500ML', 'default.jpg', 7000, 'Botol', 'PEN01', '1'),
('8992761122331', '0', 'Frestea Apel 500ML', 'default.jpg', 7000, 'Botol', 'PEN01', '1'),
('8992761122348', '0', 'Frestea Markisa 500ML', 'default.jpg', 7000, 'Botol', 'PEN01', '1'),
('8992761122430', '0', 'Frestea Madu 500ML', 'default.jpg', 7000, 'Botol', 'PEN01', '1'),
('8992761122539', '0', 'Frestea Lychee 500ML', 'default.jpg', 7000, 'Botol', 'PEN01', '1'),
('8992761139018', '0', 'Ades ', '8992761139018.png', 3000, 'Botol', 'PEN01', '1'),
('8992761145019', '0', 'Coca Cola Mini 250ML', '8992761145019.jpg', 3500, 'Botol', 'PEN01', '1'),
('8992761145026', '0', 'Sprite Mini 250ML', '8992761145026.png', 3500, 'Botol', 'PEN01', '1'),
('8992761145033', '0', 'Fanta Stroberi Mini 250ML', '8992761145033.jpg', 3500, 'Botol', 'PEN01', '1'),
('8992761145071', '0', 'Fanta Orange Mini 250ML', '8992761145071.jpg', 3500, 'Botol', 'PEN01', '1'),
('8992761147143', '0', 'Sprite Watermelon', 'default.jpg', 6000, 'Botol', 'PEN01', '1'),
('8992761151089', '0', 'Nutriforce Mangga', 'default.jpg', 2500, 'Lainnya', 'PEN01', '1'),
('8992761151102', '0', 'Nutriboost Stroberi 180ML', 'default.jpg', 5000, 'Lainnya', 'PEN01', '1'),
('8992761151119', '0', 'Nutriboost Jeruk 180ML', 'default.jpg', 5000, 'Lainnya', 'PEN01', '1'),
('8992761151140', '0', 'Nutriboost Cokelat 180ML', 'default.jpg', 5000, 'Lainnya', 'PEN01', '1'),
('8992761151157', '0', 'Nutriboost Kopi 180ML', 'default.jpg', 5000, 'Lainnya', 'PEN01', '1'),
('8992761164492', '0', 'Nutriboost Cokelat Botol', 'default.jpg', 7000, 'Botol', 'PEN01', '1'),
('8992761164508', '0', 'Nutriboost Kopi Botol', 'default.jpg', 7000, 'Botol', 'PEN01', '1'),
('8992761164539', '0', 'Nutriboost Botol Jeruk 300ML', 'default.jpg', 7000, 'Botol', 'PEN01', '1'),
('8992761164546', '0', 'Nutriboost Botol Stroberi 300ML', 'default.jpg', 7000, 'Botol', 'PEN01', '1'),
('8992761164560', '0', 'Nutriboost Botol Apel 300ML', 'default.jpg', 7000, 'Botol', 'PEN01', '1'),
('8992761164577', '0', 'Nutriboost Botol Mangga 300ML', 'default.jpg', 7000, 'Botol', 'PEN01', '1'),
('8992761164591', '0', 'Minutemaid Homestyle Guava', 'default.jpg', 6000, 'Botol', 'PEN01', '1'),
('8992761166038', '0', 'Minutemaid Pulpy Orange', '8992761166038.jpg', 5500, 'Botol', 'PEN01', '1'),
('8992761166052', '0', 'Frestea Original 330ML', 'default.jpg', 5000, 'PCS', 'PEN01', '1'),
('8992761166199', '0', 'Minutemaid Pulpy Aloevera', '8992761166199.jpg', 5500, 'Botol', 'PEN01', '1'),
('8992761166205', '0', 'Frestea Madu 330ML', 'default.jpg', 5000, 'Botol', 'PEN01', '1'),
('8992761166212', '0', 'Frestea Apel 330ML', 'default.jpg', 5000, 'Botol', 'PEN01', '1'),
('8992761166229', '0', 'Frestea Markisa Mini 350ML', 'default.jpg', 5000, 'Botol', 'PEN01', '1'),
('8992761166236', '0', 'Frestea Lychee 330ML', 'default.jpg', 5000, 'Botol', 'PEN01', '1'),
('8992772485012', '0', 'Coolant 350ML', 'default.jpg', 6500, 'Botol', 'PEN01', '1'),
('8992772585026', '0', 'Ademsari Chingku Botol', 'default.jpg', 7000, 'Botol', 'PEN01', '1'),
('8992772586016', '0', 'AdemSari Chingku Can', 'default.jpg', 6500, 'Lainnya', 'PEN01', '1'),
('8992775000274', '0', 'Chocolatos Roll Cheese 33G', 'default.jpg', 3000, 'Gram', 'PEN01', '2'),
('8992775311479', '0', 'Chocolatos Drank Roll', 'default.jpg', 1500, 'Gram', 'PEN01', '2'),
('8992775315095', '0', 'Gery Malkist Cheese', 'default.jpg', 8500, 'Gram', 'PEN01', '2'),
('8992802063012', '0', 'Fitbar Fruits', 'default.jpg', 6000, 'Gram', 'PEN01', '2'),
('8992802063029', '0', 'Fitbar Nuts', 'default.jpg', 6000, 'Gram', 'PEN01', '2'),
('8992802063173', '0', 'Fitbar Chocalate', 'default.jpg', 6000, 'Gram', 'PEN01', '2'),
('8992858527308', '0', 'Hydro Coco 250ML', 'default.jpg', 7500, 'Lainnya', 'PEN01', '1'),
('8992919856019', '0', 'Sugus Blackcurrant', '8992919856019.jpg', 3500, 'Gram', 'PEN01', '4'),
('8992919856026', '0', 'Sugus Orange', '8992919856026.jpg', 3500, 'Gram', 'PEN01', '4'),
('8992919856033', '0', 'Sugus Strawberry', '8992919856033.jpg', 3500, 'Gram', 'PEN01', '4'),
('8992931005099', '0', 'Tessa Tissue 150S', 'default.jpg', 6500, 'PCS', 'PEN01', '9'),
('8993004785139', '0', 'Smax Ring Cheese', 'default.jpg', 1500, 'Gram', 'PEN01', '5'),
('8993004785160', '0', 'Smax Ring Keju 50G', 'default.jpg', 5000, 'Gram', 'PEN01', '5'),
('8993154789841', '0', 'Della Cha Cokelat Crispy', 'default.jpg', 3000, 'Gram', 'PEN01', '4'),
('8993175533232', '0', 'Richeese Pasta Keju', 'default.jpg', 3500, 'Gram', 'PEN01', '4'),
('8993175535878', '0', 'Nabati Wafer Keju ', '8993175535878.png', 3000, 'Gram', 'PEN01', '2'),
('8993175535885', '0', 'Nabati Wafer Cokelat', '8993175535885.png', 3000, 'Gram', 'PEN01', '2'),
('8993175538824', '0', 'Nabati Siip Keju ', '8993175538824.png', 5500, 'Gram', 'PEN01', '5'),
('8993175538862', '0', 'Nabati Siip Cokelat', '8993175538862.png', 5500, 'Gram', 'PEN01', '5'),
('8993988050254', '0', 'Joyko Correction Pen', 'default.jpg', 7000, 'PCS', 'PEN01', '7'),
('8994171102101', '0', 'Luwak White Koffie', 'default.jpg', 6000, 'Botol', 'PEN01', '1'),
('8994191104611', '0', 'Smile Bottle Cokelat', 'default.jpg', 3000, 'Gram', 'PEN01', '4'),
('8995077603839', '0', 'Deka Crepes Banana 14G', 'default.jpg', 1500, 'Gram', 'PEN01', '2'),
('8996001318430', '0', 'Better Sandwich', '8996001318430.jpg', 2000, 'Gram', 'PEN01', '2'),
('8996001355046', '0', 'Roma Superstar', '8996001355046.png', 1500, 'Gram', 'PEN01', '2'),
('8996001600146', '0', 'Teh Pucuk Harum Melati', '8996001600146.jpg', 4000, 'Botol', 'PEN01', '1'),
('8996001600221', '0', 'Kopiko 78C Coffe Latte', 'default.jpg', 6000, 'Botol', 'PEN01', '1'),
('8996001600306', '0', 'Kopiko 78C Mocharetta', 'default.jpg', 6000, 'Botol', 'PEN01', '1'),
('8996001600313', '0', 'Kopiko 78C Caramel Frappe', 'default.jpg', 6000, 'Botol', 'PEN01', '1'),
('8996006142511', '0', 'Sosro Teh Botol 250ML', 'default.jpg', 3500, 'Lainnya', 'PEN01', '1'),
('8996006852045', '0', 'Prima Air Mineral 600ML', 'default.jpg', 3500, 'Botol', 'PEN01', '1'),
('8996006853127', '0', 'Sosro Tebs Sparkling Can', 'default.jpg', 6500, 'Lainnya', 'PEN01', '1'),
('8996006855879', '0', 'Fruit Tea Blackcrunt ', 'default.jpg', 3000, 'PCS', 'PEN01', '1'),
('8996006855886', '0', 'Sosro Fruittea Apel ', '8996006855886.png', 3000, 'Lainnya', 'PEN01', '1'),
('8996006856715', '0', 'Sosro Tebs Botol ', 'default.jpg', 7000, 'Botol', 'PEN01', '1'),
('8997009490029', '0', 'Big Boss peppermint', 'default.jpg', 2000, 'Gram', 'PEN01', '4'),
('8997009510017', '0', 'You C1000 Lemon', '8997009510017.jpg', 7000, 'Botol', 'PEN01', '1'),
('8997035563414', '0', 'Pocari Sweat 500ML', 'default.jpg', 7500, 'Botol', 'PEN01', '1'),
('8997035563544', '0', 'Pocari Sweat 330ML', 'default.jpg', 6500, 'Botol', 'PEN01', '1'),
('8998009010224', '0', 'Ultra Milk Plain 250ML', '8998009010224.png', 6500, 'Lainnya', 'PEN01', '1'),
('8998009010231', '0', 'Ultra Milk Cokelat 250ML', '8998009010231.png', 6000, 'Lainnya', 'PEN01', '1'),
('8998009010248', '0', 'Ultra Milk Stroberi 250ML ', 'default.jpg', 6000, 'Lainnya', 'PEN01', '1'),
('8998009010255', '0', 'Ultra Milk Moka 250ML', '8998009010255.png', 6000, 'Lainnya', 'PEN01', '1'),
('8998009010262', '8998009010262', 'Ultra Lowfat Plain 250ML', '8998009010262.png', 6500, 'Lainnya', 'PEN01', '1'),
('8998009010569', '0', 'Ultra Milk Cokelat 200ML', 'default.jpg', 5000, 'Lainnya', 'PEN01', '1'),
('8998009010576', '0', 'Ultra Milk Stroberi 200ML', 'default.jpg', 5000, 'PCS', 'PEN01', '1'),
('8998009011214', '0', 'Ultra Lowfat Cokelat 250ML', '8998009011214.png', 6500, 'Lainnya', 'PEN01', '1'),
('8998009011702', '0', 'Ultra Milk Taro 200ML', '8998009011702.png', 5000, 'Lainnya', 'PEN01', '1'),
('8998009011740', '0', 'Ultra Milk Karamel 200ML', '8998009011740.png', 5000, 'Lainnya', 'PEN01', '1'),
('8998009020186', '0', 'Buavita Guava', 'default.jpg', 7500, 'Lainnya', 'PEN01', '1'),
('8998009020193', '0', 'Buavita Manggo', 'default.jpg', 7500, 'Lainnya', 'PEN01', '1'),
('8998009030031', '0', ' Sari Asem ', 'default.jpg', 6000, 'Lainnya', 'PEN01', '1'),
('8998009040023', '0', 'Teh Kotak Melati 200ML', '8998009040023.png', 4000, 'Lainnya', 'PEN01', '1'),
('8998009040313', '0', 'Teh Kotak Lemon 200ML', '8998009040313.png', 4000, 'Lainnya', 'PEN01', '1'),
('8998009050053', '0', 'Sari Kacang Ijo 250 ML', 'default.jpg', 5000, 'Lainnya', 'PEN01', '1'),
('8998685011003', '0', 'Hexos Mint', '8998685011003.jpg', 2500, 'Gram', 'PEN01', '4'),
('8998685012000', '0', 'Hexos Lemon ', '8998685012000.jpg', 2500, 'Gram', 'PEN01', '4'),
('8998685021002', '0', 'Nano Nano Kulit Jeruk', 'default.jpg', 3000, 'Gram', 'PEN01', '4'),
('8998838290231', '0', 'kenko Eraser', 'default.jpg', 2000, 'PCS', 'PEN01', '7'),
('8998866820004', '0', 'Frostbite Cup Strawberry', 'default.jpg', 5000, 'PCS', 'PEN01', '6'),
('8998866820011', '0', 'frostbite Cup vanila', 'default.jpg', 5000, 'PCS', 'PEN01', '6'),
('8998866820028', '0', 'Frostbite Kacang Hijau', 'default.jpg', 5000, 'PCS', 'PEN01', '6'),
('8998866820035', '0', 'Frostbite Pari Vanila', 'default.jpg', 5000, 'PCS', 'PEN01', '6'),
('8998866820042', '0', 'Frostbite Vanila ChocoNut', 'default.jpg', 5000, 'PCS', 'PEN01', '6'),
('8998866820097', '0', 'Haku Matcha', 'default.jpg', 12000, 'PCS', 'PEN01', '6'),
('8998866820103', '0', 'Haku Monaka Vanila', 'default.jpg', 12000, 'PCS', 'PEN01', '6'),
('8998866820127', '0', 'Haku Classic', 'default.jpg', 12000, 'PCS', 'PEN01', '6'),
('8998866820134', '0', 'Jcone Junior Choco Vanila', 'default.jpg', 3000, 'PCS', 'PEN01', '6'),
('8998866820141', '0', 'Jcone Pari Vanila', 'default.jpg', 8500, 'PCS', 'PEN01', '6'),
('8998866820158', '0', 'Waku Waku Choco Bomb', 'default.jpg', 4000, 'PCS', 'PEN01', '6'),
('8998866820172', '0', 'Waku Waku Happy Soda', 'default.jpg', 4000, 'PCS', 'PEN01', '6'),
('8998866820189', '0', 'Waku Waku Strawberryloop', 'default.jpg', 2000, 'PCS', 'PEN01', '6'),
('8998866820196', '0', 'Waku Waku Watermelon', 'default.jpg', 4000, 'PCS', 'PEN01', '6'),
('8998866820219', '0', 'Waku Waku Manggoloop', 'default.jpg', 3000, 'PCS', 'PEN01', '6'),
('8998866820226', '0', 'Frostbite ChocoNut', 'default.jpg', 5000, 'PCS', 'PEN01', '6'),
('8998866820233', '0', 'Haku Monaka Choco', 'default.jpg', 12000, 'PCS', 'PEN01', '6'),
('8998866820257', '0', 'Waku Waku Grape', 'default.jpg', 4000, 'PCS', 'PEN01', '6'),
('8999999000059', '0', 'Taro Net Potato 9G', 'default.jpg', 1500, 'Gram', 'PEN01', '5'),
('95502489', '0', 'Kit Kat 2F Cokelat', 'default.jpg', 6000, 'Gram', 'PEN01', '4'),
('9555192501107', '0', 'Wrigley Doublemint Original', 'default.jpg', 3500, 'Gram', 'PEN01', '4'),
('9555192501206', '0', 'Wrigley Doublemint Spearmint', 'default.jpg', 3500, 'Gram', 'PEN01', '4'),
('9555192501305', '0', 'Wrigley Juicy Fruit', 'default.jpg', 3500, 'Gram', 'PEN01', '4'),
('9555192508694', '0', 'Wrigley Chewy Mints', 'default.jpg', 2500, 'PCS', 'PEN01', '4'),
('9555192508724', '0', 'Wrigley Chewy Lemon Mints', 'default.jpg', 2500, 'Gram', 'PEN01', '4'),
('9556001025272', '0', 'Nescafe Latte Can', 'default.jpg', 10000, 'Lainnya', 'PEN01', '1'),
('9556001029362', '0', 'Kit Kat Bites Cokelat', 'default.jpg', 13000, 'Gram', 'PEN01', '4'),
('9556001047175', '0', 'Nescafe Original Can', 'default.jpg', 10000, 'Lainnya', 'PEN01', '1'),
('9556001051509', '0', 'Milo Original Can', 'default.jpg', 10000, 'Lainnya', 'PEN01', '1'),
('9556001054005', '0', 'Nescafe Mocha Can', 'default.jpg', 10000, 'Lainnya', 'PEN01', '1'),
('9556001174659', '0', 'Milo Calcium Can', 'default.jpg', 10000, 'Lainnya', 'PEN01', '1'),
('9556001227560', '0', 'Kit Kat 2F Greentea', 'default.jpg', 6000, 'Gram', 'PEN01', '4'),
('9556001238733', '0', 'Kit Kat Bites Greentea', 'default.jpg', 13500, 'Gram', 'PEN01', '4');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_transaksi`
--

CREATE TABLE `detail_transaksi` (
  `Kode_transaksi` varchar(15) NOT NULL,
  `Barcode` varchar(25) NOT NULL,
  `Jumlah` int(4) NOT NULL,
  `Harga_satuan` int(100) NOT NULL,
  `Harga_total` int(100) NOT NULL,
  `ID_Kasir` varchar(5) NOT NULL,
  `tanggal_transaksi` date NOT NULL,
  `Waktu_Transaksi` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_transaksi`
--

INSERT INTO `detail_transaksi` (`Kode_transaksi`, `Barcode`, `Jumlah`, `Harga_satuan`, `Harga_total`, `ID_Kasir`, `tanggal_transaksi`, `Waktu_Transaksi`) VALUES
('0709190001TRX', '002', 1, 3500, 3500, 'EGN23', '2019-09-07', '16:05:20'),
('0709190001TRX', '003', 1, 2500, 2500, 'EGN23', '2019-09-07', '16:05:20'),
('0709190002TRX', '003', 1, 2500, 2500, 'EGN23', '2019-09-07', '16:10:15'),
('0709190003TRX', '001', 1, 2500, 2500, 'EGN23', '2019-09-07', '16:14:16'),
('0709190004TRX', '002', 3, 3500, 10500, 'EGN23', '2019-09-07', '16:32:07'),
('1309190001TRX', '002', 1, 3500, 3500, 'EGN23', '2019-09-13', '22:24:11'),
('1309190001TRX', '001', 1, 2500, 2500, 'EGN23', '2019-09-13', '22:24:11'),
('1309190001TRX', '003', 1, 2500, 2500, 'EGN23', '2019-09-13', '22:24:11'),
('1309190001TRX', '008', 1, 16000, 16000, 'EGN23', '2019-09-13', '22:24:12'),
('1309190001TRX', '006', 1, 2500, 2500, 'EGN23', '2019-09-13', '22:24:12'),
('1309190001TRX', '005', 1, 3500, 3500, 'EGN23', '2019-09-13', '22:24:12'),
('0710190001TRX', '001', 1, 2500, 2500, 'EGN23', '2019-10-07', '09:37:47'),
('0710190001TRX', '002', 2, 3500, 7000, 'EGN23', '2019-10-07', '09:37:47'),
('0710190002TRX', '8997035563414', 1, 7500, 7500, 'EGN23', '2019-10-07', '21:52:52'),
('0710190003TRX', '019', 1, 6500, 6500, 'EGN23', '2019-10-07', '21:53:38'),
('0710190005TRX', '123', 1, 9000, 9000, 'EGN23', '2019-10-07', '22:17:03'),
('0710190005TRX', '019', 3, 6500, 19500, 'EGN23', '2019-10-07', '22:17:03'),
('0710190005TRX', '7622300442477', 2, 9000, 18000, 'EGN23', '2019-10-07', '22:17:03'),
('0710190006TRX', '001', 1, 2500, 2500, 'EGN23', '2019-10-07', '22:20:34'),
('0710190006TRX', '002', 1, 3500, 3500, 'EGN23', '2019-10-07', '22:20:34'),
('0710190006TRX', '003', 2, 3500, 7000, 'EGN23', '2019-10-07', '22:20:34'),
('0710190007TRX', '001', 1, 2500, 2500, 'EGN23', '2019-10-07', '22:21:55'),
('0710190007TRX', '002', 1, 3500, 3500, 'EGN23', '2019-10-07', '22:21:55'),
('0710190008TRX', '123', 2, 9000, 18000, 'EGN23', '2019-10-07', '22:27:02'),
('0710190008TRX', '4006381333689', 2, 10000, 20000, 'EGN23', '2019-10-07', '22:27:02'),
('0810190001TRX', '123', 1, 9000, 9000, 'EGN23', '2019-10-08', '19:47:43'),
('0810190001TRX', '005', 1, 3500, 3500, 'EGN23', '2019-10-08', '19:47:44'),
('0810190001TRX', '003', 1, 2500, 2500, 'EGN23', '2019-10-08', '19:47:44');

-- --------------------------------------------------------

--
-- Struktur dari tabel `diskon`
--

CREATE TABLE `diskon` (
  `Kode_Diskon` varchar(5) NOT NULL,
  `Nama_Diskon` text NOT NULL,
  `Potongan_Harga` int(10) NOT NULL,
  `Persentase` int(3) NOT NULL,
  `Min_Transaksi` int(12) NOT NULL,
  `Max_Diskon` int(10) NOT NULL,
  `ID_pengelola` varchar(5) NOT NULL,
  `Status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `diskon`
--

INSERT INTO `diskon` (`Kode_Diskon`, `Nama_Diskon`, `Potongan_Harga`, `Persentase`, `Min_Transaksi`, `Max_Diskon`, `ID_pengelola`, `Status`) VALUES
('DSK0', '0', 0, 0, 0, 0, 'PEN01', 'Aktif'),
('DSK02', 'Dsik', 0, 4, 0, 0, 'PEN01', 'Selesai'),
('LBR01', 'Diskon Lebaran', 0, 5, 0, 0, 'PEN01', 'Aktif');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kasir`
--

CREATE TABLE `kasir` (
  `ID_kasir` varchar(5) NOT NULL,
  `PIN` varchar(100) NOT NULL,
  `Foto` text NOT NULL,
  `Nama_kasir` varchar(30) NOT NULL,
  `Jenis_kelamin` varchar(10) NOT NULL,
  `Nomor_telp` varchar(15) NOT NULL,
  `Tanggal_masuk` date NOT NULL,
  `Alamat` text NOT NULL,
  `Status` varchar(15) NOT NULL,
  `ID_pengelola` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kasir`
--

INSERT INTO `kasir` (`ID_kasir`, `PIN`, `Foto`, `Nama_kasir`, `Jenis_kelamin`, `Nomor_telp`, `Tanggal_masuk`, `Alamat`, `Status`, `ID_pengelola`) VALUES
('EGN23', '827ccb0eea8a706c4c34a16891f84e7b', 'default.jpg', 'egan', 'Laki-Laki', '083815649707', '2019-07-16', 'Asr. Brigif 1', 'Aktif', 'PEN01'),
('Yan01', '05623c9819b494421892868dbc03df67', 'default.jpg', 'Yanti', 'Perempuan', '083821704617', '2019-08-13', 'jl hoils ', 'Aktif', 'PEN01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE `kategori` (
  `Kode_kategori` varchar(10) NOT NULL,
  `Nama_kategori` varchar(30) NOT NULL,
  `ID_pengelola` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`Kode_kategori`, `Nama_kategori`, `ID_pengelola`) VALUES
('1', 'Beverage', 'PEN01'),
('10', 'Snack Konsinyasi', 'PEN01'),
('2', 'Biscuit', 'PEN01'),
('3', 'Breakfast', 'PEN01'),
('4', 'Confetionery', 'PEN01'),
('5', 'Snack', 'PEN01'),
('6', 'Frozen', 'PEN01'),
('7', 'Stationery', 'PEN01'),
('8', 'Gms', 'PEN01'),
('9', 'Napkins', 'PEN01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `metode_pembayaran`
--

CREATE TABLE `metode_pembayaran` (
  `Id_metodePembayaran` varchar(50) NOT NULL,
  `Nama` varchar(50) NOT NULL,
  `Jenis` varchar(50) NOT NULL,
  `Foto` varchar(50) NOT NULL,
  `Logo` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `metode_pembayaran`
--

INSERT INTO `metode_pembayaran` (`Id_metodePembayaran`, `Nama`, `Jenis`, `Foto`, `Logo`) VALUES
('DBT01', 'Debit', 'Debit', 'default.jpg', 'default.jpg'),
('ovo01', 'OVO', 'Wallet', 'default.jpg', 'default.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `oprkasir`
--

CREATE TABLE `oprkasir` (
  `ID_opr` int(11) NOT NULL,
  `Tanggal` date NOT NULL,
  `Jam_buka` time DEFAULT NULL,
  `Saldo_awal` bigint(10) NOT NULL,
  `Jam_tutup` time DEFAULT NULL,
  `Saldo_akhir` bigint(10) NOT NULL,
  `ID_kasir` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `oprkasir`
--

INSERT INTO `oprkasir` (`ID_opr`, `Tanggal`, `Jam_buka`, `Saldo_awal`, `Jam_tutup`, `Saldo_akhir`, `ID_kasir`) VALUES
(16, '2019-10-07', '09:37:34', 1000000, '22:38:49', 200000, 'EGN23'),
(17, '2019-10-07', '21:52:28', 1000000, '22:38:49', 200000, 'EGN23'),
(18, '2019-10-07', '22:13:44', 1000000, '22:38:49', 200000, 'EGN23');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembayaran`
--

CREATE TABLE `pembayaran` (
  `ID_pembayaran` varchar(15) NOT NULL,
  `Total_bayar` int(11) NOT NULL,
  `Tanggal_pembayaran` date NOT NULL,
  `Waktu_Pembayaran` time NOT NULL,
  `Kode_transaksi` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pembayaran`
--

INSERT INTO `pembayaran` (`ID_pembayaran`, `Total_bayar`, `Tanggal_pembayaran`, `Waktu_Pembayaran`, `Kode_transaksi`) VALUES
('0709190001PBY', 6000, '2019-09-07', '16:06:33', '0709190001TRX'),
('0709190002PBY', 4500, '2019-09-07', '16:11:30', '0709190002TRX'),
('0709190003PBY', 2500, '2019-09-07', '16:24:56', '0709190003TRX'),
('0709190004PBY', 10500, '2019-09-07', '16:32:19', '0709190004TRX'),
('0710190001PBY', 9500, '2019-10-07', '09:37:56', '0710190001TRX'),
('0710190002PBY', 7500, '2019-10-07', '21:53:01', '0710190002TRX'),
('0710190003PBY', 6500, '2019-10-07', '21:53:43', '0710190003TRX'),
('0710190004PBY', 10000, '2019-10-07', '21:54:07', '0710190004TRX'),
('0710190005PBY', 46500, '2019-10-07', '22:18:16', '0710190005TRX'),
('0710190006PBY', 13000, '2019-10-07', '22:20:43', '0710190006TRX'),
('0710190007PBY', 6000, '2019-10-07', '22:22:01', '0710190007TRX'),
('0710190008PBY', 38000, '2019-10-07', '22:29:25', '0710190008TRX'),
('0810190001PBY', 15000, '2019-10-08', '19:47:56', '0810190001TRX'),
('1309190001PBY', 32500, '2019-09-13', '22:24:39', '1309190001TRX');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembayaran_debit`
--

CREATE TABLE `pembayaran_debit` (
  `ID_pembayaran` varchar(15) NOT NULL,
  `Nomor_kartu` varchar(100) NOT NULL,
  `Nomor_transaksi` varchar(100) NOT NULL,
  `Kode_diskon` varchar(15) NOT NULL,
  `Total_tagihan` int(50) NOT NULL,
  `Nama_bank` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembayaran_tunai`
--

CREATE TABLE `pembayaran_tunai` (
  `ID_pembayaran` varchar(15) NOT NULL,
  `ID_tunai` int(100) NOT NULL,
  `Kode_diskon` varchar(15) NOT NULL,
  `Jumlah_pembayaran` int(100) NOT NULL,
  `Tunai` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pembayaran_tunai`
--

INSERT INTO `pembayaran_tunai` (`ID_pembayaran`, `ID_tunai`, `Kode_diskon`, `Jumlah_pembayaran`, `Tunai`) VALUES
('0709190001PBY', 830, 'DSK0', 6000, 9000),
('0709190002PBY', 355, 'DSK0', 4500, 10000),
('0709190003PBY', 293, 'DSK0', 2500, 5000),
('0709190004PBY', 500, 'DSK0', 10500, 11000),
('1309190001PBY', 502, 'DSK0', 32500, 40000),
('0710190001PBY', 901, 'DSK0', 9500, 10000),
('0710190002PBY', 559, 'DSK0', 7500, 10000),
('0710190003PBY', 272, 'DSK0', 6500, 10000),
('0710190004PBY', 649, 'DSK0', 10000, 20000),
('0710190005PBY', 69, 'DSK0', 46500, 50000),
('0710190006PBY', 524, 'DSK0', 13000, 20000),
('0710190007PBY', 821, 'DSK0', 6000, 10000),
('0710190008PBY', 575, 'DSK0', 38000, 50000),
('0810190001PBY', 248, 'DSK0', 15000, 20000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembayaran_wallet`
--

CREATE TABLE `pembayaran_wallet` (
  `ID_pembayaran` varchar(15) NOT NULL,
  `Nomor_transaksi` varchar(100) NOT NULL,
  `Kode_diskon` varchar(15) NOT NULL,
  `Total_tagihan` int(50) NOT NULL,
  `Nama_pembayaran` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengelola`
--

CREATE TABLE `pengelola` (
  `ID_pengelola` varchar(5) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Password` text NOT NULL,
  `Foto` text NOT NULL,
  `Nama_pengelola` varchar(30) NOT NULL,
  `Nomor_Telp` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengelola`
--

INSERT INTO `pengelola` (`ID_pengelola`, `Email`, `Password`, `Foto`, `Nama_pengelola`, `Nomor_Telp`) VALUES
('PEN01', 'ritelpreneur@gmail.com', '202cb962ac59075b964b07152d234b70', '.png', 'RitelPreneur', '0836818376444');

-- --------------------------------------------------------

--
-- Struktur dari tabel `stok`
--

CREATE TABLE `stok` (
  `ID_stok` int(11) NOT NULL,
  `Tanggal_stok` date NOT NULL,
  `Jumlah_stok_tersedia` int(4) NOT NULL,
  `Harga_barang` int(8) NOT NULL,
  `Total_harga` int(11) NOT NULL,
  `Barcode` varchar(25) NOT NULL,
  `ID_pengelola_stok` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `stok`
--

INSERT INTO `stok` (`ID_stok`, `Tanggal_stok`, `Jumlah_stok_tersedia`, `Harga_barang`, `Total_harga`, `Barcode`, `ID_pengelola_stok`) VALUES
(265, '2019-09-07', 16, 0, 0, '001', 'PEN01'),
(266, '2019-10-07', 7, 0, 0, '003', 'PEN01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `stok_keluar`
--

CREATE TABLE `stok_keluar` (
  `ID_stok_keluar` int(11) NOT NULL,
  `Tanggal_stok_keluar` date NOT NULL,
  `Jumlah_stok_keluar` int(4) NOT NULL,
  `Keterangan_stok_keluar` text NOT NULL,
  `ID_stok` int(11) NOT NULL,
  `ID_pengelola_stok_keluar` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `stok_keluar`
--

INSERT INTO `stok_keluar` (`ID_stok_keluar`, `Tanggal_stok_keluar`, `Jumlah_stok_keluar`, `Keterangan_stok_keluar`, `ID_stok`, `ID_pengelola_stok_keluar`) VALUES
(3, '2019-10-07', 10, 'Rusak', 266, 'PEN01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `stok_masuk`
--

CREATE TABLE `stok_masuk` (
  `ID_stok_masuk` int(11) NOT NULL,
  `Tanggal_stok_masuk` date NOT NULL,
  `Jumlah_stok_masuk` int(4) NOT NULL,
  `Harga_beli` int(11) NOT NULL,
  `Total_harga_beli` int(11) NOT NULL,
  `ID_stok` int(11) NOT NULL,
  `ID_pengelola_stok_masuk` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `stok_masuk`
--

INSERT INTO `stok_masuk` (`ID_stok_masuk`, `Tanggal_stok_masuk`, `Jumlah_stok_masuk`, `Harga_beli`, `Total_harga_beli`, `ID_stok`, `ID_pengelola_stok_masuk`) VALUES
(8, '2019-09-07', 10, 2000, 20000, 265, 'PEN01'),
(9, '2019-09-07', 5, 1500, 7500, 265, 'PEN01'),
(10, '2019-10-07', 10, 100000, 1000000, 266, 'PEN01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `stok_opname`
--

CREATE TABLE `stok_opname` (
  `ID_stokopname` int(11) NOT NULL,
  `Tanggal` date NOT NULL,
  `Keterangan_stok_opname` text DEFAULT NULL,
  `ID_pengelola` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `stok_relation`
--

CREATE TABLE `stok_relation` (
  `ID_stok` int(11) NOT NULL,
  `ID_stokopname` int(11) NOT NULL,
  `Jumlah_sistem` int(11) NOT NULL,
  `Jumlah_aktual` int(4) NOT NULL,
  `Selisih` int(4) NOT NULL,
  `Harga_lama` int(11) NOT NULL,
  `Harga_baru` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `Kode_transaksi` varchar(15) NOT NULL,
  `Tanggal_transaksi` date NOT NULL,
  `Waktu_Transaksi` time NOT NULL,
  `Estimasi_pembayaran` int(11) NOT NULL,
  `Jumlah_barang` int(4) NOT NULL,
  `ID_Kasir` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi`
--

INSERT INTO `transaksi` (`Kode_transaksi`, `Tanggal_transaksi`, `Waktu_Transaksi`, `Estimasi_pembayaran`, `Jumlah_barang`, `ID_Kasir`) VALUES
('0709190001TRX', '2019-09-07', '16:06:33', 6000, 2, 'EGN23'),
('0709190002TRX', '2019-09-07', '16:11:30', 4500, 2, 'EGN23'),
('0709190003TRX', '2019-09-07', '16:24:56', 2500, 1, 'EGN23'),
('0709190004TRX', '2019-09-07', '16:32:19', 10500, 3, 'EGN23'),
('0710190001TRX', '2019-10-07', '09:37:56', 9500, 3, 'EGN23'),
('0710190002TRX', '2019-10-07', '21:53:01', 7500, 1, 'EGN23'),
('0710190003TRX', '2019-10-07', '21:53:43', 6500, 1, 'EGN23'),
('0710190004TRX', '2019-10-07', '21:54:07', 10000, 1, 'EGN23'),
('0710190005TRX', '2019-10-07', '22:18:16', 46500, 6, 'EGN23'),
('0710190006TRX', '2019-10-07', '22:20:43', 13000, 4, 'EGN23'),
('0710190007TRX', '2019-10-07', '22:22:01', 6000, 2, 'EGN23'),
('0710190008TRX', '2019-10-07', '22:29:25', 38000, 4, 'EGN23'),
('0810190001TRX', '2019-10-08', '19:47:56', 15000, 3, 'EGN23'),
('1309190001TRX', '2019-09-13', '22:24:39', 32500, 7, 'EGN23');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`Barcode`),
  ADD KEY `Kode_kategori` (`Kode_kategori`),
  ADD KEY `ID_pengelola` (`ID_pengelola`);

--
-- Indeks untuk tabel `detail_transaksi`
--
ALTER TABLE `detail_transaksi`
  ADD KEY `detail_transaksi_ibfk_1` (`Kode_transaksi`),
  ADD KEY `detail_transaksi_ibfk_2` (`Barcode`),
  ADD KEY `ID_Kasir` (`ID_Kasir`);

--
-- Indeks untuk tabel `diskon`
--
ALTER TABLE `diskon`
  ADD PRIMARY KEY (`Kode_Diskon`),
  ADD KEY `ID_pengelola` (`ID_pengelola`);

--
-- Indeks untuk tabel `kasir`
--
ALTER TABLE `kasir`
  ADD PRIMARY KEY (`ID_kasir`),
  ADD KEY `ID_pengelola` (`ID_pengelola`);

--
-- Indeks untuk tabel `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`Kode_kategori`),
  ADD KEY `ID_pengelola` (`ID_pengelola`);

--
-- Indeks untuk tabel `metode_pembayaran`
--
ALTER TABLE `metode_pembayaran`
  ADD PRIMARY KEY (`Id_metodePembayaran`);

--
-- Indeks untuk tabel `oprkasir`
--
ALTER TABLE `oprkasir`
  ADD PRIMARY KEY (`ID_opr`),
  ADD KEY `ID_kasir` (`ID_kasir`);

--
-- Indeks untuk tabel `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD PRIMARY KEY (`ID_pembayaran`),
  ADD KEY `pembayaran_ibfk_1` (`Kode_transaksi`);

--
-- Indeks untuk tabel `pembayaran_tunai`
--
ALTER TABLE `pembayaran_tunai`
  ADD KEY `ID_pembayaran` (`ID_pembayaran`),
  ADD KEY `Kode_diskon` (`Kode_diskon`);

--
-- Indeks untuk tabel `pengelola`
--
ALTER TABLE `pengelola`
  ADD PRIMARY KEY (`ID_pengelola`);

--
-- Indeks untuk tabel `stok`
--
ALTER TABLE `stok`
  ADD PRIMARY KEY (`ID_stok`),
  ADD KEY `stok_ibfk_1` (`ID_pengelola_stok`),
  ADD KEY `stok_ibfk_2` (`Barcode`);

--
-- Indeks untuk tabel `stok_keluar`
--
ALTER TABLE `stok_keluar`
  ADD PRIMARY KEY (`ID_stok_keluar`),
  ADD KEY `ID_stok` (`ID_stok`),
  ADD KEY `ID_pengelola` (`ID_pengelola_stok_keluar`);

--
-- Indeks untuk tabel `stok_masuk`
--
ALTER TABLE `stok_masuk`
  ADD PRIMARY KEY (`ID_stok_masuk`),
  ADD KEY `ID_stok` (`ID_stok`),
  ADD KEY `ID_pengelola` (`ID_pengelola_stok_masuk`);

--
-- Indeks untuk tabel `stok_opname`
--
ALTER TABLE `stok_opname`
  ADD PRIMARY KEY (`ID_stokopname`),
  ADD KEY `ID_pengelola` (`ID_pengelola`);

--
-- Indeks untuk tabel `stok_relation`
--
ALTER TABLE `stok_relation`
  ADD KEY `ID_stok` (`ID_stok`),
  ADD KEY `ID_stokopname` (`ID_stokopname`);

--
-- Indeks untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`Kode_transaksi`),
  ADD KEY `transaksi_ibfk_1` (`ID_Kasir`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `oprkasir`
--
ALTER TABLE `oprkasir`
  MODIFY `ID_opr` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT untuk tabel `stok`
--
ALTER TABLE `stok`
  MODIFY `ID_stok` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=267;

--
-- AUTO_INCREMENT untuk tabel `stok_keluar`
--
ALTER TABLE `stok_keluar`
  MODIFY `ID_stok_keluar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `stok_masuk`
--
ALTER TABLE `stok_masuk`
  MODIFY `ID_stok_masuk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `stok_opname`
--
ALTER TABLE `stok_opname`
  MODIFY `ID_stokopname` int(11) NOT NULL AUTO_INCREMENT;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `barang`
--
ALTER TABLE `barang`
  ADD CONSTRAINT `barang_ibfk_1` FOREIGN KEY (`Kode_kategori`) REFERENCES `kategori` (`Kode_kategori`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `barang_ibfk_2` FOREIGN KEY (`ID_pengelola`) REFERENCES `pengelola` (`ID_pengelola`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `detail_transaksi`
--
ALTER TABLE `detail_transaksi`
  ADD CONSTRAINT `detail_transaksi_ibfk_2` FOREIGN KEY (`Barcode`) REFERENCES `barang` (`Barcode`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `detail_transaksi_ibfk_3` FOREIGN KEY (`ID_Kasir`) REFERENCES `kasir` (`ID_kasir`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `diskon`
--
ALTER TABLE `diskon`
  ADD CONSTRAINT `diskon_ibfk_1` FOREIGN KEY (`ID_pengelola`) REFERENCES `pengelola` (`ID_pengelola`);

--
-- Ketidakleluasaan untuk tabel `kasir`
--
ALTER TABLE `kasir`
  ADD CONSTRAINT `kasir_ibfk_1` FOREIGN KEY (`ID_pengelola`) REFERENCES `pengelola` (`ID_pengelola`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `kategori`
--
ALTER TABLE `kategori`
  ADD CONSTRAINT `kategori_ibfk_1` FOREIGN KEY (`ID_pengelola`) REFERENCES `pengelola` (`ID_pengelola`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `oprkasir`
--
ALTER TABLE `oprkasir`
  ADD CONSTRAINT `oprkasir_ibfk_1` FOREIGN KEY (`ID_kasir`) REFERENCES `kasir` (`ID_kasir`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `pembayaran_tunai`
--
ALTER TABLE `pembayaran_tunai`
  ADD CONSTRAINT `pembayaran_tunai_ibfk_1` FOREIGN KEY (`ID_pembayaran`) REFERENCES `pembayaran` (`ID_pembayaran`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `stok`
--
ALTER TABLE `stok`
  ADD CONSTRAINT `stok_ibfk_1` FOREIGN KEY (`ID_pengelola_stok`) REFERENCES `pengelola` (`ID_pengelola`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `stok_ibfk_2` FOREIGN KEY (`Barcode`) REFERENCES `barang` (`Barcode`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `stok_keluar`
--
ALTER TABLE `stok_keluar`
  ADD CONSTRAINT `stok_keluar_ibfk_1` FOREIGN KEY (`ID_stok`) REFERENCES `stok` (`ID_stok`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `stok_keluar_ibfk_2` FOREIGN KEY (`ID_pengelola_stok_keluar`) REFERENCES `pengelola` (`ID_pengelola`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `stok_masuk`
--
ALTER TABLE `stok_masuk`
  ADD CONSTRAINT `stok_masuk_ibfk_1` FOREIGN KEY (`ID_stok`) REFERENCES `stok` (`ID_stok`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `stok_masuk_ibfk_2` FOREIGN KEY (`ID_pengelola_stok_masuk`) REFERENCES `pengelola` (`ID_pengelola`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `stok_opname`
--
ALTER TABLE `stok_opname`
  ADD CONSTRAINT `stok_opname_ibfk_1` FOREIGN KEY (`ID_pengelola`) REFERENCES `pengelola` (`ID_pengelola`);

--
-- Ketidakleluasaan untuk tabel `stok_relation`
--
ALTER TABLE `stok_relation`
  ADD CONSTRAINT `stok_relation_ibfk_1` FOREIGN KEY (`ID_stok`) REFERENCES `stok` (`ID_stok`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `stok_relation_ibfk_2` FOREIGN KEY (`ID_stokopname`) REFERENCES `stok_opname` (`ID_stokopname`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `transaksi_ibfk_3` FOREIGN KEY (`ID_Kasir`) REFERENCES `kasir` (`ID_kasir`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
